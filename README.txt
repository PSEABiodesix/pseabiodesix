PSEA - Protein Set Enrichment Analysis

Contact Information - PSEA@biodesix.com

NOTE:
This application was developed for use with protein set evaluation but is used with genomic data for the purpose of this paper.

LICENSE:
This application can be used in accordance with the New BSD license agreement provided in the License.txt file.

OPERATIONAL EXECUTION:
The PSEA executable is designed to be run on a Windows platform from a command line prompt. The only tested environment is Windows 7.  It uses the Roguewave IMSL 
library (version 6.5.0) for math routines, so an IMSL license is required to run this executable - see https://www.roguewave.com/products-services/imsl-numerical-libraries/net-libraries.

The ready to run executable can be found in the folder - ..\bin\PSEA.exe

BUILDING THE SOURCE CODE:
The PSEA source code is written in C# and can be built using Microsoft Visual Studio 2012. The solution, project, and source files are included in 
..\source\.  The product has not been tested outside of a Windows 7, MS VS 2012, IMSL 6.5.0 environment.

The Visual Studio (2012 or newer) solution file can be found - ..\source\PSEA.sln

REQUIRED INPUT FILES (example data filenames):
-Feature table spreadsheet (*.xlsx) containing sample IDs (filenames), corresponding group names (this column needs to be present to run, but the contents can be arbitrary), 
 and feature or label data (FT_Phenotype.xlsx). The filenames in this file must be present in the analytes file.
-The analytes file (*.xlsx or *.csv) where the unique ID column contains the list of filenames (must match the list from the feature table) and the analyte values (protein expresssions, 
 or gene expressions - GT_Analytes.csv).
-The protein sets file (*.xlsx) that contains the ProteinIDs (or GeneIDs) matching the list from the analytes file and the protein/gene set definitions (Buckets.xlsx).

USAGE: (Can be obtained by entering the following command: <InstallationPath>\PSEA.exe ?

PSEA.exe
-ftbl Fully justified feature table spreadsheet filename (*.xlsx).
-ftblws Worksheet to use in the feature table spreadsheet.
-fcol Column in feature table worksheet that contains filenames (Must match analytes file filenames).
-gcol Column in feature table worksheet that contains groupnames.
-af Fully justified analytes filename (*.xlsx or *.csv file formats supported).
-afws Worksheet to use in the analytes spreadsheet.
-afcol Column in analytes worksheet that contains filenames (Must match feature table filenames).
-psf Fully justified protein sets spreadsheet filename (*.xlsx).
-psfws Worksheet to use in the protein sets spreadsheet.
-pspidcol Column in protein sets worksheet that contains the protein IDs.
-nr Number of realizations to use to compute the null distribution of the enrichment score(s).
-od Fully justified directory path where processing results will be saved.

Optional Arguments:
-nsplits Number of data split realizations to use in the PSEA process.  If this argument is not specified, then the standard PSEA process without data splitting will be used.
-ftr Feature name of continuous feature to analyze. This argument may be present more than once to specify more than one feature to analyze.
-allftrs If this argument is present, all numeric features in the input feature table will be analyzed.
-pftr Phenotype label to analyze (binary feature). This argument may be present only once.
-posl This argument defines the positive label to use with the phenotype label to analyze.  This arg is required if -pftr is used.
-mingrpsz This argument specifies the minimum allowable group size when analyzing labels and using the data split strategy.  This arg is required  and must be greater than zero when -pftr and -nsplits are used.
-psn Protein Set Name to analyze.  This argument may be present more than once to specify multiple protein sets to analyze.
-allpsn If this argument is present, all protein sets in the input protein sets file will be analyzed.
-outperm If this argument is present, permutations from the realizations will be output to a file called <ProteinSet>_<Feature>_Permutations.csv

Example Data: ..\ExampleData

NOTE - These examples must be modified to reflect the installation location.

Example command line syntax no splits:
>PSEA.exe -pftr vantveer2002_70_genes -posl Good -af C:\..\ExampleData\GT_Analytes.csv -afcol Filename -psf C:\..\ExampleData\Buckets.xlsx -psfws ProteinSets -pspidcol SomaID -psn HALLMARK_MYC_TARGETS_V1 -ftbl C:\..\ExampleData\FT_Phenotype.xlsx -ftblws FT -fcol Filename -gcol Groupname -od C:\..\ExampleData\Output -nr 1000

Example command line syntax with splits:
>PSEA.exe -pftr vantveer2002_70_genes -posl Good -af C:\..\ExampleData\GT_Analytes.csv -afcol Filename -psf C:\..\ExampleData\Buckets.xlsx -psfws ProteinSets -pspidcol SomaID -psn HALLMARK_MYC_TARGETS_V1 -ftbl C:\..\ExampleData\FT_Phenotype.xlsx -ftblws FT -fcol Filename -gcol Groupname -od C:\..\ExampleData\Output -mingrpsz 10 -nsplits 1 -nr 1000

