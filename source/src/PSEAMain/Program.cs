﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: Program.cs
//
//      (C) Copyright 2018 Biodesix Inc.
//      All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Reflection;
using Biodesix.PSEA.Utils;
using System.Diagnostics;

namespace Biodesix.PSEA
{
    /// <summary>
    /// The PSEA command line tool's startup class.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The initial method to execute when the application runs.
        /// </summary>
        /// <param name="args">The arguments supplied on the command line.</param>
        static void Main(string[] args)
        {
            try
            {

                Console.WriteLine("Running PSEA.exe (ver " + Assembly.GetExecutingAssembly().GetName().Version.ToString() + ")");
                // start the timer and parse the file list
                Stopwatch sw = Stopwatch.StartNew();
                
                // parse the command line arguments using an ArgParser instance
                ArgParser theParser = new ArgParser(args);

                // process the arguments
                PSEAController.PerformPSEA(theParser);

                sw.Stop();

                // report the processing time
                Console.WriteLine("Processing time: " + sw.Elapsed.TotalSeconds.ToString() + " seconds");  
                Environment.Exit(0);
            }
            catch (Exception ex)
            {                
                Console.WriteLine(ex.Message);
                if (!ex.Message.Contains("You asked for PSEA (ver "))
                {
                    Console.WriteLine(ex.StackTrace);
                }
                Environment.Exit(-1);
            }
        }
        
    }
}
