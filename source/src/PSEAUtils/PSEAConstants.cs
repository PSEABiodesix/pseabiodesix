﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: PSEAConstants.cs
//
//      (C) Copyright 2018 Biodesix Inc.
//      All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

namespace Biodesix.PSEA.Utils
{
    /// <summary>
    /// This class contains constants used throughout the PSEA application.
    /// </summary>
    public static class PSEAConstants
    {
        #region Command Line Argument Constants
        /// <summary>
        /// "?" The command line argument that will display the usage information.
        /// </summary>
        public const string PSEA_USAGE_ARG = "?";
        /// <summary>
        /// "-ftbl" The command line argument to specify the input feature table spreadsheet filename.
        /// </summary>
        public const string PSEA_FTR_TBL_SS_FILE_ARG = "-ftbl";
        /// <summary>
        /// "-ftblws" The command line argument to specify the worksheet to use in the input feature table spreadsheet.
        /// </summary>
        public const string PSEA_FTR_TBL_WS_NAME_ARG = "-ftblws";
        /// <summary>
        /// "-fcol" The command line argument to specify the name of the column that contains filenames in the 
        /// input feature table spreadsheet.
        /// </summary>
        public const string PSEA_FTR_TBL_FILENAME_COL_ARG = "-fcol";
        /// <summary>
        /// "-gcol" The command line argument to specify the name of the column that contains groupnames in the input
        /// feature table spreadsheet.
        /// </summary>
        public const string PSEA_FTR_TBL_GROUPNAME_COL_ARG = "-gcol";
        /// <summary>
        /// "-ftr" The command line argument to specify the name of a continous feature to analyze.
        /// </summary>
        public const string PSEA_FTR_ARG = "-ftr";
        /// <summary>
        /// "-allftrs" The command line argument to specify to analyze all continuous features in the input feature table.
        /// </summary>
        public const string PSEA_ALL_FTRS_ARG = "-allftrs";
        /// <summary>
        /// "-pftr" The command line argument to specify the name of a binary feature to analyze.
        /// </summary>
        public const string PSEA_PFTR_ARG = "-pftr";
        /// <summary>
        /// "-posl" The command line argument to specify the label to use as the positive label when 
        /// doing PSEA analysis of a binary feature.
        /// </summary>
        public const string PSEA_POS_LABEL_ARG = "-posl";
        /// <summary>
        /// "-psf" The command line argument to specify the input protein set spreadsheet filename.
        /// </summary>
        public const string PSEA_PROTEIN_SET_SS_FILE_ARG = "-psf";
        /// <summary>
        /// "-psfws" The command line argument to specify the worksheet to use in the input protein set spreadsheet.
        /// </summary>
        public const string PSEA_PROTEIN_SET_WS_NAME_ARG = "-psfws";
        /// <summary>
        /// "-pspidcol" The command line argument to specify the column in the protein set worksheet that contains
        /// the protein IDs.
        /// </summary>
        public const string PSEA_PROTEIN_SET_PROTEIN_ID_COL_ARG = "-pspidcol";
        /// <summary>
        /// "-psn" The command line argument to specify the name of a protein set to analyze.
        /// </summary>
        public const string PSEA_PROTEIN_SET_NAME_ARG = "-psn";
        /// <summary>
        /// "-allpsn" The command line argument to specify to analyze all protein sets in the input protein set spreadsheet.
        /// </summary>
        public const string PSEA_ALL_PROTEIN_SETS_ARG = "-allpsn";
        /// <summary>
        /// "-af" The command line argument to specify the analytes spreadsheet file.
        /// </summary>
        public const string PSEA_ANALYTES_FILE_ARG = "-af";
        /// <summary>
        /// "-afws" The command line argument to specify the worksheet to use in the analytes spreadsheet file.
        /// </summary>
        public const string PSEA_ANALYTES_WS_NAME_ARG = "-afws";
        /// <summary>
        /// "-afcol" The command line argument to specify the name of the column in the analytes worksheet that contains
        /// the filenames (or sampleids).
        /// </summary>
        public const string PSEA_ANALYTES_FILENAME_COL_ARG = "-afcol";
        /// <summary>
        /// "-nr" The command line argument to specify the number of realizations to use when computing the 
        /// null distribution statistics.
        /// </summary>
        public const string PSEA_NUM_REALIZATIONS_ARG = "-nr";
        /// <summary>
        /// "-nsplits" The command line argument to specify the number of 50/50 data splits to use when performing
        /// the PSEA process.
        /// </summary>
        public const string PSEA_NUM_SPLITS_ARG = "-nsplits";
        /// <summary>
        /// "-mingrpsz" The command line argument to specify the minimum allowable group size when analyzing binary labels
        /// with data splitting.  This argument is used to prevent the random selection and shuffling from 
        /// selecting all of one group and none of the other.
        /// </summary>
        public const string PSEA_MIN_GRP_SZ_ARG = "-mingrpsz";
        /// <summary>
        /// "-od" The command line argument to specify the output directory in which to write the results.
        /// </summary>
        public const string PSEA_OUTPUT_FOLDER_ARG = "-od";
        /// <summary>
        /// "-outperm" The command line argument to specify that the output permutations for the realizations should 
        /// be written to a file.
        /// </summary>
        public const string PSEA_OUTPUT_PERMUTATIONS_ARG = "-outperm";
        #endregion

        #region Common Data Values
        /// <summary>
        /// Value expected in the protein set table for a protein ID that is in a given protein set.
        /// </summary>
        public const string PROTEIN_SET_YES = "yes";
        /// <summary>
        /// Value expected in the protein set table for a protein ID that is NOT in a given protein set.
        /// </summary>
        public const string PROTEIN_SET_NO = "no";
        #endregion

        #region Computation Constants
        /// <summary>
        /// This is the P-Value to return is the enrichment score p-value encounters an error.
        /// </summary>
        public const double INVALID_ES_PVALUE = -1000000.0;
        #endregion
    }
}
