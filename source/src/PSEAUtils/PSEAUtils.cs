﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: PSEAUtils.cs
//
//      (C) Copyright 2018 Biodesix Inc.
//      All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

using Local.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using ZedGraph;

namespace Biodesix.PSEA.Utils
{
    /// <summary>
    /// This class contains the utility methods needed to do PSEA computations.
    /// </summary>
    public static class PSEAUtils
    {
        #region PSEA Method Results Keys
        /// <summary>
        /// This is the key to get the feature name that was analyzed out of the results of 
        /// PerformBinaryPSEA or PerformContinuousPSEA.
        /// </summary>
        public static string RESULTS_FEATURE_NAME_KEY = "FeatureName";
        /// <summary>
        /// This is the key to get the name of the protein set that was analyzed out of the results of 
        /// PerformBinaryPSEA or PerformContinuousPSEA.
        /// </summary>
        public static string RESULTS_PROTEIN_SET_NAME_KEY = "ProteinSetName";
        /// <summary>
        /// This is the key to get the Enrichment Score computed using the max-method out of the results of 
        /// PerformBinaryPSEA or PerformContinuousPSEA.
        /// </summary>
        public static string RESULTS_ES_MAX_KEY = "ESMax";
        /// <summary>
        /// This is the key to get the Enrichment Score computed using the sum-method out of the results of 
        /// PerformBinaryPSEA or PerformContinuousPSEA.
        /// </summary>
        public static string RESULTS_ES_SUM_KEY = "ESSum";
        /// <summary>
        /// This is the key to get the Pvalue for the Enrichment Score computed using the 
        /// max-method out of the results of PerformBinaryPSEA or PerformContinuousPSEA.
        /// </summary>
        public static string RESULTS_PVALUE_MAX_KEY = "PValueMax";
        /// <summary>
        /// This is the key to get the Pvalue for the Enrichment Score computed using the 
        /// sum-method out of the results of PerformBinaryPSEA or PerformContinuousPSEA.
        /// </summary>
        public static string RESULTS_PVALUE_SUM_KEY = "PValueSum";
        #endregion

        #region ES Running Sum Data Table Constants
        /// <summary>
        /// "ESRunningSumSummary" ES running sum table name.
        /// </summary>
        public static string ES_RUNNING_SUM_TABLE_NAME = "ESRunningSumSummary";
        /// <summary>
        /// "ProteinID" ES running sum table protein ID column name.
        /// </summary>
        public static string ES_RUNNING_SUM_TABLE_PROT_ID_COL = "ProteinID";
        /// <summary>
        /// "ProteinOrder" ES running sum table protein order column name.
        /// </summary>
        public static string ES_RUNNING_SUM_TABLE_PROT_ORDER_COL = "ProteinOrder";
        /// <summary>
        /// "Correlation" ES running sum table correlation column name.
        /// </summary>
        public static string ES_RUNNING_SUM_TABLE_CORRELATION_COL = "Correlation";
        /// <summary>
        /// "PValue" ES running sum table Pvalue column name.
        /// </summary>
        public static string ES_RUNNING_SUM_TABLE_PVALUE_COL = "PValue";
        /// <summary>
        /// "ESRunningSum" ES running sum table running sum column name.
        /// </summary>
        public static string ES_RUNNING_SUM_TABLE_ESRS_COL = "ESRunningSum";
        #endregion

        #region Enrichment Score Computation Types - maybe put in an Enumerated type in the future
        /// <summary>
        /// This is the ESMax method of computation for the Enrichment Score
        /// </summary>
        public static string ES_MAX = "ESMax";
        /// <summary>
        /// This is the ESSum method of computation for the Enrichment Score
        /// </summary>
        public static string ES_SUM = "ESSum";
        #endregion

        /// <summary>
        /// Per Carlos, the normalization exponent we will use for the forseeable future is 1.0.
        /// </summary>
        private const double NORMALIZATION_EXPONENT = 1.0;

        #region Random Number Generator to use
        // Set up random number generator to be like Matlab ... not exact!
        /// <summary>
        /// This is the seed to use to create the MersenneTwister.  This seed makes the MersenneTwister
        /// random number generator perform as close to Matlab's random number generator as possible.
        /// </summary>
        public const int RANDOM_SEED = 5489;
        #endregion

        #region Main Utility Methods that are run in Tasks
        /// <summary>
        /// This method peforms Binary PSEA (Protein Set Enrichment Analysis) for a single binary feature name / protein set pair.  
        /// 
        /// Binary PSEA is PSEA performed using a binary phenotype feature (e.g. a binary classification label).  Becuase the feature is binary,
        /// correlations between the binary feature and the protein IDs and the associated p-values are computed using a ranksum statistic.
        /// 
        /// This method is designed to be run in a C# Task, but could
        /// als be called normally, and returns a dictionary of results using the defined results keys in this file with objects as the values.  
        /// The types of the results objects returned for each key are:
        /// RESULTS_FEATURE_NAME_KEY - string containing the feature name for which analysis was performed.
        /// RESULTS_PROTEIN_SET_NAME_KEY - string containing the name of the protein set for which analysis was performed.
        /// RESULTS_ES_MAX_KEY - double containing the enrichment score for this feature/protein set combo, computed using the max-method.
        /// RESULTS_ES_SUM_KEY - double containing the enrichment score for this feature/protein set combo, computed using the sum-method.
        /// RESULTS_PVALUE_MAX_KEY - double containing the p-value for the computed enrichment score using the max-method.
        /// RESULTS_PVALUE_SUM_KEY - double containing the p-value for the computed enrichment score using the sum-method.
        /// 
        /// </summary>
        /// <param name="correlations">A double array of correlations, one correlation between the provided feature and every protein ID.</param>
        /// <param name="pValues">A double array of p-values for the input correlations, expected to be the same length and order as the correlations.</param>
        /// <param name="group0Indices">A list of integer indices that correspond to the group 0 rows in the analyte data.</param>
        /// <param name="group1Indices">A list of integer indices that correspond to the group 1 rows in the analyte data.</param>
        /// <param name="analyteValues">A 2-D array where the rows are the samples, and the columns are the protein IDs.  
        /// The value is the RANK of the protein expression for the given proteinID. 
        /// The samples are expected in the same order as the input feature table</param>
        /// <param name="phenotypeFeatureToAnalyze">The name of the binary phenotype feature (label) for analysis.</param>
        /// <param name="proteinSetToAnalyze">The name of the protein set for analysis.</param>
        /// <param name="allProteinIDList">This is the list of all protein IDs in the analytes table.  This is expected to be in the order
        /// of the columns in the analyteValues input.</param>
        /// <param name="proteinIDsInProteinSet">This is a list of the protein IDs that belong to the protein set for analysis.  This 
        /// list corresponds to those protein IDs that have a 'yes' in the protein set table for the given protein set.</param>
        /// <param name="numRealizations">This is the number of realizations to perform to compute the null distribution statistics.</param>
        /// <param name="resultsDataDirectory">This is the directory in which to save intermediate results such as plots and csv files for
        /// running sums and distribution statistics.</param>
        /// <param name="rng">This is an instance of an IMSL random generation object.</param>
        /// <param name="outputPermutations">This bool indicates if the permutations should be saved.</param>
        /// <returns>The results object dictionary keyed by well known results keys. See method documentation above.</returns>
        internal static Dictionary<string, object> PerformBinaryPSEA(double[] correlations, double[] pValues, List<int> group0Indices, 
            List<int> group1Indices, double[,] analyteValues,
            string phenotypeFeatureToAnalyze, string proteinSetToAnalyze, List<string> allProteinIDList, List<string> proteinIDsInProteinSet,
            int numRealizations, string resultsDataDirectory, Random rng, bool outputPermutations)
        {
            // Create the results object and add the two known values right away (feature name and protein set name)
            Dictionary<string, object> retval = new Dictionary<string, object>();
            retval.Add(RESULTS_FEATURE_NAME_KEY, phenotypeFeatureToAnalyze);
            retval.Add(RESULTS_PROTEIN_SET_NAME_KEY, proteinSetToAnalyze);

            #region Compute the Enrichment Score (ES) Running Sum
            DataTable esRunningSumResultsTable = null;

            double[] esRunningSum = ComputeESRunningSumForProteinSet(correlations, pValues, allProteinIDList,
                proteinIDsInProteinSet, NORMALIZATION_EXPONENT, out esRunningSumResultsTable);
            #endregion

            #region Write ES Running Sum Results .csv file
            string runningSumPlotFilename = resultsDataDirectory + "\\PSEA_" + phenotypeFeatureToAnalyze + "_" + proteinSetToAnalyze + "_ESRunningSum.png";
            CreateAndSaveRunningSumPlot(esRunningSum, runningSumPlotFilename, phenotypeFeatureToAnalyze, proteinSetToAnalyze);

            string theESRunningSumFileName = resultsDataDirectory + "\\PSEA_" + phenotypeFeatureToAnalyze + "_" + proteinSetToAnalyze + "_ESRunningSum.csv";
            string[,] dtAsArray = TwoDimensionalStringArrayUtils.ConvertDataTableTo2DString(esRunningSumResultsTable);
            TwoDimensionalStringArrayUtils.WriteCSVFile(dtAsArray, theESRunningSumFileName);
            #endregion

            // Compute the Enrichment Scores (max and sum methods) - add those to the results dictionary
            double enrichmentScoreMax = 0.0;
            double enrichmentScoreSum = 0.0;

            ComputeEnrichmentScores(esRunningSum, out enrichmentScoreMax, out enrichmentScoreSum);
            retval.Add(RESULTS_ES_MAX_KEY, enrichmentScoreMax);
            retval.Add(RESULTS_ES_SUM_KEY, enrichmentScoreSum);

            // Compute p-values for enrichment scores by doing the realizations to elicit the null distribution
            double[] esMaxForAllRealizations = new double[numRealizations];
            double[] esSumForAllRealizations = new double[numRealizations];
            double[,] shuffledGroupValues = null;
            if (outputPermutations)
            {
                shuffledGroupValues = new double[numRealizations, analyteValues.GetLength(0)];
            }

            for (int i = 0; i < numRealizations; i++)
            {
                #region Perform Realization
                // For each realization, 
                // 1. Shuffle all of the data
                // 2. Recompute group0 and group1 indices
                // 3. Recompute correlations and pvalues
                // 4. Recompute ES Running Sum
                // 5. Recompute ES (max and sum)
                int[] permutationArray = GetPermutationArray(analyteValues.GetLength(0), rng);
                List<int> group0IndicesShuffled = new List<int>();
                foreach (int idx in group0Indices)
                {
                    if (outputPermutations)
                    {
                        shuffledGroupValues[i, permutationArray[idx]] = 0.0;
                    }
                    group0IndicesShuffled.Add(permutationArray[idx]);
                }

                List<int> group1IndicesShuffled = new List<int>();
                foreach (int idx in group1Indices)
                {
                    if (outputPermutations)
                    {
                        shuffledGroupValues[i, permutationArray[idx]] = 1.0;
                    }
                    group1IndicesShuffled.Add(permutationArray[idx]);
                }

                double[] correlationsShuffled = null;
                double[] pValuesShuffled = null;

                // Compute rank correlations since analyte values are ranks.
                ComputeBinaryRankCorrelationsAndPValues(group0IndicesShuffled, group1IndicesShuffled, analyteValues, out correlationsShuffled, out pValuesShuffled);

                double[] esRunningSumShuffled = ComputeESRunningSumForProteinSet(correlationsShuffled, pValuesShuffled,
                    allProteinIDList, proteinIDsInProteinSet, NORMALIZATION_EXPONENT, out esRunningSumResultsTable);
                double enrichmentScoreMaxShuffled = 0.0;
                double enrichmentScoreSumShuffled = 0.0;
                ComputeEnrichmentScores(esRunningSumShuffled, out enrichmentScoreMaxShuffled, out enrichmentScoreSumShuffled);
                esMaxForAllRealizations[i] = enrichmentScoreMaxShuffled;
                esSumForAllRealizations[i] = enrichmentScoreSumShuffled;
                #endregion
            }

            // Compute the PValues for max and sum - add them to the results dictionary
            double esMaxPValue = ComputeEnrichmentScoreMaxPValue(esMaxForAllRealizations, enrichmentScoreMax);
            double esSumPValue = ComputeEnrichmentScoreSumPValue(esSumForAllRealizations, enrichmentScoreSum);
            retval.Add(RESULTS_PVALUE_MAX_KEY, esMaxPValue);
            retval.Add(RESULTS_PVALUE_SUM_KEY, esSumPValue);

            #region Save Plots for Distributions
            string esMaxDistPlotFilename = resultsDataDirectory + "\\PSEA_" + phenotypeFeatureToAnalyze + "_" + proteinSetToAnalyze + "_ESMaxDist.png";
            CreateAndSaveESDistributionPlot(esMaxForAllRealizations, enrichmentScoreMax, ES_MAX, esMaxDistPlotFilename, phenotypeFeatureToAnalyze, proteinSetToAnalyze);
            string esSumDistPlotFilename = resultsDataDirectory + "\\PSEA_" + phenotypeFeatureToAnalyze + "_" + proteinSetToAnalyze + "_ESSumDist.png";
            CreateAndSaveESDistributionPlot(esSumForAllRealizations, enrichmentScoreSum, ES_SUM, esSumDistPlotFilename, phenotypeFeatureToAnalyze, proteinSetToAnalyze);
            #endregion

            #region Write ESNulls .csv file
            DataTable esNullsTable = new DataTable();
            esNullsTable.Columns.Add("ESMax");
            esNullsTable.Columns.Add("ESSum");

            for (int i = 0; i < numRealizations; i++)
            {
                DataRow dr = esNullsTable.NewRow();
                dr["ESMax"] = esMaxForAllRealizations[i];
                dr["ESSum"] = esSumForAllRealizations[i];
                esNullsTable.Rows.Add(dr);
            }

            string theESNullsFileName = resultsDataDirectory + "\\PSEA_" + phenotypeFeatureToAnalyze + "_" + proteinSetToAnalyze + "_ESNulls.csv";
            string[,] dtAsArray2 = TwoDimensionalStringArrayUtils.ConvertDataTableTo2DString(esNullsTable);
            TwoDimensionalStringArrayUtils.WriteCSVFile(dtAsArray2, theESNullsFileName);
            #endregion

            #region Write Permutations .csv file if desired
            if (outputPermutations)
            {
                string permFilename = resultsDataDirectory + "\\PSEA_" + phenotypeFeatureToAnalyze + "_" + proteinSetToAnalyze + "_Permutations.csv";
                Local.Util.TwoDimensionalStringArrayUtils.WriteCSVFile(shuffledGroupValues, permFilename);
            }
            #endregion

            return retval;
        }

        /// <summary>
        /// This method peforms Continuous PSEA (Protein Set Enrichment Analysis) for a single continuous feature name / protein set pair.  
        /// 
        /// Continuous PSEA is PSEA performed using a continuous phenotype feature (e.g. a mass spec feature).  Becuase the feature is continuous,
        /// correlations between the continuous feature and the protein IDs and the associated p-values are computed using a Spearman correlation
        /// statistic.
        /// 
        /// This method is designed to be run in a C# Task, but could
        /// als be called normally, and returns a dictionary of results using the defined results keys in this file with objects as the values.  
        /// The types of the results objects returned for each key are:
        /// RESULTS_FEATURE_NAME_KEY - string containing the feature name for which analysis was performed.
        /// RESULTS_PROTEIN_SET_NAME_KEY - string containing the name of the protein set for which analysis was performed.
        /// RESULTS_ES_MAX_KEY - double containing the enrichment score for this feature/protein set combo, computed using the max-method.
        /// RESULTS_ES_SUM_KEY - double containing the enrichment score for this feature/protein set combo, computed using the sum-method.
        /// RESULTS_PVALUE_MAX_KEY - double containing the p-value for the computed enrichment score using the max-method.
        /// RESULTS_PVALUE_SUM_KEY - double containing the p-value for the computed enrichment score using the sum-method.
        /// 
        /// </summary>
        /// <param name="correlations">A double array of correlations, one correlation between the provided feature and every protein ID.</param>
        /// <param name="pValues">A double array of p-values for the input correlations, expected to be the same length and order as the correlations.</param>
        /// <param name="featureValues">A double[] of feature values for the feature to analyze, in the order of rows in the analyte values.</param>
        /// <param name="analyteValues">A 2-D array where the rows are the samples, and the columns are the protein IDs.  
        /// The value is the protein expression for the given sample and proteinID. 
        /// The samples are expected in the same order as the input feature values.</param>
        /// <param name="phenotypeFeatureToAnalyze">The name of the phenotype feature for analysis.</param>
        /// <param name="proteinSetToAnalyze">The name of the protein set for analysis.</param>
        /// <param name="allProteinIDList">This is the list of all protein IDs in the analytes table.  This is expected to be in the order
        /// of the columns in the analyteValues input.</param>
        /// <param name="proteinIDsInProteinSet">This is a list of the protein IDs that belong to the protein set for analysis.  This 
        /// list corresponds to those protein IDs that have a 'yes' in the protein set table for the given protein set.</param>
        /// <param name="numRealizations">This is the number of realizations to perform to compute the null distribution statistics.</param>
        /// <param name="resultsDataDirectory">This is the directory in which to save intermediate results such as plots and csv files for
        /// running sums and distribution statistics.</param>
        /// <param name="rng">This is an instance of an IMSL random generation object.</param>
        /// <param name="outputPermutations">This bool indicates if the permutations should be saved.</param>
        /// <returns>The results object dictionary keyed by well known results keys. See method documentation above.</returns>
        internal static Dictionary<string, object> PerformContinuousPSEA(double[] correlations, double[] pValues, double[] featureValues, double[,] analyteValues,
            string phenotypeFeatureToAnalyze, string proteinSetToAnalyze, List<string> allProteinIDList, List<string> proteinIDsInProteinSet,
            int numRealizations, string resultsDataDirectory, Random rng, bool outputPermutations)
        {
            // Create the results object dictionary, and populate the two known values for feature name and protein set
            Dictionary<string, object> retval = new Dictionary<string, object>();
            retval.Add(RESULTS_FEATURE_NAME_KEY, phenotypeFeatureToAnalyze);
            retval.Add(RESULTS_PROTEIN_SET_NAME_KEY, proteinSetToAnalyze);

            #region Compute the ES Running Sum
            DataTable esRunningSumResultsTable = null;
            double[] esRunningSum = ComputeESRunningSumForProteinSet(correlations, pValues, allProteinIDList,
                proteinIDsInProteinSet, NORMALIZATION_EXPONENT, out esRunningSumResultsTable);
            #endregion

            #region Write ES Running Sum Results .csv file, and plot
            string runningSumPlotFilename = resultsDataDirectory + "\\PSEA_" + phenotypeFeatureToAnalyze + "_" + proteinSetToAnalyze + "_ESRunningSum.png";
            CreateAndSaveRunningSumPlot(esRunningSum, runningSumPlotFilename, phenotypeFeatureToAnalyze, proteinSetToAnalyze);

            string theESRunningSumFileName = resultsDataDirectory + "\\PSEA_" + phenotypeFeatureToAnalyze + "_" + proteinSetToAnalyze + "_ESRunningSum.csv";
            string[,] dtAsArray = TwoDimensionalStringArrayUtils.ConvertDataTableTo2DString(esRunningSumResultsTable);
            TwoDimensionalStringArrayUtils.WriteCSVFile(dtAsArray, theESRunningSumFileName);
            #endregion

            // Compute the Enrichment Scores and add them to the results.
            double enrichmentScoreMax = 0.0;
            double enrichmentScoreSum = 0.0;

            ComputeEnrichmentScores(esRunningSum, out enrichmentScoreMax, out enrichmentScoreSum);
            retval.Add(RESULTS_ES_MAX_KEY, enrichmentScoreMax);
            retval.Add(RESULTS_ES_SUM_KEY, enrichmentScoreSum);

            // Compute p-values for enrichment scores by doing the realizations to elicit the null distribution
            double[] esMaxForAllRealizations = new double[numRealizations];
            double[] esSumForAllRealizations = new double[numRealizations];

            double[,] shuffledFeatureValues = null;
            if (outputPermutations)
            {
                shuffledFeatureValues = new double[numRealizations, analyteValues.GetLength(0)];
            }

            for (int i = 0; i < numRealizations; i++)
            {
                #region Perform Realization
                // For each realization, 
                // 1. Shuffle all of the data
                // 3. Recompute correlations and pvalues
                // 4. Recompute ES Running Sum
                // 5. Recompute ES (max and sum)
                double[] featureValuesShuffled = ShuffleDoubles(featureValues, rng);
                if (outputPermutations)
                {
                    for (int j = 0; j < featureValuesShuffled.Length; j++)
                    {
                        shuffledFeatureValues[i, j] = featureValuesShuffled[j];
                    }
                }
                double[] correlationsShuffled = null;
                double[] pValuesShuffled = null;
                ComputeContinuousCorrelationsAndPValues(featureValuesShuffled, analyteValues, out correlationsShuffled, out pValuesShuffled);
                double[] esRunningSumShuffled = ComputeESRunningSumForProteinSet(correlationsShuffled, pValuesShuffled,
                    allProteinIDList, proteinIDsInProteinSet, NORMALIZATION_EXPONENT, out esRunningSumResultsTable);
                double enrichmentScoreMaxShuffled = 0.0;
                double enrichmentScoreSumShuffled = 0.0;
                ComputeEnrichmentScores(esRunningSumShuffled, out enrichmentScoreMaxShuffled, out enrichmentScoreSumShuffled);
                esMaxForAllRealizations[i] = enrichmentScoreMaxShuffled;
                esSumForAllRealizations[i] = enrichmentScoreSumShuffled;
                #endregion
            }

            // Compute the PValues for both methods (max and sum) and add them to the results.
            double esMaxPValue = ComputeEnrichmentScoreMaxPValue(esMaxForAllRealizations, enrichmentScoreMax);
            double esSumPValue = ComputeEnrichmentScoreSumPValue(esSumForAllRealizations, enrichmentScoreSum);
            retval.Add(RESULTS_PVALUE_MAX_KEY, esMaxPValue);
            retval.Add(RESULTS_PVALUE_SUM_KEY, esSumPValue);

            #region Save the Distribution Plots
            string esMaxDistPlotFilename = resultsDataDirectory + "\\PSEA_" + phenotypeFeatureToAnalyze + "_" + proteinSetToAnalyze + "_ESMaxDist.png";
            CreateAndSaveESDistributionPlot(esMaxForAllRealizations, enrichmentScoreMax, "ESMax", esMaxDistPlotFilename, phenotypeFeatureToAnalyze, proteinSetToAnalyze);
            string esSumDistPlotFilename = resultsDataDirectory + "\\PSEA_" + phenotypeFeatureToAnalyze + "_" + proteinSetToAnalyze + "_ESSumDist.png";
            CreateAndSaveESDistributionPlot(esSumForAllRealizations, enrichmentScoreSum, "ESSum", esSumDistPlotFilename, phenotypeFeatureToAnalyze, proteinSetToAnalyze);
            #endregion

            #region Write ESNulls .csv file
            DataTable esNullsTable = new DataTable();
            esNullsTable.Columns.Add("ESMax");
            esNullsTable.Columns.Add("ESSum");

            for (int i = 0; i < numRealizations; i++)
            {
                DataRow dr = esNullsTable.NewRow();
                dr["ESMax"] = esMaxForAllRealizations[i];
                dr["ESSum"] = esSumForAllRealizations[i];
                esNullsTable.Rows.Add(dr);
            }

            string theESNullsFileName = resultsDataDirectory + "\\PSEA_" + phenotypeFeatureToAnalyze + "_" + proteinSetToAnalyze + "_ESNulls.csv";
            string[,] dtAsArray2 = TwoDimensionalStringArrayUtils.ConvertDataTableTo2DString(esNullsTable);
            TwoDimensionalStringArrayUtils.WriteCSVFile(dtAsArray2, theESNullsFileName);
            #endregion

            #region Write Permutations .csv file if desired
            if (outputPermutations)
            {
                string permFilename = resultsDataDirectory + "\\PSEA_" + phenotypeFeatureToAnalyze + "_" + proteinSetToAnalyze + "_Permutations.csv";
                Local.Util.TwoDimensionalStringArrayUtils.WriteCSVFile(shuffledFeatureValues, permFilename);
            }
            #endregion

            return retval;
        }
        #endregion

        #region Data Structuring Methods
        /// <summary>
        /// This method takes the input protein sets data table and returns a dictionary where the keys are the protein set names
        /// to analyze, and the values are the lists of protein IDs that are in each protein set to analyze.
        /// </summary>
        /// <param name="proteinSetsDataTable">The full protein sets data table.</param>
        /// <param name="proteinSetsToAnalyze">The list of protein sets to analyze.</param>
        /// <param name="proteinIDColumn">The column in the protein sets data table that contains the protein IDs.</param>
        /// <returns>A dictionary, where the keys are the protein set names to analyze, and the value is a list of
        /// protein IDs that are included in the protein set to analyze.</returns>
        internal static Dictionary<string, List<string>> GetProteinIDListsForProteinSets(DataTable proteinSetsDataTable, List<string> proteinSetsToAnalyze, string proteinIDColumn)
        {
            Dictionary<string, List<string>> retval = new Dictionary<string, List<string>>();
            foreach (string proteinSet in proteinSetsToAnalyze)
            {
                retval.Add(proteinSet, new List<string>());

                foreach (DataRow dr in proteinSetsDataTable.Rows)
                {
                    if (dr[proteinSet].ToString().Trim().ToLower().Equals(PSEAConstants.PROTEIN_SET_YES))
                    {
                        retval[proteinSet].Add(dr[proteinIDColumn].ToString());
                    }
                }
            }
            return retval;
        }

        /// <summary>
        /// This method returns a feature value vector (double[]) for a feature given a feature table.  An optional argument is available
        /// for columns that contain binary labels to specify the positive label.  In this case a vector of 0.0's and 1.0's will be returned
        /// where 1.0 replaces the positive label strings, and 0.0 replaces the other label strings.  There is no check here to make 
        /// sure the feature is binary - that is incumbent upon the calling code if that is the desired behavior.
        /// </summary>
        /// <param name="theFT">The feature table to retrieve the feature column from.</param>
        /// <param name="featureName">The feature name of the feature to get the values for.</param>
        /// <param name="positiveLabel">Optional - the positive label for a binary string feature.</param>
        /// <returns>A double[] that contains the feature values for the specified feature in the order of the rows in the feature table.</returns>
        internal static double[] GetPhenotypeFeatureValueArray(FeatureTable theFT, string featureName, string positiveLabel = null)
        {
            // Make the return value.
            double[] featureValues = new double[theFT.Rows.Count];

            // Continuous Feature Values
            if (String.IsNullOrEmpty(positiveLabel))
            {
                int idx = 0;
                foreach (DataRow dr in theFT.Rows)
                {
                    double featureValue = Convert.ToDouble(dr[featureName].ToString());
                    featureValues[idx] = featureValue;
                    idx++;
                }
            }
            else // Binary feature values
            {
                int idx = 0;
                foreach (DataRow dr in theFT.Rows)
                {
                    string featureString = dr[featureName].ToString();
                    double featureValue = 0.0;
                    if (featureString.Equals(positiveLabel))
                    {
                        featureValue = 1.0;
                    }
                    featureValues[idx] = featureValue;
                    idx++;
                }
            }

            return featureValues;
        }

        /// <summary>
        /// This method gets a double[,] of analyte values out of an analytes feature table using the specified filename order and protein ID order
        /// to determine the rows and columns respectively.  
        /// </summary>
        /// <param name="analytesFT">The analytes feature table.  A feature table with a filename column, and values for all filename/proteinID pairs.</param>
        /// <param name="filenamesToGetInOrder">A list of filenames to get analyte values for, in order.</param>
        /// <param name="proteinIDsToGetInOrder">A list of proteinIDs to get analyte values for, in order.</param>
        /// <returns>A double[,] that contains the requested analyte values in the requested orders.  Rows correspond to filenames, columns correspond to proteinIDs.</returns>
        internal static double[,] GetAnalyteValueArray(FeatureTable analytesFT, List<string> filenamesToGetInOrder, List<string> proteinIDsToGetInOrder)
        {
            // Make return matrix.
            double[,] analyteValues = new double[filenamesToGetInOrder.Count, proteinIDsToGetInOrder.Count];

            int filenameIdx = 0;
            foreach (string filename in filenamesToGetInOrder)
            {
                // Find corresponding row in analytesFT
                DataRow dr = FeatureTableUtils.GetDataRowForFilename(analytesFT, filename);

                // If row does not exist, throw exception!
                if (dr == null)
                {
                    throw new Exception("Error: GetAnalyteValueArray - Missing filename " + filename +
                        " in analytes table. Please check your analytes table to ensure that all filenames in your feature table exist in the analytes table.");
                }

                // Get the values using the protein IDs as the order.
                int proteinIDIdx = 0;
                foreach (string proteinID in proteinIDsToGetInOrder)
                {
                    double analyteValue = Convert.ToDouble(dr[proteinID].ToString());
                    analyteValues[filenameIdx, proteinIDIdx] = analyteValue;
                    proteinIDIdx++;
                }

                filenameIdx++;
            }

            return analyteValues;
        }

        /// <summary>
        /// This method gets a double[,] of analyte ranks out of an analytes feature table using the specified filename order and protein ID order
        /// to determine the rows and columns respectively.  
        /// </summary>
        /// <param name="analytesFT">The analytes feature table.  A feature table with a filename column, and values for all filename/proteinID pairs.</param>
        /// <param name="filenamesToGetInOrder">A list of filenames to get analyte values for, in order.</param>
        /// <param name="proteinIDsToGetInOrder">A list of proteinIDs to get analyte values for, in order.</param>
        /// <returns>A double[,] that contains the requested analyte values in the requested orders.  Rows correspond to filenames, columns correspond to proteinIDs.</returns>
        internal static double[,] GetAnalyteRankArray(FeatureTable analytesFT, List<string> filenamesToGetInOrder, List<string> proteinIDsToGetInOrder)
        {
            // Make return matrix.
            double[,] analyteRanks = new double[filenamesToGetInOrder.Count, proteinIDsToGetInOrder.Count];

            //Imsl.Stat.Ranks r = new Imsl.Stat.Ranks();
            //r.TieBreaker = Imsl.Stat.Ranks.Tie.Average;

            int j = 0;
            foreach (string proteinID in proteinIDsToGetInOrder)
            {
                double[] analyteValuesToRank = new double[filenamesToGetInOrder.Count];

                int filenameIdx = 0;
                foreach (string filename in filenamesToGetInOrder)
                {
                    // Find corresponding row in analytesFT
                    DataRow dr = FeatureTableUtils.GetDataRowForFilename(analytesFT, filename);

                    // If row does not exist, throw exception!
                    if (dr == null)
                    {
                        throw new Exception("Error: GetAnalyteValueArray - Missing filename " + filename +
                            " in analytes table. Please check your analytes table to ensure that all filenames in your feature table exist in the analytes table.");
                    }

                    double analyteValue = Convert.ToDouble(dr[proteinID].ToString());

                    analyteValuesToRank[filenameIdx] = analyteValue;
                    filenameIdx++;
                }

                double[] rankedAnalyteValues = MathNet.Numerics.Statistics.Statistics.Ranks(analyteValuesToRank, MathNet.Numerics.Statistics.RankDefinition.Average);
                
                for (int i = 0; i < rankedAnalyteValues.Length; i++)
                {
                    analyteRanks[i, j] = rankedAnalyteValues[i];
                }

                j++;
            }
       
            return analyteRanks;
        }

        /// <summary>
        /// This method gets the rank values for the specified subset of rows of hte full analyte value matrix.  
        /// 
        /// For example, if we select the odd rows of the full analyte value matrix, then rank the values in each column
        /// for the odd rows from 1 ... to nRows / 2.
        /// </summary>
        /// <param name="rowIndices">The row indices to include in the ranking computation.</param>
        /// <param name="fullAnalyteValues">The full analyte value matrix.</param>
        /// <returns>The analyte rank values for the subset of rows specified.</returns>
        internal static double[,] GetAnalyteRankArrayForSubsetOfRows(int[] rowIndices, double[,] fullAnalyteValues)
        {
            // Array Dimensions
            int numRows = rowIndices.Length;
            int numCols = fullAnalyteValues.GetLength(1);

            // Make return matrix.
            double[,] analyteRanks = new double[numRows, numCols];
            
            for (int j = 0; j < numCols; j++)
            {
                // Assemble the values to rank
                double[] analyteValuesToRank = new double[numRows];

                int rowCounter = 0;
                foreach (int rowIndex in rowIndices)
                {
                    double analyteValue = fullAnalyteValues[rowIndex, j];
                    analyteValuesToRank[rowCounter] = analyteValue;
                    rowCounter++;
                }

                // Rank them
                double[] rankedAnalyteValues = MathNet.Numerics.Statistics.Statistics.Ranks(analyteValuesToRank, MathNet.Numerics.Statistics.RankDefinition.Average);

                // Copy into the return data structure
                for (int i = 0; i < numRows; i++)
                {
                    analyteRanks[i, j] = rankedAnalyteValues[i];
                }
            }

            return analyteRanks;
        }

        #endregion

        #region Enrichment Score, Enrichment Score P-Value Methods
        /// <summary>
        /// This method computes the p-value for an enrichment score that was computed using the sum method.  If the p-value 
        /// cannot be computed for some reason, -1000000.0 is returned.
        /// 
        /// This code was translated from matlab, C:\Biodesix\MatlabPrototypingCode\trunk\GSEA\EstimateES.m - line 105.
        /// </summary>
        /// <param name="esSumForAllRealizations">A double[] containing enrichment scores (sum-method) for all realizations.</param>
        /// <param name="enrichmentScoreSum">The measured enrichment score (sum-method) computed for the data set.</param>
        /// <returns>The P-Value for the measured enrichment score (sum-method).</returns>
        private static double ComputeEnrichmentScoreSumPValue(double[] esSumForAllRealizations, double enrichmentScoreSum)
        {
            double pValue = PSEAConstants.INVALID_ES_PVALUE;
            if (esSumForAllRealizations.Length != 0)
            {
                double sum = 0.0;
                for (int i = 0; i < esSumForAllRealizations.Length; i++)
                {
                    if (esSumForAllRealizations[i] >= enrichmentScoreSum)
                    {
                        sum += 1.0;
                    }
                }

                pValue = sum / esSumForAllRealizations.Length;
            }

            return pValue;
        }

        /// <summary>
        /// This method computes the p-value for an enrichment score that was computed using the max method.  If the p-value 
        /// cannot be computed for some reason, -1000000.0 is returned.
        /// 
        /// This code was translated from matlab, C:\Biodesix\MatlabPrototypingCode\trunk\GSEA\EstimateES.m - lines 100-104.
        /// </summary>
        /// <param name="esMaxForAllRealizations">A double[] containing enrichment scores (max-method) for all realizations.</param>
        /// <param name="enrichmentScoreMax">The measured enrichment score (max-method) computed for the data set.</param>
        /// <returns>The P-Value for the measured enrichment score (max-method).</returns>
        private static double ComputeEnrichmentScoreMaxPValue(double[] esMaxForAllRealizations, double enrichmentScoreMax)
        {
            // Initialize results
            double pValue = 0.0;
            double numeratorSum = 0.0;
            double denominatorSum = 0.0;

            // Compute numerator and denominator
            if (enrichmentScoreMax >= 0.0)
            {
                for (int i = 0; i < esMaxForAllRealizations.Length; i++)
                {
                    if (esMaxForAllRealizations[i] >= enrichmentScoreMax)
                    {
                        numeratorSum += 1.0;
                    }

                    if (esMaxForAllRealizations[i] >= 0.0)
                    {
                        denominatorSum += 1.0;
                    }
                }
            }
            else
            {
                for (int i = 0; i < esMaxForAllRealizations.Length; i++)
                {
                    if (esMaxForAllRealizations[i] <= enrichmentScoreMax)
                    {
                        numeratorSum += 1.0;
                    }

                    if (esMaxForAllRealizations[i] < 0.0)
                    {
                        denominatorSum += 1.0;
                    }
                }
            }

            // Compute P-Value
            if (denominatorSum != 0)
            {
                pValue = numeratorSum / denominatorSum;
            }
            else
            {
                pValue = PSEAConstants.INVALID_ES_PVALUE;
            }

            return pValue;
        }

        /// <summary>
        /// This method computes the Enrichment Scores (both max-method and sum-method) given an enrichment score running sum.
        /// 
        /// This implements a translation of matlab code - C:\Biodesix\MatlabPrototypingCode\trunk\GSEA\getESfromESrunningSum.m
        /// </summary>
        /// <param name="esRunningSum">The ES running sum double[] computed using ComputeESRunningSumForProteinSet.</param>
        /// <param name="enrichmentScoreMax">The output Enrichment Score (max-method).</param>
        /// <param name="enrichmentScoreSum">The output Enrichment Score (sum-method).</param>
        private static void ComputeEnrichmentScores(double[] esRunningSum, out double enrichmentScoreMax, out double enrichmentScoreSum)
        {
            double minimumValue = MathNet.Numerics.Statistics.ArrayStatistics.Minimum(esRunningSum);
            double maximumValue = MathNet.Numerics.Statistics.ArrayStatistics.Maximum(esRunningSum);

            if ((minimumValue < 0) && (maximumValue < 0))
            {
                enrichmentScoreSum = Math.Abs(minimumValue);
            }
            else if ((minimumValue > 0) && (maximumValue > 0))
            {
                enrichmentScoreSum = maximumValue;
            }
            else if ((minimumValue == 0) && (maximumValue == 0))
            {
                enrichmentScoreSum = 0.0;
            }
            else
            {
                enrichmentScoreSum = Math.Abs(maximumValue) + Math.Abs(minimumValue);
            }

            if (Math.Abs(minimumValue) >= Math.Abs(maximumValue))
            {
                enrichmentScoreMax = minimumValue;
            }
            else
            {
                enrichmentScoreMax = maximumValue;
            }
        }

        /// <summary>
        /// This method computes the Enrichment Score Running Sum for a given feature/protein set combination. 
        /// </summary>
        /// <param name="correlations">The double[] of correlations between the feature to analyze and every protein ID.</param>
        /// <param name="pValues">The double[] of p-values for the correlations between the feature to analyze and every protein ID.</param>
        /// <param name="allProteinIDListInOriginalOrder">The list of all protein IDs in the order retrieved from the protein sets table.</param>
        /// <param name="protIDList">The list of proteinIDs that are in the protein set to analyze.</param>
        /// <param name="normalizationExponent">The normalization exponent to use in computation of the normalization factor.</param>
        /// <param name="esResultsDataTable">The output data table containing the results of the running sum computation.</param>
        /// <returns>A double[] containing the running sum across all of the protein IDs.</returns>
        private static double[] ComputeESRunningSumForProteinSet(double[] correlations, double[] pValues,
            List<string> allProteinIDListInOriginalOrder, List<string> protIDList, double normalizationExponent, out DataTable esResultsDataTable)
        {
            // Compute the normalization factor.
            double normalizationFactor = ComputeNormalizationFactor(correlations, allProteinIDListInOriginalOrder, protIDList, normalizationExponent);

            // Next we have to sort the data in order of highest to lowest correlation.  
            // This includes sorting the protein IDs and p-values - to match Matlab for ties
            // put all of the data in a data table and use the data view to sort specifying a 
            // secondary sort column
            int numProteins = correlations.Length;
            double[] enrichmentScoreRunningSum = new double[numProteins];
            double[] sortedCorrelations = new double[numProteins];
            string[] sortedProteins = new string[numProteins];
            correlations.CopyTo(sortedCorrelations, 0);

            DataTable dt = new DataTable(ES_RUNNING_SUM_TABLE_NAME);
            DataColumn protCol = new DataColumn(ES_RUNNING_SUM_TABLE_PROT_ID_COL, typeof(string));
            DataColumn orderCol = new DataColumn(ES_RUNNING_SUM_TABLE_PROT_ORDER_COL, typeof(int));
            DataColumn corrCol = new DataColumn(ES_RUNNING_SUM_TABLE_CORRELATION_COL, typeof(double));
            DataColumn pValCol = new DataColumn(ES_RUNNING_SUM_TABLE_PVALUE_COL, typeof(double));
            DataColumn esRunningSumCol = new DataColumn(ES_RUNNING_SUM_TABLE_ESRS_COL, typeof(double));
            dt.Columns.Add(protCol);
            dt.Columns.Add(orderCol);
            dt.Columns.Add(corrCol);
            dt.Columns.Add(pValCol);
            dt.Columns.Add(esRunningSumCol);
            for (int i = 0; i < numProteins; i++)
            {
                DataRow dr = dt.NewRow();
                dr[ES_RUNNING_SUM_TABLE_PROT_ID_COL] = allProteinIDListInOriginalOrder[i];
                dr[ES_RUNNING_SUM_TABLE_PROT_ORDER_COL] = i;
                dr[ES_RUNNING_SUM_TABLE_CORRELATION_COL] = correlations[i];
                dr[ES_RUNNING_SUM_TABLE_PVALUE_COL] = pValues[i];
                dt.Rows.Add(dr);
            }

            // Sort the data first by correlation, second by protein order
            DataView dv = dt.DefaultView;
            dv.Sort = ES_RUNNING_SUM_TABLE_CORRELATION_COL + " DESC, " + ES_RUNNING_SUM_TABLE_PROT_ORDER_COL + " ASC";
            DataTable sortedTable = dv.ToTable();

            // Extract the sorted data back into double[]
            for (int i = 0; i < numProteins; i++)
            {
                sortedCorrelations[i] = Convert.ToDouble(sortedTable.Rows[i][ES_RUNNING_SUM_TABLE_CORRELATION_COL].ToString());
                sortedProteins[i] = sortedTable.Rows[i][ES_RUNNING_SUM_TABLE_PROT_ID_COL].ToString();
            }

            // Compute the running sum, stashing the results away in the results table and in the double[] to return
            double runningSum = 0.0;
            for (int i = 0; i < numProteins; i++)
            {
                string thisProtein = sortedProteins[i];
                if (protIDList.Contains(thisProtein))
                {
                    runningSum += (Math.Pow(Math.Abs(sortedCorrelations[i]), normalizationExponent) / normalizationFactor);
                }
                else
                {
                    runningSum += (-1.0 / (numProteins - protIDList.Count));
                }

                enrichmentScoreRunningSum[i] = runningSum;
                sortedTable.Rows[i][ES_RUNNING_SUM_TABLE_ESRS_COL] = runningSum;
            }

            esResultsDataTable = sortedTable;

            return enrichmentScoreRunningSum;
        }

        /// <summary>
        /// This method computes the normalization factor to use when computing the enrichment score running sum.
        /// 
        /// This code is translated from the matlab code - C:\Biodesix\MatlabPrototypingCode\trunk\GSEA\calculateNormFactors.m
        /// </summary>
        /// <param name="correlations">A double[] of correlations between a feature and all protein IDs.</param>
        /// <param name="allProteinIDs">The list of all protein IDs.</param>
        /// <param name="proteinIDsForProteinSet">The list of protein IDs that belong to the protein set being analyzed.</param>
        /// <param name="normalizationExponent">The normalization exponent.</param>
        /// <returns>The normalization factor.</returns>
        internal static double ComputeNormalizationFactor(double[] correlations, List<string> allProteinIDs,
            List<string> proteinIDsForProteinSet, double normalizationExponent)
        {
            double retval = 0.0;

            foreach (string proteinID in proteinIDsForProteinSet)
            {
                int indexOfProtID = allProteinIDs.IndexOf(proteinID);
                double correlation = correlations[indexOfProtID];
                retval += Math.Abs(Math.Pow(correlation, normalizationExponent));
            }

            return retval;
        }
        #endregion

        #region Random Shuffling / Permutation Methods
        /// <summary>
        /// This method returns a double[] that is a randomly shuffled version of the input data array, using the
        /// provided random number generator.  This implements a Fisher-Yates shuffle.
        /// </summary>
        /// <param name="data">Data to shuffle.</param>
        /// <param name="rng">Random number generator to use.</param>
        /// <returns>Shuffled data.</returns>
        public static double[] ShuffleDoubles(double[] data, Random rng)
        {
            int n = data.Length;
            double[] shuffledData = new double[n];
            data.CopyTo(shuffledData, 0);
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                double value = shuffledData[k];
                shuffledData[k] = shuffledData[n];
                shuffledData[n] = value;
            }
            return shuffledData;
        }

        /// <summary>
        /// This method returns an int[] that is a random permutation vector of the specified length. It uses the
        /// provided random number generator.  This implements a Fisher-Yates shuffle.
        /// </summary>
        /// <param name="lengthOfPermutationArray">The length of the permutation vector to create.</param>
        /// <param name="rng">Random number generator to use.</param>
        /// <returns>Permutation vector that contains the numbers [0, lengthOfPermutationArray-1] shuffled.</returns>
        public static int[] GetPermutationArray(int lengthOfPermutationArray, Random rng)
        {
            int n = lengthOfPermutationArray;
            int[] shuffledData = new int[n];
            for (int i = 0; i < lengthOfPermutationArray; i++)
            {
                shuffledData[i] = i;
            }

            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                int value = shuffledData[k];
                shuffledData[k] = shuffledData[n];
                shuffledData[n] = value;
            }
            return shuffledData;
        }
        #endregion

        #region Correlations and P-Values for Correlations
        /// <summary>
        /// This method computes the correlations and associated p-values for the correlations between a binary feature and 
        /// all proteins in the analytes data. The output correlations and p-values are in the order of the protein columns in
        /// the analytes data.  
        /// 
        /// NOTE - THIS IS SLOW and not used any longer.  The Wilcoxon rank sum sorts the data, so if this 
        /// is used inside of a loop, the data is sorted and ranked every single time.
        /// </summary>
        /// <param name="group0Indices">The indices of the rows in the analytes data that correspond to group0 in the analysis.</param>
        /// <param name="group1Indices">The indices of the rows in the analytes data that correspond to group1 in the analysis.</param>
        /// <param name="analyteValuesForFilesInFT">A double[,] of analyte values, rows correspond to filenames, columns correspond to proteinIDs.</param>
        /// <param name="correlations">The double[] of output correlations, in the order of the columns of the analyte values.</param>
        /// <param name="pValues">The double[] of output p-values associated with the correlations.</param>
        internal static void ComputeBinaryCorrelationsAndPValues(List<int> group0Indices, List<int> group1Indices, double[,] analyteValuesForFilesInFT, out double[] correlations, out double[] pValues)
        {
            // Set up the data.
            int numberOfProteins = analyteValuesForFilesInFT.GetLength(1);
            int numberOfSamples = group0Indices.Count + group1Indices.Count;
            correlations = new double[numberOfProteins];
            pValues = new double[numberOfProteins];

            // For each protein, split the data into group0, group1 and compute the correlation, p-value 
            for (int j = 0; j < numberOfProteins; j++)
            {
                double[] group0Values = new double[group0Indices.Count];
                double[] group1Values = new double[group1Indices.Count];

                int counter = 0;
                foreach (int idx in group0Indices)
                {
                    group0Values[counter] = analyteValuesForFilesInFT[idx, j];
                    counter++;
                }

                counter = 0;
                foreach (int idx in group1Indices)
                {
                    group1Values[counter] = analyteValuesForFilesInFT[idx, j];
                    counter++;
                }

                int n0 = group0Indices.Count;
                int n1 = group1Indices.Count;
                
                // Compute the correlation using ranksum since this is a binary feature
                double ranksum = 0.0;
                for (int i = 0; i < n0; i++)
                {
                    ranksum += group0Values[i];
                }
                double correlation = (2.0 * ranksum - (n0 * n1 + n0 * (n0 + 1.0))) / (n0 * n1); // (2.*genes_correlations - (n1*n2+n1*(n1+1)))./(n1*n2);
                double std = Math.Sqrt((n0 * n1 * (numberOfSamples + 1.0)) / 12.0);
                double mean0 = 0.5 * n0 * (numberOfSamples + 1.0);
                double zScore0 = (ranksum - mean0) / std;
                correlations[j] = correlation;
                pValues[j] = 2.0 * (1.0 - MathNet.Numerics.Distributions.Normal.CDF(0.0, 1.0, Math.Abs(zScore0))); // Two-sided pvalue for the standard normal score of the ranksum
            }

        }

        /// <summary>
        /// This method computes the correlations and associated p-values for the correlations between a binary feature and 
        /// all proteins in the analytes data. The output correlations and p-values are in the order of the protein columns in
        /// the analyte rank data.
        /// </summary>
        /// <param name="group0Indices">The indices of the rows in the analytes data that correspond to group0 in the analysis.</param>
        /// <param name="group1Indices">The indices of the rows in the analytes data that correspond to group1 in the analysis.</param>
        /// <param name="analyteRanks">A double[,] of analyte value ranks, rows correspond to filenames, columns correspond to proteinIDs.</param>
        /// <param name="correlations">The double[] of output correlations, in the order of the columns of the analyte values.</param>
        /// <param name="pValues">The double[] of output p-values associated with the correlations.</param>
        internal static void ComputeBinaryRankCorrelationsAndPValues(List<int> group0Indices, List<int> group1Indices, double[,] analyteRanks, out double[] correlations, out double[] pValues)
        {
            // Set up the data.
            int numberOfProteins = analyteRanks.GetLength(1);
            int numberOfSamples = group0Indices.Count + group1Indices.Count;
            correlations = new double[numberOfProteins];
            pValues = new double[numberOfProteins];

            // For each protein, split the data into group0, group1 and compute the correlation, p-value 
            for (int j = 0; j < numberOfProteins; j++)
            {
                double[] group0Values = new double[group0Indices.Count];
                double[] group1Values = new double[group1Indices.Count];

                int counter = 0;
                foreach (int idx in group0Indices)
                {
                    group0Values[counter] = analyteRanks[idx, j];
                    counter++;
                }

                counter = 0;
                foreach (int idx in group1Indices)
                {
                    group1Values[counter] = analyteRanks[idx, j];
                    counter++;
                }

                int n0 = group0Indices.Count;
                int n1 = group1Indices.Count;

                // Compute the correlation using ranksum since this is a binary feature
                double ranksum = 0.0;
                for (int i = 0; i < n0; i++)
                {
                    ranksum += group0Values[i];
                }
                double correlation = (2.0 * ranksum - (n0 * n1 + n0 * (n0 + 1.0))) / (n0 * n1); // (2.*genes_correlations - (n1*n2+n1*(n1+1)))./(n1*n2);
                double std = Math.Sqrt((n0 * n1 * (numberOfSamples + 1.0)) / 12.0);
                double mean0 = 0.5 * n0 * (numberOfSamples + 1.0);
                double zScore0 = (ranksum - mean0) / std;
                correlations[j] = correlation;
                pValues[j] = 2.0 * (1.0 - MathNet.Numerics.Distributions.Normal.CDF(0.0, 1.0, Math.Abs(zScore0))); // Two-sided pvalue for the standard normal score of the ranksum
            }

        }

        /// <summary>
        /// This method computes the correlations and associated p-values between a continuous feature and each protein in the analyte values 
        /// table.  This is a Spearman correlation, which is a PearsonsR value computed on the ranked feature value data.
        /// 
        /// This code is translated from matlab - C:\Biodesix\MatlabPrototypingCode\trunk\GSEA\getGeneCorrelationsContinuous.m
        /// </summary>
        /// <param name="featureValues">The double[] of feature values for the feature to analyze.</param>
        /// <param name="analyteValuesForFilesInFT">The double[,] of analyte values, rows correspond to filenames, columns correspond to protein IDs.</param>
        /// <param name="correlations">The output double[] of correlations, one per proteinID.</param>
        /// <param name="pValues">The output double[] of p-values, one per correlation.</param>
        internal static void ComputeContinuousCorrelationsAndPValues(double[] featureValues, double[,] analyteValuesForFilesInFT, out double[] correlations, out double[] pValues)
        {
            // Set up the data
            int numberOfProteins = analyteValuesForFilesInFT.GetLength(1);
            int numberOfSamples = analyteValuesForFilesInFT.GetLength(0);
            correlations = new double[numberOfProteins];
            pValues = new double[numberOfProteins];
            
            // For each protein in the analytes data - compute the correlation and p-value
            for (int j = 0; j < numberOfProteins; j++)
            {
                // get the analyte values for this protein
                double[] analyteValues = new double[numberOfSamples];
                for (int i = 0; i < numberOfSamples; i++)
                {
                    analyteValues[i] = analyteValuesForFilesInFT[i, j];
                }

                //// Rank the data and compute a pearsonsR correlation (equivalent to Spearmans correlation in matlab)
                //Imsl.Stat.Ranks r = new Imsl.Stat.Ranks();
                //double[] rankedX = r.GetRanks(featureValues);
                //double[] rankedY = r.GetRanks(analyteValues);
                //List<double> rankedXList = new List<double>(rankedX);
                //List<double> rankedYList = new List<double>(rankedY);

                // Rank the data and compute a pearsonsR correlation (equivalent to Spearmans correlation in matlab)
                double[] rankedX = MathNet.Numerics.Statistics.Statistics.Ranks(featureValues, MathNet.Numerics.Statistics.RankDefinition.Average);
                double[] rankedY = MathNet.Numerics.Statistics.Statistics.Ranks(analyteValues, MathNet.Numerics.Statistics.RankDefinition.Average);
                List<double> rankedXList = new List<double>(rankedX);
                List<double> rankedYList = new List<double>(rankedY);

                double spearmansRho = Local.Util.PearsonsRValue.ComputePearsonsR(rankedXList, rankedYList);
                correlations[j] = spearmansRho;

                //// Compute the 2-sided p-value for spearmans correlation using t score
                //double tScore = Math.Abs(spearmansRho * Math.Sqrt((numberOfSamples - 2) / (1 - Math.Pow(spearmansRho, 2.0))));
                //double pValue = 2.0 * (1.0 - Imsl.Stat.Cdf.StudentsT(tScore, (double)(numberOfSamples - 2)));

                //pValues[j] = pValue;
                // Compute the 2-sided p-value for spearmans correlation using t score
                double tScore = Math.Abs(spearmansRho * Math.Sqrt((numberOfSamples - 2) / (1 - Math.Pow(spearmansRho, 2.0))));
                double pValue = 2.0 * (1.0 - MathNet.Numerics.Distributions.StudentT.CDF(0.0, 1.0, numberOfSamples - 2, tScore));
                pValues[j] = pValue;
            }
        }
        #endregion

        #region Utility Methods to Save Plots as .PNG
        /// <summary>
        /// This method creates and saves an enrichment score distribution plot given the enrichment score for all realizations
        /// and the measured enrichment score.  The filename to save to is provided as input, the feature name and protein set are 
        /// passed as input to label the plot. The enrichment score method is also provided to label the plot.  The plot is a histogram
        /// of all of the enrichement scores for the random realizations, with a red line showing the measured enrichment score.
        /// </summary>
        /// <param name="esForAllRealizations">The double[] of enrichment scores for all realizations.</param>
        /// <param name="esMeasuredValue">The measured enrichment score.</param>
        /// <param name="esMethod">The method (ESSum or ESMax) used to compute the enrichment score and statistics.</param>
        /// <param name="filename">The filename to save the plot .PNG to.</param>
        /// <param name="featureName">The feature name that was analyzed.</param>
        /// <param name="proteinSetName">The name of the protein set that was analyzed.</param>
        private static void CreateAndSaveESDistributionPlot(double[] esForAllRealizations, double esMeasuredValue, string esMethod,
            string filename, string featureName, string proteinSetName)
        {

            double min = -1.0;
            double max = 1.0;
            int totalBuckets = 40;

            double bucketSize = (max - min) / totalBuckets;
            // Get data
            double[] xValues = new double[totalBuckets];

            for (int i = 0; i < totalBuckets; i++)
            {
                xValues[i] = -0.975 + i * bucketSize;
            }

            double[] buckets = new double[totalBuckets];

            foreach (double value in esForAllRealizations)
            {
                int bucketIndex = 0;
                if (bucketSize > 0.0)
                {
                    bucketIndex = (int)((value - min) / bucketSize);
                    if (bucketIndex == totalBuckets)
                    {
                        bucketIndex--;
                    }
                }
                buckets[bucketIndex]++;
            }

            //// Scale the number of major and minor ticks to be reasonable.
            //double maxBucketSize = Imsl.Stat.Summary.GetMaximum(buckets);
            //double majorStep = ((int)Math.Round(maxBucketSize / 40.0)) * 10.0;

            // Scale the number of major and minor ticks to be reasonable.
            double maxBucketSize = MathNet.Numerics.Statistics.ArrayStatistics.Maximum(buckets);
            double majorStep = ((int)Math.Round(maxBucketSize / 40.0)) * 10.0;

            // Modified here to make sure that the major step and minor step are not set to 0.0!
            if (majorStep == 0.0)
            {
                majorStep = maxBucketSize;
            }
            double minorStep = majorStep / 4.0;

            ZedGraphControl zgc = new ZedGraphControl();
            ZedGraph.GraphPane pane = zgc.GraphPane;

            // Set pane properties
            pane.GraphObjList.Clear();
            pane.CurveList.Clear();
            pane.Legend.IsVisible = true;
            pane.Legend.Position = ZedGraph.LegendPos.BottomCenter;
            pane.XAxis.Scale.Min = -1.0;
            if (esMethod.Equals(ES_SUM))
            {
                pane.XAxis.Scale.Min = 0.0;
            }
            pane.XAxis.Scale.Max = 1.0;
            pane.XAxis.Scale.MajorStep = 0.2;
            pane.XAxis.Scale.MinorStep = 0.05;
            pane.XAxis.Scale.Format = "F1";
            pane.YAxis.Scale.Min = 0.0;
            pane.YAxis.Scale.MajorStep = majorStep;
            pane.YAxis.Scale.MinorStep = minorStep;
            pane.YAxis.Scale.Format = "F1";
            pane.YAxis.Scale.Max = maxBucketSize;
            pane.XAxis.Type = AxisType.Linear;
            pane.BarSettings.MinClusterGap = 0.0f;
            pane.BarSettings.ClusterScaleWidth = 0.05f;

            //set the title and axis information
            pane.Title.Text = "Null Distribution of " + esMethod + " for Feature " + featureName + ", Protein Set " + proteinSetName;
            pane.XAxis.Title.Text = esMethod;
            pane.YAxis.Title.Text = "Counts";

            // Add the real ES
            double[] xES = { esMeasuredValue, esMeasuredValue };
            double[] yES = { pane.YAxis.Scale.Min, pane.YAxis.Scale.Max };
            LineItem li = pane.AddCurve(esMethod + " Measured", xES, yES, Color.Red, SymbolType.None);
            li.Line.Width = 8.0f;

            // Add the points, line is visible, points are not
            BarItem bi = pane.AddBar("Counts(" + esMethod + ")", xValues, buckets, Color.Blue);

            // Save the plot
            pane.GetImage(4800, 3600, 300).Save(filename, System.Drawing.Imaging.ImageFormat.Png);
        }

        /// <summary>
        /// This method saves the .PNG file that contains the plot for a running sum.  A running sum plot plots the data from the running
        /// sum input as the y-value using the index as the x-value.  The filename to save to is provided as input.  The feature name 
        /// and protein set name for the analysis are passed in to label the plot accurately.
        /// </summary>
        /// <param name="esRunningSum">The running sum double[] to plot.</param>
        /// <param name="filename">The filename to save the .PNG to.</param>
        /// <param name="featureName">The name of the feature used in analysis.</param>
        /// <param name="proteinSetName">The name of the protein set used in analysis.</param>
        private static void CreateAndSaveRunningSumPlot(double[] esRunningSum, string filename, string featureName, string proteinSetName)
        {
            // Contrived x data - just ordinal data.
            double[] xValues = new double[esRunningSum.Length];
            for (int i = 0; i < esRunningSum.Length; i++)
            {
                xValues[i] = i;
            }

            ZedGraphControl zgc = new ZedGraphControl();
            ZedGraph.GraphPane pane = zgc.GraphPane;

            // Set pane properties
            pane.GraphObjList.Clear();
            pane.CurveList.Clear();
            pane.Legend.IsVisible = false;
            pane.XAxis.Scale.Min = 0;
            pane.XAxis.Scale.Max = esRunningSum.Length;
            pane.XAxis.Scale.MajorStep = 200.0;
            pane.XAxis.Scale.MinorStep = 50.0;
            pane.XAxis.MajorGrid.IsVisible = true;
            pane.XAxis.MinorGrid.IsVisible = true;
            pane.XAxis.Scale.Format = "F0";
            pane.YAxis.Scale.Min = MathNet.Numerics.Statistics.ArrayStatistics.Minimum(esRunningSum);
            pane.YAxis.Scale.Max = MathNet.Numerics.Statistics.ArrayStatistics.Maximum(esRunningSum);
            pane.YAxis.MajorGrid.IsVisible = true;
            pane.YAxis.MinorGrid.IsVisible = true;
            pane.XAxis.Type = AxisType.Linear;


            //set the title and axis information
            pane.Title.Text = "ES Running Sum for Feature " + featureName + ", Protein Set " + proteinSetName;
            pane.XAxis.Title.Text = "Somamer Index";
            pane.YAxis.Title.Text = "ES(Index)";

            // Add the points, line is visible, points are not
            LineItem esRunningSumLI = pane.AddCurve("ES Running Sum", xValues, esRunningSum, Color.Black, SymbolType.None);
            esRunningSumLI.Line.Width = 8.0f;

            // Save the plot
            pane.GetImage(4800, 3600, 300).Save(filename, System.Drawing.Imaging.ImageFormat.Png);
        }
        #endregion


        /// <summary>
        /// This method computes the enrichment score for binary variables including the data split strategy.  The 
        /// data split strategy divides the input data in 1/2 randomly some number of times.  The split indices are
        /// specified in the splits input list of integer arrays - each integer array contains the indices that are 
        /// part of that particular data split.
        /// </summary>
        /// <param name="splits">List of integer arrays that define the row indices for the binary labels and analytes table for each split.</param>
        /// <param name="group0Indices">The row indices that belong to group 0.</param>
        /// <param name="group1Indices">The row indices that belong to group 1.</param>
        /// <param name="analyteRankValuesPerSplit">The analyte rank values (ranked protein value for a given protein or column) table per data split.</param>
        /// <param name="allProteinIDList">List of all of the protein IDs.</param>
        /// <param name="proteinIDsInProteinSet">List of the protein IDs in the protein set being analyzed.</param>
        /// <returns>A two-tuple that contains the enrichment score (ES) as Item1 and p-value for the ES as Item2.</returns>
        private static Tuple<double, double> ComputeBinaryEnrichmentScoreWithSplits(
            List<int[]> splits, List<int> group0Indices, List<int> group1Indices, List<double[,]> analyteRankValuesPerSplit, 
            List<string> allProteinIDList, List<string> proteinIDsInProteinSet)
        {
            double[] esScoreMaxes = new double[splits.Count];
            double[] esScoreSums = new double[splits.Count];

            for (int splitIdx = 0; splitIdx < splits.Count; splitIdx++)
            {
                int[] currentSplit = splits[splitIdx];
                double[,] analyteRankValuesThisSplit = analyteRankValuesPerSplit[splitIdx];

                #region Compute Corellations and PValues
                double[] correlationsThisSplit = null;
                double[] pValuesThisSplit = null;
                List<int> group0IndicesThisSplit = new List<int>();
                List<int> group1IndicesThisSplit = new List<int>();

                int rowCounter = 0;
                foreach (int idx in currentSplit)
                {
                    if (group0Indices.Contains(idx))
                    {
                        group0IndicesThisSplit.Add(rowCounter);
                        rowCounter++;
                    }
                    else if (group1Indices.Contains(idx))
                    {
                        group1IndicesThisSplit.Add(rowCounter);
                        rowCounter++;
                    }
                }

                // Compute the rank correlations!
                PSEAUtils.ComputeBinaryRankCorrelationsAndPValues(group0IndicesThisSplit, group1IndicesThisSplit, analyteRankValuesThisSplit, out correlationsThisSplit, out pValuesThisSplit);
                #endregion

                #region Compute the Enrichment Score (ES) Running Sum
                DataTable esRunningSumResultsTable = null;

                double[] esRunningSum = ComputeESRunningSumForProteinSet(correlationsThisSplit, pValuesThisSplit, allProteinIDList,
                    proteinIDsInProteinSet, NORMALIZATION_EXPONENT, out esRunningSumResultsTable);
                #endregion

                // Compute the Enrichment Scores (max and sum methods) - add those to the arrays
                double esScoreMax = 0.0;
                double esScoreSum = 0.0;
                ComputeEnrichmentScores(esRunningSum, out esScoreMax, out esScoreSum);

                esScoreMaxes[splitIdx] = esScoreMax;
                esScoreSums[splitIdx] = esScoreSum;
            }


            // Compute the Avg. Enrichment Scores (max and sum methods) - add those to the results
            double enrichmentScoreMax = MathNet.Numerics.Statistics.ArrayStatistics.Mean(esScoreMaxes);
            double enrichmentScoreSum = MathNet.Numerics.Statistics.ArrayStatistics.Mean(esScoreSums);

            Tuple<double, double> retval = new Tuple<double, double>(enrichmentScoreMax, enrichmentScoreSum);
            return retval;
        }

        /// <summary>
        /// This method computes the enrichment score for continuous variables including the data split strategy.  The 
        /// data split strategy divides the input data in 1/2 randomly some number of times.  The split indices are
        /// specified in the splits input list of integer arrays - each integer array contains the indices that are 
        /// part of that particular data split.
        /// </summary>
        /// <param name="splits">List of integer arrays that define the row indices for the binary labels and analytes table for each split.</param>
        /// <param name="featureValues">The feature values for the feature being analyzed.</param>
        /// <param name="analyteValues">The analyte (protein value) feature table.</param>
        /// <param name="allProteinIDList">List of all of the protein IDs.</param>
        /// <param name="proteinIDsInProteinSet">List of the protein IDs in the protein set being analyzed.</param>
        /// <returns>A two-tuple that contains the enrichment score (ES) as Item1 and p-value for the ES as Item2.</returns>
        private static Tuple<double, double> ComputeContinuousEnrichmentScoreWithSplits(
            List<int[]> splits, double[] featureValues, double[,] analyteValues,
            List<string> allProteinIDList, List<string> proteinIDsInProteinSet)
        {
            double[] esScoreMaxes = new double[splits.Count];
            double[] esScoreSums = new double[splits.Count];

            for (int splitIdx = 0; splitIdx < splits.Count; splitIdx++)
            {
                int[] currentSplit = splits[splitIdx];

                #region Compute Corellations and PValues
                double[] correlationsThisSplit = null;
                double[] pValuesThisSplit = null;
                double[] featureValuesThisSplit = new double[currentSplit.Length];
                double[,] analyteValuesThisSplit = new double[currentSplit.Length, analyteValues.GetLength(1)];

                int rowCounter = 0;
                foreach (int idx in currentSplit)
                {
                    featureValuesThisSplit[rowCounter] = featureValues[idx];
                    for (int j = 0; j < analyteValues.GetLength(1); j++)
                    {
                        analyteValuesThisSplit[rowCounter, j] = analyteValues[idx, j];
                    }
                    rowCounter++;
                }

                PSEAUtils.ComputeContinuousCorrelationsAndPValues(featureValuesThisSplit, analyteValuesThisSplit, out correlationsThisSplit, out pValuesThisSplit);
                #endregion

                #region Compute the Enrichment Score (ES) Running Sum
                DataTable esRunningSumResultsTable = null;

                double[] esRunningSum = ComputeESRunningSumForProteinSet(correlationsThisSplit, pValuesThisSplit, allProteinIDList,
                    proteinIDsInProteinSet, NORMALIZATION_EXPONENT, out esRunningSumResultsTable);
                #endregion
                
                // Compute the Enrichment Scores (max and sum methods) - add those to the arrays
                double esScoreMax = 0.0;
                double esScoreSum = 0.0;
                ComputeEnrichmentScores(esRunningSum, out esScoreMax, out esScoreSum);

                esScoreMaxes[splitIdx] = esScoreMax;
                esScoreSums[splitIdx] = esScoreSum;
            }

            //// Compute the Avg. Enrichment Scores (max and sum methods) - add those to the results
            //double enrichmentScoreMax = Imsl.Stat.Summary.GetMean(esScoreMaxes);
            //double enrichmentScoreSum = Imsl.Stat.Summary.GetMean(esScoreSums);
            // Compute the Avg. Enrichment Scores (max and sum methods) - add those to the results
            double enrichmentScoreMax = MathNet.Numerics.Statistics.ArrayStatistics.Mean(esScoreMaxes);
            double enrichmentScoreSum = MathNet.Numerics.Statistics.ArrayStatistics.Mean(esScoreSums);

            Tuple<double, double> retval = new Tuple<double, double>(enrichmentScoreMax, enrichmentScoreSum);
            return retval;
        }

        /// <summary>
        /// This method performs binary PSEA using the data split strategy where the input data is split 50/50 randomly some number of 
        /// times.  The splits input specifies the row indices to be used in each split.
        /// </summary>
        /// <param name="splits">List of integer arrays that define the data splits.  The integer arrays contain the row indices of the data to be used in a particular split.</param>
        /// <param name="perms">List of integer arrays that define the random permutations to use when computing an estimate of the null distribution.</param>
        /// <param name="group0Indices">The row indices of the rows that belong to group 0.</param>
        /// <param name="group1Indices">The row indices of the rows that belong to group 1.</param>
        /// <param name="analyteRankValuesPerSplit">The analyte rank values (ranks of the protein values for a given protein or column) per split.</param>
        /// <param name="phenotypeLabelToAnalyze">The name of the binary label being analyzed.</param>
        /// <param name="proteinSetToAnalyze">The name of the protein set being analyzed.</param>
        /// <param name="allProteinIDList">The list of all of the protein IDs in the analytes table.</param>
        /// <param name="proteinIDsInProteinSet">The list of the protein IDs in this particular protein set being analyzed.</param>
        /// <param name="resultsDataDirectory">The path to the directory to output results.</param>
        /// <returns>A dictionary, with named keys, that contains the results of this operation.</returns>
        internal static Dictionary<string, object> PerformBinaryPSEAWithSplits(List<int[]> splits, List<int[]> perms, List<int> group0Indices, List<int> group1Indices, List<double[,]> analyteRankValuesPerSplit, string phenotypeLabelToAnalyze, string proteinSetToAnalyze, List<string> allProteinIDList, List<string> proteinIDsInProteinSet, string resultsDataDirectory)
        {
            // Create the results object and add the two known values right away (feature name and protein set name)
            Dictionary<string, object> retval = new Dictionary<string, object>();
            retval.Add(RESULTS_FEATURE_NAME_KEY, phenotypeLabelToAnalyze);
            retval.Add(RESULTS_PROTEIN_SET_NAME_KEY, proteinSetToAnalyze);

            // Compute the Avg. Enrichment Scores (max and sum methods) for the data
            Tuple<double, double> enrichmentScores = ComputeBinaryEnrichmentScoreWithSplits(splits, group0Indices, group1Indices, analyteRankValuesPerSplit, allProteinIDList, proteinIDsInProteinSet);
            double enrichmentScoreMax = enrichmentScores.Item1;
            double enrichmentScoreSum = enrichmentScores.Item2;
            retval.Add(RESULTS_ES_MAX_KEY, enrichmentScoreMax);
            retval.Add(RESULTS_ES_SUM_KEY, enrichmentScoreSum);

            // Compute p-values for enrichment scores by doing the realizations to elicit the null distribution
            int numRealizations = perms.Count;
            double[] esMaxForAllRealizations = new double[numRealizations];
            double[] esSumForAllRealizations = new double[numRealizations];

            // Perform the realizations based on the permutations contained in the input data
            for (int i = 0; i < numRealizations; i++)
            {
                #region Perform Realization
                // For each realization, 
                // 1. Shuffle all of the data
                // 2. Recompute group0 and group1 indices
                // 3. Recompute correlations and pvalues
                // 4. Recompute ES Running Sum
                // 5. Recompute ES (max and sum)
                int[] permutationArray = perms[i];
                List<int> group0IndicesShuffled = new List<int>();
                foreach (int idx in group0Indices)
                {
                    group0IndicesShuffled.Add(permutationArray[idx]);
                }

                List<int> group1IndicesShuffled = new List<int>();
                foreach (int idx in group1Indices)
                {
                    group1IndicesShuffled.Add(permutationArray[idx]);
                }

                Tuple<double, double> enrichmentScoresThisRealization = ComputeBinaryEnrichmentScoreWithSplits(splits, group0IndicesShuffled, group1IndicesShuffled, analyteRankValuesPerSplit, allProteinIDList, proteinIDsInProteinSet);
                double enrichmentScoreMaxThisRealization = enrichmentScoresThisRealization.Item1;
                double enrichmentScoreSumThisRealization = enrichmentScoresThisRealization.Item2;

                esMaxForAllRealizations[i] = enrichmentScoreMaxThisRealization;
                esSumForAllRealizations[i] = enrichmentScoreSumThisRealization;
                #endregion
            }

            // Compute the PValues for max and sum - add them to the results dictionary
            double esMaxPValue = ComputeEnrichmentScoreMaxPValue(esMaxForAllRealizations, enrichmentScoreMax);
            double esSumPValue = ComputeEnrichmentScoreSumPValue(esSumForAllRealizations, enrichmentScoreSum);
            retval.Add(RESULTS_PVALUE_MAX_KEY, esMaxPValue);
            retval.Add(RESULTS_PVALUE_SUM_KEY, esSumPValue);

            #region Save Plots for Distributions
            string esMaxDistPlotFilename = resultsDataDirectory + "\\PSEA_" + phenotypeLabelToAnalyze + "_" + proteinSetToAnalyze + "_ESMaxDist.png";
            CreateAndSaveESDistributionPlot(esMaxForAllRealizations, enrichmentScoreMax, ES_MAX, esMaxDistPlotFilename, phenotypeLabelToAnalyze, proteinSetToAnalyze);
            string esSumDistPlotFilename = resultsDataDirectory + "\\PSEA_" + phenotypeLabelToAnalyze + "_" + proteinSetToAnalyze + "_ESSumDist.png";
            CreateAndSaveESDistributionPlot(esSumForAllRealizations, enrichmentScoreSum, ES_SUM, esSumDistPlotFilename, phenotypeLabelToAnalyze, proteinSetToAnalyze);
            #endregion

            #region Write ESNulls .csv file
            DataTable esNullsTable = new DataTable();
            esNullsTable.Columns.Add("ESMax");
            esNullsTable.Columns.Add("ESSum");

            for (int i = 0; i < numRealizations; i++)
            {
                DataRow dr = esNullsTable.NewRow();
                dr["ESMax"] = esMaxForAllRealizations[i];
                dr["ESSum"] = esSumForAllRealizations[i];
                esNullsTable.Rows.Add(dr);
            }

            string theESNullsFileName = resultsDataDirectory + "\\PSEA_" + phenotypeLabelToAnalyze + "_" + proteinSetToAnalyze + "_ESNulls.csv";
            string[,] dtAsArray2 = TwoDimensionalStringArrayUtils.ConvertDataTableTo2DString(esNullsTable);
            TwoDimensionalStringArrayUtils.WriteCSVFile(dtAsArray2, theESNullsFileName);
            #endregion

            return retval;
        }

        /// <summary>
        /// This method performs continuous PSEA using the data split strategy where the input data is split 50/50 randomly some number of 
        /// times.  The splits input specifies the row indices to be used in each split.
        /// </summary>
        /// <param name="splits">List of integer arrays that define the data splits.  The integer arrays contain the row indices of the data to be used in a particular split.</param>
        /// <param name="perms">List of integer arrays that define the random permutations to use when computing an estimate of the null distribution.</param>
        /// <param name="featureValues">The feature values for the feature being analyzed.</param>
        /// <param name="analyteValues">The analyte (protein values) feature table.</param>
        /// <param name="featureName">The name of the feature being analyzed.</param>
        /// <param name="proteinSetToAnalyze">The name of the protein set being analyzed.</param>
        /// <param name="allProteinIDList">The list of all of the protein IDs in the analytes table.</param>
        /// <param name="proteinIDsInProteinSet">The list of the protein IDs in this particular protein set being analyzed.</param>
        /// <param name="resultsDataDirectory">The path to the directory to output results.</param>
        /// <returns>A dictionary, with named keys, that contains the results of this operation.</returns>
        internal static Dictionary<string, object> PerformContinuousPSEAWithSplits(
            List<int[]> splits, List<int[]> perms, double[] featureValues, double[,] analyteValues, string featureName, string proteinSetToAnalyze, List<string> allProteinIDList, List<string> proteinIDsInProteinSet, string resultsDataDirectory)
        {
            // Create the results object and add the two known values right away (feature name and protein set name)
            Dictionary<string, object> retval = new Dictionary<string, object>();
            retval.Add(RESULTS_FEATURE_NAME_KEY, featureName);
            retval.Add(RESULTS_PROTEIN_SET_NAME_KEY, proteinSetToAnalyze);

            // Compute the Avg. Enrichment Scores (max and sum methods) for the data
            Tuple<double, double> enrichmentScores = ComputeContinuousEnrichmentScoreWithSplits(splits, featureValues, analyteValues, allProteinIDList, proteinIDsInProteinSet);
            double enrichmentScoreMax = enrichmentScores.Item1;
            double enrichmentScoreSum = enrichmentScores.Item2;
            retval.Add(RESULTS_ES_MAX_KEY, enrichmentScoreMax);
            retval.Add(RESULTS_ES_SUM_KEY, enrichmentScoreSum);

            // Compute p-values for enrichment scores by doing the realizations to elicit the null distribution
            int numRealizations = perms.Count;
            double[] esMaxForAllRealizations = new double[numRealizations];
            double[] esSumForAllRealizations = new double[numRealizations];

            // Perform the realizations based on the permutations contained in the input data
            for (int i = 0; i < numRealizations; i++)
            {
                #region Perform Realization
                // For each realization, 
                // 1. Shuffle all of the data
                // 2. Recompute group0 and group1 indices
                // 3. Recompute correlations and pvalues
                // 4. Recompute ES Running Sum
                // 5. Recompute ES (max and sum)
                int[] permutationArray = perms[i];
                double[] shuffledFeatureValues = new double[featureValues.Length];
                for (int j = 0; j < featureValues.Length; j++)
                {
                    shuffledFeatureValues[j] = featureValues[permutationArray[j]];
                }

                Tuple<double, double> enrichmentScoresThisRealization = ComputeContinuousEnrichmentScoreWithSplits(splits, shuffledFeatureValues, analyteValues, allProteinIDList, proteinIDsInProteinSet);
                double enrichmentScoreMaxThisRealization = enrichmentScoresThisRealization.Item1;
                double enrichmentScoreSumThisRealization = enrichmentScoresThisRealization.Item2;

                esMaxForAllRealizations[i] = enrichmentScoreMaxThisRealization;
                esSumForAllRealizations[i] = enrichmentScoreSumThisRealization;
                #endregion
            }

            // Compute the PValues for max and sum - add them to the results dictionary
            double esMaxPValue = ComputeEnrichmentScoreMaxPValue(esMaxForAllRealizations, enrichmentScoreMax);
            double esSumPValue = ComputeEnrichmentScoreSumPValue(esSumForAllRealizations, enrichmentScoreSum);
            retval.Add(RESULTS_PVALUE_MAX_KEY, esMaxPValue);
            retval.Add(RESULTS_PVALUE_SUM_KEY, esSumPValue);

            #region Save Plots for Distributions
            string esMaxDistPlotFilename = resultsDataDirectory + "\\PSEA_" + featureName + "_" + proteinSetToAnalyze + "_ESMaxDist.png";
            CreateAndSaveESDistributionPlot(esMaxForAllRealizations, enrichmentScoreMax, ES_MAX, esMaxDistPlotFilename, featureName, proteinSetToAnalyze);
            string esSumDistPlotFilename = resultsDataDirectory + "\\PSEA_" + featureName + "_" + proteinSetToAnalyze + "_ESSumDist.png";
            CreateAndSaveESDistributionPlot(esSumForAllRealizations, enrichmentScoreSum, ES_SUM, esSumDistPlotFilename, featureName, proteinSetToAnalyze);
            #endregion

            #region Write ESNulls .csv file
            DataTable esNullsTable = new DataTable();
            esNullsTable.Columns.Add("ESMax");
            esNullsTable.Columns.Add("ESSum");

            for (int i = 0; i < numRealizations; i++)
            {
                DataRow dr = esNullsTable.NewRow();
                dr["ESMax"] = esMaxForAllRealizations[i];
                dr["ESSum"] = esSumForAllRealizations[i];
                esNullsTable.Rows.Add(dr);
            }

            string theESNullsFileName = resultsDataDirectory + "\\PSEA_" + featureName + "_" + proteinSetToAnalyze + "_ESNulls.csv";
            string[,] dtAsArray2 = TwoDimensionalStringArrayUtils.ConvertDataTableTo2DString(esNullsTable);
            TwoDimensionalStringArrayUtils.WriteCSVFile(dtAsArray2, theESNullsFileName);
            #endregion

            return retval;
        }
    }
}
