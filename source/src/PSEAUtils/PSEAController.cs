﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: PSEAController.cs
//
//      (C) Copyright 2018 Biodesix Inc.
//      All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

using Local.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace Biodesix.PSEA.Utils
{
    /// <summary>
    /// This class contains the execution logic for doing a PSEA computation given the command
    /// line arguments parsed by the ArgParser.  It uses utility methods as needed to complete
    /// the analysis.
    /// </summary>
    public static class PSEAController
    {
        /// <summary>
        /// "\\PSEA_Results_Data" - This is the name of the results subdirectory that is created under the user
        /// specified output directory.  This sub-directory holds all of the intermediate results 
        /// and plots needed by the team to support the PSEA.
        /// </summary>
        private const string RESULTS_DATA_DIRECTORY = "\\PSEA_Results_Data";

        /// <summary>
        /// This is the filename of the spreadsheet that will contain the summarized results of all
        /// of the PSEAs that are run.
        /// </summary>
        private const string PSEA_RESULTS_SPREADSHEET_FILENAME = "\\PSEA_Results.xlsx";

        /// <summary>
        /// This is the main controller method that takes the user specified arguments and 
        /// executes the PSEA.  The arguments are extracted from the ArgParser, and C# Tasks are 
        /// created for every feature name / protein set pair to analyze.  The results are collected
        /// and a summary results spreadsheet is written to the user specified location.
        /// </summary>
        /// <param name="theParser">A PSEA argument parser instance.</param>
        public static void PerformPSEA(ArgParser theParser)
        {
            #region Make sure the output directory and results data subdirectory exist and are empty.
            string outputDirectory = theParser.OutputDir;

            if (!Directory.Exists(outputDirectory))
            {
                Directory.CreateDirectory(outputDirectory);
            }
            else
            {
                if (Directory.GetFiles(outputDirectory, "*.xlsx", SearchOption.AllDirectories).Length > 0)
                {
                    Console.WriteLine("PSEA Execution Error:  The output directory contains .xlsx files.  Please remove them, and try again.");
                    return;
                }
                if (Directory.GetFiles(outputDirectory, "*.png", SearchOption.AllDirectories).Length > 0)
                {
                    Console.WriteLine("PSEA Execution Error:  The output directory contains .png files.  Please remove them, and try again.");
                    return;
                }
                if (Directory.GetFiles(outputDirectory, "*.csv", SearchOption.AllDirectories).Length > 0)
                {
                    Console.WriteLine("PSEA Execution Error:  The output directory contains .csv files.  Please remove them, and try again.");
                    return;
                }
            }

            string resultsDataDirectory = outputDirectory + RESULTS_DATA_DIRECTORY;
            if (!Directory.Exists(resultsDataDirectory))
            {
                Directory.CreateDirectory(resultsDataDirectory);
            }
            #endregion

            // Get the number of realizations to perform to elicit the null distributions
            int numRealizations = theParser.NumRealizationsToComputeNullDist;

            // Get input feature table - convert from DataTable to FeatureTable
            DataTable ftDataTable = theParser.InputFeatureTableDataTable;
            FeatureTable theFT = new FeatureTable(ftDataTable.TableName, theParser.FeatureTableFilenameColumn, theParser.FeatureTableGroupnameColumn);
            theFT.SetDataFromTable(ftDataTable);

            // Get input analytes table - convert from DataTable to FeatureTable - groupnames do not matter for this table,
            // so call the filename column the groupname column for convenience.  This allows us to use FeatureTableUtils on 
            // the analytes table.
            DataTable analytesDataTable = theParser.AnalytesDataTable;
            FeatureTable analytesFT = new FeatureTable(analytesDataTable.TableName, theParser.AnalytesFilenameColumn, theParser.AnalytesFilenameColumn);
            analytesFT.SetDataFromTable(analytesDataTable);

            // Get input protein sets table
            DataTable proteinSetsDataTable = theParser.ProteinSetsDataTable;
            string proteinSetsProteinIDColumn = theParser.ProteinSetsProteinIDColumn;

            // Get the protein sets to Analyze - if all protein sets, get all of the column names
            // in the protein sets data table (except the protein ID column)
            bool analyzeAllProteinSets = theParser.AnalyzeAllProteinSets;
            List<string> proteinSetsToAnalyze = null;
            if (analyzeAllProteinSets)
            {
                proteinSetsToAnalyze = new List<string>();
                foreach (DataColumn dc in proteinSetsDataTable.Columns)
                {
                    if (!dc.ColumnName.Equals(proteinSetsProteinIDColumn))
                    {
                        proteinSetsToAnalyze.Add(dc.ColumnName);
                    }
                }
            }
            else
            {
                proteinSetsToAnalyze = theParser.ProteinSetsToAnalyze;
            }

            // Read the protein sets data table and get the proteinIDs that are part of each protein set
            Dictionary<string, List<string>> protSetsToAnalyzeToProtIDMap = PSEAUtils.GetProteinIDListsForProteinSets(
                proteinSetsDataTable, proteinSetsToAnalyze, proteinSetsProteinIDColumn);

            // Get the filenames in order of rows in the feature table
            List<string> filenamesInFeatureTableOrder = new List<string>();
            foreach (DataRow dr in theFT.Rows)
            {
                filenamesInFeatureTableOrder.Add(dr[theFT.FilenameColumnName].ToString());
            }

            // Get the protein IDs in order of the rows in the protein set table
            List<string> proteinIDsInProteinSetOrder = new List<string>();
            foreach (DataRow dr in proteinSetsDataTable.Rows)
            {
                proteinIDsInProteinSetOrder.Add(dr[proteinSetsProteinIDColumn].ToString());
            }

            // If we're in continuous mode, or binary with splits get the analyte values, if we're in binary label mode get the analyte ranks.
            double[,] analyteValues = null;

            if ((!String.IsNullOrEmpty(theParser.PhenotypeLabelToAnalyze))&&(theParser.NumDataSplits <= 0))
            {
                // Get all of the analyte rank values
                analyteValues = PSEAUtils.GetAnalyteRankArray(analytesFT, filenamesInFeatureTableOrder, proteinIDsInProteinSetOrder);
            }
            else
            {
                // Get all of the analyte values
                analyteValues = PSEAUtils.GetAnalyteValueArray(analytesFT, filenamesInFeatureTableOrder, proteinIDsInProteinSetOrder);
            }

            // Set up the results data structure - these are the results returned from every task in the task list.
            List<Dictionary<string, object>> allResults = new List<Dictionary<string, object>>();

            // Create the task list - there will be one task for every feature name / protein set pair.
            List<Task> taskList = new List<Task>();

            // Figure out if we are doing data splits, if so, then perform PSEA analysis in that mode
            int numDataSplits = theParser.NumDataSplits;

            if (numDataSplits <= 0)
            {
                #region Do Binary Analyses
                // Do Binary Analyses as specified - only one label to analyze allowed for now, so the 
                // maximum number of tasks created here is (1 x numProteinSetsToAnalyze)
                if (!String.IsNullOrEmpty(theParser.PhenotypeLabelToAnalyze))
                {
                    // Get the phenotypeLabelToAnalyze and positive label
                    string phenotypeLabelToAnalyze = theParser.PhenotypeLabelToAnalyze;
                    string positiveLabel = theParser.PositiveLabel;

                    // Get the feature values for the feature to be analyzed - positive label = 1.0, negative label = 0.0
                    double[] featureValues = PSEAUtils.GetPhenotypeFeatureValueArray(theFT, phenotypeLabelToAnalyze, positiveLabel);

                    // Pre-compute the correlations (between the feature values and analyte values for each protein ID) and p-values
                    double[] correlations = null;
                    double[] pValues = null;

                    // Split the feature values into indices that are part of the positive group (group1) and negative group (group0)
                    List<int> group0Indices = new List<int>();
                    List<int> group1Indices = new List<int>();
                    for (int i = 0; i < featureValues.Length; i++)
                    {
                        if (featureValues[i] == 0.0)
                        {
                            group0Indices.Add(i);
                        }
                        else
                        {
                            group1Indices.Add(i);
                        }
                    }

                    // Compute the rank correlations for the baseline once, then do the realizations for each protein set below.
                    PSEAUtils.ComputeBinaryRankCorrelationsAndPValues(group0Indices, group1Indices, analyteValues, out correlations, out pValues);

                    // For each protein set / feature name pair (only one feature in the binary case - so just iterate over protein sets to analyze)
                    // Create a task that does the PSEA.  Add it to the task list.
                    foreach (string proteinSetToAnalyze in proteinSetsToAnalyze)
                    {
                        // Set up random number generator
                        int rngSeed = PSEAUtils.RANDOM_SEED;
                        Random rng = new Random(rngSeed);

                        Task t = Task<Dictionary<string, object>>.Run(() => PSEAUtils.PerformBinaryPSEA(correlations, pValues, group0Indices, group1Indices, analyteValues,
                            phenotypeLabelToAnalyze, proteinSetToAnalyze, proteinIDsInProteinSetOrder, protSetsToAnalyzeToProtIDMap[proteinSetToAnalyze],
                            numRealizations, resultsDataDirectory, rng, theParser.OutputPermutations));
                        taskList.Add(t);                        
                    }
                }
                #endregion

                #region Do Continuous Analyses
                // Do Continuous Analyses as specified
                bool analyzeAllFeatures = theParser.AnalyzeAllFeatures;
                List<string> featuresToAnalyze = null;
                if (analyzeAllFeatures)
                {
                    featuresToAnalyze = FeatureTableUtils.GetCompleteNumericFeatures(theFT);
                }
                else
                {
                    featuresToAnalyze = theParser.FeaturesToAnalyze;
                }

                // For each feature name to be analyzed ...
                foreach (string featureName in featuresToAnalyze)
                {
                    // Get the feature values for the feature to be analyzed
                    double[] featureValues = PSEAUtils.GetPhenotypeFeatureValueArray(theFT, featureName);

                    // Pre-compute the correlations and pvalues
                    double[] correlations = null;
                    double[] pValues = null;
                    PSEAUtils.ComputeContinuousCorrelationsAndPValues(featureValues, analyteValues, out correlations, out pValues);

                    // For each protein set to be analyzed ...
                    foreach (string proteinSetToAnalyze in proteinSetsToAnalyze)
                    {
                        // Set up random number generator
                        int rngSeed = PSEAUtils.RANDOM_SEED;
                        Random rng = new Random(rngSeed);

                        // Create a task that does the PSEA analysis and add to the task list.
                        Task t = Task<Dictionary<string, object>>.Run(() => PSEAUtils.PerformContinuousPSEA(correlations, pValues, featureValues, analyteValues,
                            featureName, proteinSetToAnalyze, proteinIDsInProteinSetOrder, protSetsToAnalyzeToProtIDMap[proteinSetToAnalyze],
                            numRealizations, resultsDataDirectory, rng, theParser.OutputPermutations));
                        taskList.Add(t);
                    }
                }
                #endregion
            }
            else
            {
                // Set up random number generator
                int rngSeed = PSEAUtils.RANDOM_SEED;
                Random rng = new Random(rngSeed);

                List<int[]> splits = null;
                List<int[]> perms = null;
                List<double[,]> analyteRankValuesPerSplit = null;
                if (!String.IsNullOrEmpty(theParser.PhenotypeLabelToAnalyze))
                {
                    #region Set up the positive and negative labels for counting to check the minimum group size.
                    // Get the phenotypeLabelToAnalyze and positive label
                    string phenotypeLabelToAnalyze = theParser.PhenotypeLabelToAnalyze;
                    string positiveLabel = theParser.PositiveLabel;

                    // Get the feature values for the feature to be analyzed - positive label = 1.0, negative label = 0.0
                    double[] featureValues = PSEAUtils.GetPhenotypeFeatureValueArray(theFT, phenotypeLabelToAnalyze, positiveLabel);

                    List<int> group0Indices = new List<int>();
                    List<int> group1Indices = new List<int>();
                    for (int i = 0; i < featureValues.Length; i++)
                    {
                        if (featureValues[i] == 0.0)
                        {
                            group0Indices.Add(i);
                        }
                        else
                        {
                            group1Indices.Add(i);
                        }
                    }
                    #endregion

                    #region Compute Splits - with checking for minimum group size
                    splits = new List<int[]>();
                    analyteRankValuesPerSplit = new List<double[,]>();
                    int sizeOfSplit = filenamesInFeatureTableOrder.Count / 2;
                    DataTable splitsDT = new DataTable("DataSplits");
                    splitsDT.Columns.Add("Filename", typeof(string));
                    for (int j = 0; j < filenamesInFeatureTableOrder.Count; j++)
                    {
                        DataRow newRow = splitsDT.NewRow();
                        newRow["Filename"] = filenamesInFeatureTableOrder[j];
                        splitsDT.Rows.Add(newRow);
                    }

                    int numSuccessfulSplits = 0;
                    int numTries = 0;
                    while ((numSuccessfulSplits < theParser.NumDataSplits) && (numTries < 100 * theParser.NumDataSplits))
                    {
                        // Add two columns for each split (50/50 split)
                        string set1ColumnName = "Set1_Split" + numTries.ToString();
                        string set2ColumnName = "Set2_Split" + numTries.ToString();
                        splitsDT.Columns.Add(set1ColumnName, typeof(int));
                        splitsDT.Columns.Add(set2ColumnName, typeof(int));

                        // Shuffle the integers from (0 ... numSamples-1)
                        int[] shuffledRowIndices = PSEAUtils.GetPermutationArray(filenamesInFeatureTableOrder.Count, rng);

                        // This will contain the split information.
                        int[] split = new int[sizeOfSplit];
                        int[] oppositeSplit = new int[sizeOfSplit];
                        int numInSplit = 0;
                        int numInOppositeSplit = 0;

                        // For each number in the shuffled array, if it is >= 1/2 the number of samples, then it is in set 1, otherwise in set 0
                        for (int j = 0; j < filenamesInFeatureTableOrder.Count; j++)
                        {
                            DataRow rowToWorkOn = splitsDT.Rows[j];
                            if (shuffledRowIndices[j] >= sizeOfSplit)
                            {
                                rowToWorkOn[set1ColumnName] = 1;
                                rowToWorkOn[set2ColumnName] = 0;
                                split[numInSplit] = j;
                                numInSplit++;
                            }
                            else
                            {
                                rowToWorkOn[set1ColumnName] = 0;
                                rowToWorkOn[set2ColumnName] = 1;
                                oppositeSplit[numInOppositeSplit] = j;
                                numInOppositeSplit++;
                            }
                        }

                        // CHECK THE SPLIT HERE TO ENFORCE MIN GROUP SIZE!
                        List<int> group0IndicesThisSplit = new List<int>();
                        List<int> group1IndicesThisSplit = new List<int>();

                        int rowCounter = 0;
                        foreach (int idx in split)
                        {
                            if (group0Indices.Contains(idx))
                            {
                                group0IndicesThisSplit.Add(idx);
                            }
                            else if (group1Indices.Contains(idx))
                            {
                                group1IndicesThisSplit.Add(idx);
                            }
                        }

                        if ((group0IndicesThisSplit.Count >= theParser.MinGroupSizeForLabelsAndSplits) &&
                            (group1IndicesThisSplit.Count >= theParser.MinGroupSizeForLabelsAndSplits))
                        {
                            // Add this split to the list of splits.
                            splits.Add(split);
                            double[,] analyteRanksForSplit = PSEAUtils.GetAnalyteRankArrayForSubsetOfRows(split, analyteValues);
                            analyteRankValuesPerSplit.Add(analyteRanksForSplit);
                            splits.Add(oppositeSplit);
                            double[,] analyteRanksForOppositeSplit = PSEAUtils.GetAnalyteRankArrayForSubsetOfRows(oppositeSplit, analyteValues);
                            analyteRankValuesPerSplit.Add(analyteRanksForOppositeSplit);
                            
                            numSuccessfulSplits++;
                        }
                        else
                        {
                            splitsDT.Columns.Remove(set1ColumnName);
                            splitsDT.Columns.Remove(set2ColumnName);
                        }

                        numTries++;
                    }

                    if (numSuccessfulSplits == theParser.NumDataSplits)
                    {
                        // Write out splits file
                        string timestamp = DateTime.Now.ToString("yyyyMMddhhmmss");
                        string splitsFilename = theParser.OutputDir + "\\Splits_" + timestamp + ".xlsx";
                        OpenXmlExcelWriter.WriteDataTableToExcelWorksheet(splitsDT, splitsFilename, splitsDT.TableName);
                        Console.WriteLine("Computed Data Splits for Label Analysis, computed " + numSuccessfulSplits.ToString() + " splits using " + numTries.ToString() + " tries.");
                    }
                    else
                    {
                        throw new Exception("PSEA Error: Unable to create the desired number of splits (" + theParser.NumDataSplits.ToString() +
                            ") while honoring the minimum group size (" + theParser.MinGroupSizeForLabelsAndSplits.ToString() +
                            ") in " + numTries.ToString() + " tries.  Please check your data and adjust parameters as needed.");
                    }
                    #endregion

                    #region Compute Permutations - with checking for minimum group size
                    perms = new List<int[]>();
                    int numSuccessfulPerms = 0;
                    int numPermTries = 0;
                    while ((numSuccessfulPerms < theParser.NumRealizationsToComputeNullDist) &&
                           (numPermTries < 100 * theParser.NumRealizationsToComputeNullDist))
                    {
                        // Shuffle the integers from (0 ... numSamples-1)
                        int[] perm = PSEAUtils.GetPermutationArray(filenamesInFeatureTableOrder.Count, rng);

                        // CHECK THE SPLIT HERE TO ENFORCE MIN GROUP SIZE!
                        List<int> group0IndicesShuffled = new List<int>();
                        foreach (int idx in group0Indices)
                        {
                            group0IndicesShuffled.Add(perm[idx]);
                        }

                        List<int> group1IndicesShuffled = new List<int>();
                        foreach (int idx in group1Indices)
                        {
                            group1IndicesShuffled.Add(perm[idx]);
                        }

                        bool permValidForAllSplits = true;
                        for (int splitIdx = 0; splitIdx < splits.Count; splitIdx++)
                        {
                            int[] currentSplit = splits[splitIdx];

                            #region Check permutation for all splits to ensure minimum group number
                            List<int> group0IndicesThisSplit = new List<int>();
                            List<int> group1IndicesThisSplit = new List<int>();

                            foreach (int idx in currentSplit)
                            {
                                if (group0IndicesShuffled.Contains(idx))
                                {
                                    group0IndicesThisSplit.Add(idx);
                                }
                                else if (group1IndicesShuffled.Contains(idx))
                                {
                                    group1IndicesThisSplit.Add(idx);
                                }
                            }

                            if ((group0IndicesThisSplit.Count < theParser.MinGroupSizeForLabelsAndSplits) ||
                            (group1IndicesThisSplit.Count < theParser.MinGroupSizeForLabelsAndSplits))
                            {
                                permValidForAllSplits = false;
                                break;
                            }
                            #endregion
                        }

                        if (permValidForAllSplits)
                        {
                            // Add this perm to the list of perms.
                            perms.Add(perm);
                            numSuccessfulPerms++;
                        }

                        numPermTries++;
                    }

                    if (numSuccessfulPerms != theParser.NumRealizationsToComputeNullDist)
                    {
                        throw new Exception("PSEA Error: Unable to create the desired number of null realizations (" + theParser.NumRealizationsToComputeNullDist.ToString() +
                            ") while honoring the minimum group size (" + theParser.MinGroupSizeForLabelsAndSplits.ToString() +
                            ") in " + numPermTries.ToString() + " tries.  Please check your data and adjust parameters as needed.");
                    }
                    Console.WriteLine("Computed Null Realizations for Label Analysis with Data Splits, computed " + numSuccessfulPerms.ToString() + " permutations using " + numPermTries.ToString() + " tries.");
                    #endregion
                }
                else
                {
                    #region Compute Splits
                    splits = new List<int[]>();
                    int sizeOfSplit = filenamesInFeatureTableOrder.Count / 2;
                    DataTable splitsDT = new DataTable("DataSplits");
                    splitsDT.Columns.Add("Filename", typeof(string));
                    for (int j = 0; j < filenamesInFeatureTableOrder.Count; j++)
                    {
                        DataRow newRow = splitsDT.NewRow();
                        newRow["Filename"] = filenamesInFeatureTableOrder[j];
                        splitsDT.Rows.Add(newRow);
                    }

                    for (int i = 0; i < theParser.NumDataSplits; i++)
                    {
                        // Add two columns for each split (50/50 split)
                        string set1ColumnName = "Set1_Split" + i.ToString();
                        string set2ColumnName = "Set2_Split" + i.ToString();
                        splitsDT.Columns.Add(set1ColumnName, typeof(int));
                        splitsDT.Columns.Add(set2ColumnName, typeof(int));

                        // Shuffle the integers from (0 ... numSamples-1)
                        int[] shuffledRowIndices = PSEAUtils.GetPermutationArray(filenamesInFeatureTableOrder.Count, rng);

                        // This will contain the split information.
                        int[] split = new int[sizeOfSplit];
                        int[] oppositeSplit = new int[sizeOfSplit];
                        int numInSplit = 0;
                        int numInOppositeSplit = 0;

                        // For each number in the shuffled array, if it is >= 1/2 the number of samples, then it is in set 1, otherwise in set 0
                        for (int j = 0; j < filenamesInFeatureTableOrder.Count; j++)
                        {
                            DataRow rowToWorkOn = splitsDT.Rows[j];
                            if (shuffledRowIndices[j] >= sizeOfSplit)
                            {
                                rowToWorkOn[set1ColumnName] = 1;
                                rowToWorkOn[set2ColumnName] = 0;
                                split[numInSplit] = j;
                                numInSplit++;
                            }
                            else
                            {
                                rowToWorkOn[set1ColumnName] = 0;
                                rowToWorkOn[set2ColumnName] = 1;
                                oppositeSplit[numInOppositeSplit] = j;
                                numInOppositeSplit++;
                            }
                        }

                        // Add this split to the list of splits.
                        splits.Add(split);
                        splits.Add(oppositeSplit);
                    }

                    // Write out splits file
                    string timestamp = DateTime.Now.ToString("yyyyMMddhhmmss");
                    string splitsFilename = theParser.OutputDir + "\\Splits_" + timestamp + ".xlsx";
                    OpenXmlExcelWriter.WriteDataTableToExcelWorksheet(splitsDT, splitsFilename, splitsDT.TableName);
                    #endregion

                    #region Compute Permutations
                    perms = new List<int[]>();
                    int sizeOfPerm = filenamesInFeatureTableOrder.Count;

                    for (int i = 0; i < theParser.NumRealizationsToComputeNullDist; i++)
                    {
                        // Shuffle the integers from (0 ... numSamples-1)
                        int[] perm = PSEAUtils.GetPermutationArray(filenamesInFeatureTableOrder.Count, rng);
                        perms.Add(perm);
                    }
                    #endregion
                }

                #region Do Binary Analyses
                // Do Binary Analyses as specified - only one label to analyze allowed for now, so the 
                // maximum number of tasks created here is (1 x numProteinSetsToAnalyze)
                if (!String.IsNullOrEmpty(theParser.PhenotypeLabelToAnalyze))
                {
                    // Get the phenotypeLabelToAnalyze and positive label
                    string phenotypeLabelToAnalyze = theParser.PhenotypeLabelToAnalyze;
                    string positiveLabel = theParser.PositiveLabel;

                    // Get the feature values for the feature to be analyzed - positive label = 1.0, negative label = 0.0
                    double[] featureValues = PSEAUtils.GetPhenotypeFeatureValueArray(theFT, phenotypeLabelToAnalyze, positiveLabel);

                    // Split the feature values into indices that are part of the positive group (group1) and negative group (group0)
                    List<int> group0Indices = new List<int>();
                    List<int> group1Indices = new List<int>();
                    for (int i = 0; i < featureValues.Length; i++)
                    {
                        if (featureValues[i] == 0.0)
                        {
                            group0Indices.Add(i);
                        }
                        else
                        {
                            group1Indices.Add(i);
                        }
                    }

                    // For each protein set / feature name pair (only one feature in the binary case - so just iterate over protein sets to analyze)
                    // Create a task that does the PSEA.  Add it to the task list.
                    foreach (string proteinSetToAnalyze in proteinSetsToAnalyze)
                    {
                        Task t = Task<Dictionary<string, object>>.Run(() => PSEAUtils.PerformBinaryPSEAWithSplits(splits, perms, group0Indices, group1Indices, analyteRankValuesPerSplit,
                            phenotypeLabelToAnalyze, proteinSetToAnalyze, proteinIDsInProteinSetOrder, protSetsToAnalyzeToProtIDMap[proteinSetToAnalyze],
                            resultsDataDirectory));
                        taskList.Add(t);

                        // THIS IS THE NON TASK CODE ... In case we revert to not using C# tasks!
                        //Dictionary<string, object> result = PSEAUtils.PerformBinaryPSEA(correlations, pValues, featureValues, analyteValues, 
                        //    phenotypeLabelToAnalyze, proteinSetToAnalyze, proteinIDsInProteinSetOrder, protSetsToAnalyzeToProtIDMap[proteinSetToAnalyze], 
                        //    numRealizations, resultsDataDirectory);

                        //allResults.Add(result);
                    }
                }
                #endregion

                #region Do Continuous Analyses
                // Do Continuous Analyses as specified
                bool analyzeAllFeatures = theParser.AnalyzeAllFeatures;
                List<string> featuresToAnalyze = null;
                if (analyzeAllFeatures)
                {
                    featuresToAnalyze = FeatureTableUtils.GetCompleteNumericFeatures(theFT);
                }
                else
                {
                    featuresToAnalyze = theParser.FeaturesToAnalyze;
                }

                // For each feature name to be analyzed ...
                foreach (string featureName in featuresToAnalyze)
                {
                    // Get the feature values for the feature to be analyzed
                    double[] featureValues = PSEAUtils.GetPhenotypeFeatureValueArray(theFT, featureName);

                    // For each protein set to be analyzed ...
                    foreach (string proteinSetToAnalyze in proteinSetsToAnalyze)
                    {
                        // Create a task that does the PSEA analysis and add to the task list.
                        Task t = Task<Dictionary<string, object>>.Run(() => PSEAUtils.PerformContinuousPSEAWithSplits(splits, perms, featureValues, analyteValues,
                            featureName, proteinSetToAnalyze, proteinIDsInProteinSetOrder, protSetsToAnalyzeToProtIDMap[proteinSetToAnalyze],
                            resultsDataDirectory));
                        taskList.Add(t);

                        // THIS IS THE NON TASK CODE ... In case we revert to not using C# tasks!
                        //Dictionary<string, object> result = PSEAUtils.PerformContinuousPSEA(correlations, pValues, featureValues, analyteValues,
                        //   featureName, proteinSetToAnalyze, proteinIDsInProteinSetOrder, protSetsToAnalyzeToProtIDMap[proteinSetToAnalyze],
                        //   numRealizations, resultsDataDirectory);

                        // allResults.Add(result);
                    }
                }
                #endregion
            }

            // Wait for all tasks to complete ... handling errors appropriately
            try
            {
                Task.WaitAll(taskList.ToArray());
            }
            catch (AggregateException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine(ex.GetBaseException().Message);
                Console.WriteLine(ex.GetBaseException().StackTrace);
                foreach (Exception innerEx in ex.InnerExceptions)
                {
                    Console.WriteLine(innerEx.Message);
                    Console.WriteLine(ex.StackTrace);
                }
            }

            // Compile the results into allresults
            foreach (Task<Dictionary<string, object>> t in taskList)
            {
                // If there was an error with a task, the result could be null ... so check.
                if (t.Result != null)
                {
                    allResults.Add(t.Result);
                }
            }

            // Take all results and write the PSEAResults.xlsx file!
            WritePSEAResultsFile(allResults, proteinSetsToAnalyze, outputDirectory);
        }

        /// <summary>
        /// This method takes the results from all of the PSEA tasks and creates the PSEA_Results.xlsx file
        /// that contains the summary of all of the results.
        /// </summary>
        /// <param name="allResults">The results dictionaries from all of the executed PSEA tasks.</param>
        /// <param name="analyzedProteinSets">The list of analyzed protein set names.</param>
        /// <param name="outputDirectory">The output directory in which to write the results file.</param>
        private static void WritePSEAResultsFile(List<Dictionary<string, object>> allResults, List<string> analyzedProteinSets, string outputDirectory)
        {
            // The PSEA_Results.xlsx file contains 4 worksheets, 1 for each of ES_MAX, ES_SUM, P_VALUE_MAX, P_VALUE_SUM.
            // Create the 4 data tables to be written to those 4 worksheets.  Add the feature name column to all.
            DataTable pseaResultsTableESMax = new DataTable(PSEAUtils.RESULTS_ES_MAX_KEY);
            pseaResultsTableESMax.Columns.Add(PSEAUtils.RESULTS_FEATURE_NAME_KEY);
            DataTable pseaResultsTableESSum = new DataTable(PSEAUtils.RESULTS_ES_SUM_KEY);
            pseaResultsTableESSum.Columns.Add(PSEAUtils.RESULTS_FEATURE_NAME_KEY);
            DataTable pseaResultsTablePValueMax = new DataTable(PSEAUtils.RESULTS_PVALUE_MAX_KEY);
            pseaResultsTablePValueMax.Columns.Add(PSEAUtils.RESULTS_FEATURE_NAME_KEY);
            DataTable pseaResultsTablePValueSum = new DataTable(PSEAUtils.RESULTS_PVALUE_SUM_KEY);
            pseaResultsTablePValueSum.Columns.Add(PSEAUtils.RESULTS_FEATURE_NAME_KEY);

            // Add the protein set columns to all worksheets
            foreach (string proteinSet in analyzedProteinSets)
            {
                pseaResultsTableESMax.Columns.Add(proteinSet);
                pseaResultsTableESSum.Columns.Add(proteinSet);
                pseaResultsTablePValueMax.Columns.Add(proteinSet);
                pseaResultsTablePValueSum.Columns.Add(proteinSet);
            }

            // Using the results data, fill out all 4 worksheets from the results of the PSEA tasks.
            foreach (Dictionary<string, object> result in allResults)
            {
                string featureName = result[PSEAUtils.RESULTS_FEATURE_NAME_KEY].ToString();
                string proteinSet = result[PSEAUtils.RESULTS_PROTEIN_SET_NAME_KEY].ToString();
                double esMax = Convert.ToDouble(result[PSEAUtils.RESULTS_ES_MAX_KEY].ToString());
                double esSum = Convert.ToDouble(result[PSEAUtils.RESULTS_ES_SUM_KEY].ToString());
                double pValueMax = Convert.ToDouble(result[PSEAUtils.RESULTS_PVALUE_MAX_KEY].ToString());
                double pValueSum = Convert.ToDouble(result[PSEAUtils.RESULTS_PVALUE_SUM_KEY].ToString());

                #region Put ESMax in table
                DataRow esMaxDR = null;
                foreach (DataRow dr in pseaResultsTableESMax.Rows)
                {
                    if (dr[PSEAUtils.RESULTS_FEATURE_NAME_KEY].ToString().Equals(featureName))
                    {
                        esMaxDR = dr;
                        break;
                    }
                }

                if (esMaxDR == null)
                {
                    esMaxDR = pseaResultsTableESMax.NewRow();
                    esMaxDR[PSEAUtils.RESULTS_FEATURE_NAME_KEY] = featureName;
                    pseaResultsTableESMax.Rows.Add(esMaxDR);
                }
                esMaxDR[proteinSet] = esMax;
                #endregion

                #region Put ESSum in table
                DataRow esSumDR = null;
                foreach (DataRow dr in pseaResultsTableESSum.Rows)
                {
                    if (dr[PSEAUtils.RESULTS_FEATURE_NAME_KEY].ToString().Equals(featureName))
                    {
                        esSumDR = dr;
                        break;
                    }
                }

                if (esSumDR == null)
                {
                    esSumDR = pseaResultsTableESSum.NewRow();
                    esSumDR[PSEAUtils.RESULTS_FEATURE_NAME_KEY] = featureName;
                    pseaResultsTableESSum.Rows.Add(esSumDR);
                }
                esSumDR[proteinSet] = esSum;
                #endregion

                #region Put PValueMax in table
                DataRow pValueMaxDR = null;
                foreach (DataRow dr in pseaResultsTablePValueMax.Rows)
                {
                    if (dr[PSEAUtils.RESULTS_FEATURE_NAME_KEY].ToString().Equals(featureName))
                    {
                        pValueMaxDR = dr;
                        break;
                    }
                }

                if (pValueMaxDR == null)
                {
                    pValueMaxDR = pseaResultsTablePValueMax.NewRow();
                    pValueMaxDR[PSEAUtils.RESULTS_FEATURE_NAME_KEY] = featureName;
                    pseaResultsTablePValueMax.Rows.Add(pValueMaxDR);
                }
                pValueMaxDR[proteinSet] = pValueMax;
                #endregion

                #region Put PValueSum in table
                DataRow pValueSumDR = null;
                foreach (DataRow dr in pseaResultsTablePValueSum.Rows)
                {
                    if (dr[PSEAUtils.RESULTS_FEATURE_NAME_KEY].ToString().Equals(featureName))
                    {
                        pValueSumDR = dr;
                        break;
                    }
                }

                if (pValueSumDR == null)
                {
                    pValueSumDR = pseaResultsTablePValueSum.NewRow();
                    pValueSumDR[PSEAUtils.RESULTS_FEATURE_NAME_KEY] = featureName;
                    pseaResultsTablePValueSum.Rows.Add(pValueSumDR);
                }
                pValueSumDR[proteinSet] = pValueSum;
                #endregion
            }

            // Write the 4 data tables to the specified output spreadsheet.
            string pseaResultsFilename = outputDirectory + PSEA_RESULTS_SPREADSHEET_FILENAME;
            OpenXmlExcelWriter.WriteDataTableToExcelWorksheet(pseaResultsTableESMax, pseaResultsFilename, pseaResultsTableESMax.TableName);
            OpenXmlExcelWriter.WriteDataTableToExcelWorksheet(pseaResultsTableESSum, pseaResultsFilename, pseaResultsTableESSum.TableName);
            OpenXmlExcelWriter.WriteDataTableToExcelWorksheet(pseaResultsTablePValueMax, pseaResultsFilename, pseaResultsTablePValueMax.TableName);
            OpenXmlExcelWriter.WriteDataTableToExcelWorksheet(pseaResultsTablePValueSum, pseaResultsFilename, pseaResultsTablePValueSum.TableName);
        }
    }
}
