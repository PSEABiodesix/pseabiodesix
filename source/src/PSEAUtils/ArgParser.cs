﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: ArgParser.cs
//
//      (C) Copyright 2018 Biodesix Inc.
//      All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using Local.Util;

namespace Biodesix.PSEA.Utils
{
    /// <summary>
    /// This class contains a method for reading and validating the entries for the PSEA
    /// command line arguments.
    /// </summary>
    public class ArgParser
    {
        #region Private Members
        /// <summary>
        /// Stores the application version of the currently executing assembly.
        /// </summary>
        private string _applicationVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        /// <summary>
        /// Usage statement that describes the argument requirements.
        /// </summary>
        private string _usage =
            "\nPSEA.exe "
            + "\n" + PSEAConstants.PSEA_FTR_TBL_SS_FILE_ARG + " Fully justified feature table spreadsheet filename (*.xlsx)."
            + "\n" + PSEAConstants.PSEA_FTR_TBL_WS_NAME_ARG + " Worksheet to use in the feature table spreadsheet."
            + "\n" + PSEAConstants.PSEA_FTR_TBL_FILENAME_COL_ARG + " Column in feature table worksheet that contains filenames (Must match analytes file filenames)."
            + "\n" + PSEAConstants.PSEA_FTR_TBL_GROUPNAME_COL_ARG + " Column in feature table worksheet that contains groupnames."
            + "\n" + PSEAConstants.PSEA_ANALYTES_FILE_ARG + " waiverFully justified analytes filename (*.xlsx or *.csv file formats supported)."
            + "\n" + PSEAConstants.PSEA_ANALYTES_WS_NAME_ARG + " Worksheet to use in the analytes spreadsheet."
            + "\n" + PSEAConstants.PSEA_ANALYTES_FILENAME_COL_ARG + " Column in analytes worksheet that contains filenames."
            + "\n" + PSEAConstants.PSEA_PROTEIN_SET_SS_FILE_ARG + " Fully justified protein sets spreadsheet filename (*.xlsx)."
            + "\n" + PSEAConstants.PSEA_PROTEIN_SET_WS_NAME_ARG + " Worksheet to use in the protein sets spreadsheet."
            + "\n" + PSEAConstants.PSEA_PROTEIN_SET_PROTEIN_ID_COL_ARG + " Column in protein sets worksheet that contains the protein IDs."
            + "\n" + PSEAConstants.PSEA_NUM_REALIZATIONS_ARG + " Number of realizations to use to compute the null distribution of the enhancement score(s)."
            + "\n" + PSEAConstants.PSEA_OUTPUT_FOLDER_ARG + " Fully justified directory path where processing results will be saved."
            + "\n\nOptional Arguments:"
            + "\n" + PSEAConstants.PSEA_NUM_SPLITS_ARG + " Number of data split realizations to use in the PSEA process.  If this argument is not specified, then the standard PSEA process without data splitting will be used."
            + "\n" + PSEAConstants.PSEA_FTR_ARG + " Feature name of continuous feature to analyze. This argument may be present more than once to specify more than one feature to analyze."
            + "\n" + PSEAConstants.PSEA_ALL_FTRS_ARG + " If this argument is present, all numeric features in the input feature table will be analyzed."
            + "\n" + PSEAConstants.PSEA_PFTR_ARG + " Phenotype label to analyze (binary feature). This argument may be present only once."
            + "\n" + PSEAConstants.PSEA_POS_LABEL_ARG + " This argument defines the positive label to use with the phenotype label to analyze.  This arg is required if "
            + PSEAConstants.PSEA_PFTR_ARG + " is used."
            + "\n" + PSEAConstants.PSEA_MIN_GRP_SZ_ARG + " This argument specifies the minimum allowable group size when analyzing labels and using the data split strategy.  This arg is required "
            + " and must be greater than zero when " + PSEAConstants.PSEA_PFTR_ARG + " and " + PSEAConstants.PSEA_NUM_SPLITS_ARG + " are used."
            + "\n" + PSEAConstants.PSEA_PROTEIN_SET_NAME_ARG + " Protein Set Name to analyze.  This argument may be present more than once to specify multiple protein sets to analyze."
            + "\n" + PSEAConstants.PSEA_ALL_PROTEIN_SETS_ARG + " If this argument is present, all protein sets in the input protein sets file will be analyzed."
            + "\n" + PSEAConstants.PSEA_OUTPUT_PERMUTATIONS_ARG + " If this argument is present, permutations from the realizations will be output to a file called <ProteinSet>_<Feature>_Permutations.csv"
            + "\n" + PSEAConstants.PSEA_USAGE_ARG + " Returns the command line usage information."
            + "\n\nExample:"
            + "\n" + "PSEA.exe " + PSEAConstants.PSEA_FTR_TBL_SS_FILE_ARG + " ...\\MyFeatureTable.xlsx " + PSEAConstants.PSEA_FTR_TBL_WS_NAME_ARG
            + " Sheet1 " + PSEAConstants.PSEA_FTR_TBL_FILENAME_COL_ARG + " Filename " + PSEAConstants.PSEA_FTR_TBL_GROUPNAME_COL_ARG
            + " Groupname " + PSEAConstants.PSEA_ANALYTES_FILE_ARG + " ...\\soma_CLEAN.xlsx " + PSEAConstants.PSEA_ANALYTES_WS_NAME_ARG
            + " bdx " + PSEAConstants.PSEA_ANALYTES_FILENAME_COL_ARG + " SomaId_unique " + PSEAConstants.PSEA_PROTEIN_SET_SS_FILE_ARG
            + " ...\\Buckets.xlsx " + PSEAConstants.PSEA_PROTEIN_SET_WS_NAME_ARG + " Results_12_Searches "
            + PSEAConstants.PSEA_PROTEIN_SET_PROTEIN_ID_COL_ARG + " SomaId " + PSEAConstants.PSEA_NUM_REALIZATIONS_ARG + " 5000 "
            + PSEAConstants.PSEA_OUTPUT_FOLDER_ARG + " ...\\MyOutputFolder " + PSEAConstants.PSEA_FTR_ARG + " 3418 "
            + PSEAConstants.PSEA_FTR_ARG + " 3111 " + PSEAConstants.PSEA_PFTR_ARG + " IS2_Label " + PSEAConstants.PSEA_POS_LABEL_ARG
            + " Late " + PSEAConstants.PSEA_PROTEIN_SET_NAME_ARG + " Amigo1 " + PSEAConstants.PSEA_PROTEIN_SET_NAME_ARG + " Amigo3 ";

        #region Member variable that hold the argument information
        /// <summary>
        /// This is the output directory for all processing results.
        /// </summary>
        private string _outputDir = String.Empty;

        /// <summary>
        /// This is the spreadsheet filename for the input feature table.
        /// </summary>
        private string _featureTableSpreadsheetFilename = String.Empty;

        /// <summary>
        /// This is the worksheet name for the input feature table.
        /// </summary>
        private string _featureTableWorksheetName = String.Empty;

        /// <summary>
        /// This is the column name for the column that contains the filenames in the input feature table.
        /// </summary>
        private string _featureTableFilenameColumn = String.Empty;

        /// <summary>
        /// This is the column name for the column that contains the groupnames in the input feature table.
        /// </summary>
        private string _featureTableGroupnameColumn = String.Empty;

        /// <summary>
        /// This is the list of features to analyze.
        /// </summary>
        private List<string> _featuresToAnalyze = new List<string>();

        /// <summary>
        /// If this flag is true, analyze all features in the input feature table.
        /// </summary>
        private bool _analyzeAllFeatures = false;

        /// <summary>
        /// This is the phenotype label to analyze.
        /// </summary>
        private string _phenotypeLabelToAnalyze = String.Empty;

        /// <summary>
        /// This is the positive value of the phenotype label to analyze.
        /// </summary>
        private string _positiveLabel = String.Empty;

        /// <summary>
        /// This is the fully justified filename for the protein set spreadsheet.
        /// </summary>
        private string _proteinSetsSpreadsheetFilename = String.Empty;

        /// <summary>
        /// This is the worksheet name to use for the protein sets in the protein set spreadsheet.
        /// </summary>
        private string _proteinSetsWorksheetName = String.Empty;

        /// <summary>
        /// This is the column name for the column that contains protein IDs in the protein set spreadsheet.
        /// </summary>
        private string _proteinSetsProteinIDColumn = String.Empty;

        /// <summary>
        /// This is the list of protein sets to analyze.
        /// </summary>
        private List<string> _proteinSetsToAnalyze = new List<string>();

        /// <summary>
        /// If set to true, this indicates to analyze all defined protein sets in the protein sets file.
        /// </summary>
        private bool _analyzeAllProteinSets = false;

        /// <summary>
        /// If set to true, this indicates to output the permutations for all of the realizations to a file.
        /// </summary>
        private bool _outputPermutations = false;

        /// <summary>
        /// This is the fully justified filename for the analytes file.
        /// </summary>
        private string _analytesFilename = String.Empty;

        /// <summary>
        /// This is the worksheet name of the worksheet to use in the analytes spreadsheet file.
        /// </summary>
        private string _analytesWorksheetName = String.Empty;

        /// <summary>
        /// This is the name of the column in the analytes spreadsheet that contains the filenames.
        /// </summary>
        private string _analytesFilenameColumn = String.Empty;

        /// <summary>
        /// This is the number of realizations to use to compute the null ditribution of the enhancement score.
        /// </summary>
        private int _numRealizationsToComputeNullDist = -1;

        /// <summary>
        /// This is the number of data splits to use in the PSEA process.
        /// </summary>
        private int _numDataSplits = -1;

        /// <summary>
        /// This is the minimum number of samples allowed in a group for a data split or null shuffle of a data split.
        /// </summary>
        private int _minGroupSize = -1;

        /// <summary>
        /// This is the DataTable read from the specified input feature table spreadsheet, worksheet. 
        /// </summary>
        private DataTable _inputFeatureTableDataTable = null;

        /// <summary>
        /// This is the DataTable read from the specified analytes spreadsheet, worksheet.
        /// </summary>
        private DataTable _analytesDataTable = null;

        /// <summary>
        /// This is the DataTable read from the specified protein sets spreadsheet, worksheet.
        /// </summary>
        private DataTable _proteinSetsDataTable = null;
        #endregion

        /// <summary>
        /// This list contains the arguments that can be used with this tool
        /// </summary>
        private static List<string> _legalArgs = SetupLegalArgs();
        private static List<string> SetupLegalArgs()
        {
            List<string> retVal = new List<string>();
            retVal.Add(PSEAConstants.PSEA_FTR_TBL_SS_FILE_ARG);
            retVal.Add(PSEAConstants.PSEA_FTR_TBL_WS_NAME_ARG);
            retVal.Add(PSEAConstants.PSEA_FTR_TBL_FILENAME_COL_ARG);
            retVal.Add(PSEAConstants.PSEA_FTR_TBL_GROUPNAME_COL_ARG);
            retVal.Add(PSEAConstants.PSEA_FTR_ARG);
            retVal.Add(PSEAConstants.PSEA_ALL_FTRS_ARG);
            retVal.Add(PSEAConstants.PSEA_PFTR_ARG);
            retVal.Add(PSEAConstants.PSEA_POS_LABEL_ARG);
            retVal.Add(PSEAConstants.PSEA_PROTEIN_SET_SS_FILE_ARG);
            retVal.Add(PSEAConstants.PSEA_PROTEIN_SET_WS_NAME_ARG);
            retVal.Add(PSEAConstants.PSEA_PROTEIN_SET_PROTEIN_ID_COL_ARG);
            retVal.Add(PSEAConstants.PSEA_PROTEIN_SET_NAME_ARG);
            retVal.Add(PSEAConstants.PSEA_ALL_PROTEIN_SETS_ARG);
            retVal.Add(PSEAConstants.PSEA_ANALYTES_FILE_ARG);
            retVal.Add(PSEAConstants.PSEA_ANALYTES_WS_NAME_ARG);
            retVal.Add(PSEAConstants.PSEA_ANALYTES_FILENAME_COL_ARG);
            retVal.Add(PSEAConstants.PSEA_NUM_SPLITS_ARG);
            retVal.Add(PSEAConstants.PSEA_MIN_GRP_SZ_ARG);
            retVal.Add(PSEAConstants.PSEA_NUM_REALIZATIONS_ARG);
            retVal.Add(PSEAConstants.PSEA_OUTPUT_FOLDER_ARG);
            retVal.Add(PSEAConstants.PSEA_OUTPUT_PERMUTATIONS_ARG);
            return retVal;
        }
        #endregion

        #region Public Accessors
        /// <summary>
        /// Returns the fully justified path of the output directory.
        /// </summary>
        public string OutputDir
        {
            get { return this._outputDir; }
        }

        /// <summary>
        /// Returns the version of the currently executing assembly.
        /// </summary>
        public string ApplicationVersion
        {
            get { return this._applicationVersion; }
        }

        /// <summary>
        /// Returns the phenotype label to analyze.
        /// </summary>
        public string PhenotypeLabelToAnalyze
        {
            get { return this._phenotypeLabelToAnalyze; }
        }

        /// <summary>
        /// Returns the phenotyple label value to use as the positive label
        /// during PSEA analysis.
        /// </summary>
        public string PositiveLabel
        {
            get { return this._positiveLabel; }
        }

        /// <summary>
        /// Returns the number of realizations to use to compute the null distribution.
        /// </summary>
        public int NumRealizationsToComputeNullDist
        {
            get { return this._numRealizationsToComputeNullDist; }
        }

        /// <summary>
        /// Returns the number of data splits to use in the PSEA process.
        /// </summary>
        public int NumDataSplits
        {
            get { return this._numDataSplits; }
        }

        /// <summary>
        /// Returns the minimum group size when analyzing binary labels and using the data splitting
        /// strategy.  This is used to prevent have a group that contains too few members to reasonably calculate 
        /// the binary corellations.
        /// </summary>
        public int MinGroupSizeForLabelsAndSplits
        {
            get { return this._minGroupSize; }
        }

        /// <summary>
        /// Returns the input feature table, stored as a DataTable.
        /// </summary>
        public DataTable InputFeatureTableDataTable
        {
            get { return this._inputFeatureTableDataTable; }
        }

        /// <summary>
        /// Returns the column name of the filename column for the input feature table.
        /// </summary>
        public string FeatureTableFilenameColumn
        {
            get { return this._featureTableFilenameColumn; }
        }

        /// <summary>
        /// Returns the column name of the groupname column for the input feature table.
        /// </summary>
        public string FeatureTableGroupnameColumn
        {
            get { return this._featureTableGroupnameColumn; }
        }

        /// <summary>
        /// Returns the list of features to analyze using PSEA, this is the list of continuous (non-label) features
        /// that are specified on the command line.  This list may be over-ridden by using the "AnalyzeAllFeatures"
        /// flag, in which case this list is invalid.
        /// </summary>
        public List<string> FeaturesToAnalyze
        {
            get { return this._featuresToAnalyze; }
        }

        /// <summary>
        /// Returns true, if the user specifies to use all available continuous features in the input feature table.
        /// </summary>
        public bool AnalyzeAllFeatures
        {
            get { return this._analyzeAllFeatures; }
        }

        /// <summary>
        /// Returns the analytes table stored as a data table.
        /// </summary>
        public DataTable AnalytesDataTable
        {
            get { return this._analytesDataTable; }
        }

        /// <summary>
        /// Returns the name of the column that contains the filename in the analytes data table.
        /// </summary>
        public string AnalytesFilenameColumn
        {
            get { return this._analytesFilenameColumn; }
        }

        /// <summary>
        /// Returns the protein sets table as a DataTable.
        /// </summary>
        public DataTable ProteinSetsDataTable
        {
            get { return this._proteinSetsDataTable; }
        }

        /// <summary>
        /// Returns the column name in the protein sets table that contains the protein IDs.
        /// </summary>
        public string ProteinSetsProteinIDColumn
        {
            get { return this._proteinSetsProteinIDColumn; }
        }

        /// <summary>
        /// Returns true, if the user specifies to analyze all protein sets available in the protein sets table.
        /// </summary>
        public bool AnalyzeAllProteinSets
        {
            get { return this._analyzeAllProteinSets; }
        }

        /// <summary>
        /// Returns whether or not the user specified to output the permutations from all of the realizations to 
        /// a file.  The default value is false.
        /// </summary>
        public bool OutputPermutations
        {
            get { return this._outputPermutations; }
        }

        /// <summary>
        /// Returns the list of protein set names to analyze.  This may be overridden if the user specifies to 
        /// analyze all protein sets.
        /// </summary>
        public List<string> ProteinSetsToAnalyze
        {
            get { return this._proteinSetsToAnalyze; }
        }
        #endregion

        /// <summary>
        /// The constructor for the ArgParser class. Verfies that the correct argument values are present.
        /// </summary>
        /// <param name="args">Command line arguments entered by the user.</param>
        public ArgParser(string[] args)
        {
            // If no args passed, display the usage statement
            if (args.Length == 0)
            {
                throw new Exception("No arguments found - PSEA (ver " + this._applicationVersion + ") usage information.\n" + this._usage);
            }

            // Call the method that will read the values from the file and validate the required entries.
            this.ParseArguments(args);

            // Validate that the entries are coherent and the input data is good.
            this.ValidateEntries();
        }

        /// <summary>
        /// This method will read and validate the command line arguments.
        /// </summary>
        /// <param name="args">Command line arguments</param>
        private void ParseArguments(string[] args)
        {
            // this is the index within the argument list.
            int argIndex = 0;

            // if the arg is a question mark then throw the asked for usage exception.
            if ((args.Length > 0) && (args[0].Equals(PSEAConstants.PSEA_USAGE_ARG)))
            {
                throw new Exception("You asked for PSEA (ver " + this._applicationVersion + ") usage information.\n" + this._usage);
            }

            // loop over all the arguments.
            while (argIndex < args.Length)
            {
                // trim the argument.
                string s = args[argIndex].Trim();
                if (s.Equals(PSEAConstants.PSEA_OUTPUT_FOLDER_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_OUTPUT_FOLDER_ARG + "' argument. "
                            + "Please specify a fully justfied path to the output directory.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_OUTPUT_FOLDER_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the folder has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._outputDir))
                    {
                        throw new Exception("Use Error -- Multiple output directories were specified.");
                    }
                    else
                    {
                        // set the output directory
                        this._outputDir = s;
                        if (!Directory.Exists(this._outputDir))
                        {
                            throw new Exception("Use Error -- The output directory '" + this._outputDir + "' does not exist.");
                        }
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_FTR_TBL_SS_FILE_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_FTR_TBL_SS_FILE_ARG + "' argument. "
                            + "Please specify a fully justfied filename for the input feature table.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_FTR_TBL_SS_FILE_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the folder has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._featureTableSpreadsheetFilename))
                    {
                        throw new Exception("Use Error -- Multiple feature tables were specified.");
                    }
                    else
                    {
                        // set the feature table ss filename
                        this._featureTableSpreadsheetFilename = s;
                        if (!File.Exists(this._featureTableSpreadsheetFilename))
                        {
                            throw new Exception("Use Error -- The feature table file '" + this._featureTableSpreadsheetFilename + "' does not exist.");
                        }
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_FTR_TBL_WS_NAME_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_FTR_TBL_WS_NAME_ARG + "' argument. "
                            + "Please specify a worksheet name for the input feature table.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_FTR_TBL_WS_NAME_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the folder has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._featureTableWorksheetName))
                    {
                        throw new Exception("Use Error -- Multiple feature table worksheet names were specified.");
                    }
                    else
                    {
                        // set the feature table ss filename
                        this._featureTableWorksheetName = s;
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_FTR_TBL_FILENAME_COL_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_FTR_TBL_FILENAME_COL_ARG + "' argument. "
                            + "Please specify a filename column for the input feature table.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_FTR_TBL_FILENAME_COL_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the folder has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._featureTableFilenameColumn))
                    {
                        throw new Exception("Use Error -- Multiple feature table filename columns were specified.");
                    }
                    else
                    {
                        // set the feature table ss filename
                        this._featureTableFilenameColumn = s;
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_FTR_TBL_GROUPNAME_COL_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_FTR_TBL_GROUPNAME_COL_ARG + "' argument. "
                            + "Please specify a groupname column for the input feature table.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_FTR_TBL_GROUPNAME_COL_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the folder has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._featureTableGroupnameColumn))
                    {
                        throw new Exception("Use Error -- Multiple feature table groupname columns were specified.");
                    }
                    else
                    {
                        // set the feature table ss filename
                        this._featureTableGroupnameColumn = s;
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_FTR_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_FTR_ARG + "' argument. "
                            + "Please specify a feature to analyze.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_FTR_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // add the feature to the list of those to analyze
                    if (!this._featuresToAnalyze.Contains(s))
                    {
                        this._featuresToAnalyze.Add(s);
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_ALL_FTRS_ARG))
                {
                    // set the flag to analyze all features in the input feature table.
                    this._analyzeAllFeatures = true;
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_OUTPUT_PERMUTATIONS_ARG))
                {
                    // set the flag to output all permutations to a file.
                    this._outputPermutations = true;
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_PFTR_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_PFTR_ARG + "' argument. "
                            + "Please specify a phenotype label to analyze.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_PFTR_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the folder has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._phenotypeLabelToAnalyze))
                    {
                        throw new Exception("Use Error -- Multiple phenotype labels to analyze were specified.  This is not currently supported.");
                    }
                    else
                    {
                        // set the feature table ss filename
                        this._phenotypeLabelToAnalyze = s;
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_POS_LABEL_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_POS_LABEL_ARG + "' argument. "
                            + "Please specify the positive label for the phenotype label to analyze.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_POS_LABEL_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the folder has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._positiveLabel))
                    {
                        throw new Exception("Use Error -- Multiple positive labels for the phenotype label to analyze were specified.");
                    }
                    else
                    {
                        // set the feature table ss filename
                        this._positiveLabel = s;
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_PROTEIN_SET_SS_FILE_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_PROTEIN_SET_SS_FILE_ARG + "' argument. "
                            + "Please specify a fully justfied filename for the protein sets spreadsheet file.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_PROTEIN_SET_SS_FILE_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the attribute has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._proteinSetsSpreadsheetFilename))
                    {
                        throw new Exception("Use Error -- Multiple protein set filenames were specified.");
                    }
                    else
                    {
                        // set the protein set filename
                        this._proteinSetsSpreadsheetFilename = s;
                        if (!File.Exists(this._proteinSetsSpreadsheetFilename))
                        {
                            throw new Exception("Use Error -- The protein set file '" + this._proteinSetsSpreadsheetFilename + "' does not exist.");
                        }
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_PROTEIN_SET_WS_NAME_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_PROTEIN_SET_WS_NAME_ARG + "' argument. "
                            + "Please specify a worksheet name for the input protein sets file.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_PROTEIN_SET_WS_NAME_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the folder has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._proteinSetsWorksheetName))
                    {
                        throw new Exception("Use Error -- Multiple protein set worksheet names were specified.");
                    }
                    else
                    {
                        // set the protein set worksheet name
                        this._proteinSetsWorksheetName = s;
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_PROTEIN_SET_PROTEIN_ID_COL_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_PROTEIN_SET_PROTEIN_ID_COL_ARG + "' argument. "
                            + "Please specify a protein set protein ID column for the input protein sets file.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_PROTEIN_SET_PROTEIN_ID_COL_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the folder has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._proteinSetsProteinIDColumn))
                    {
                        throw new Exception("Use Error -- Multiple protein set protein ID columns were specified.");
                    }
                    else
                    {
                        // set the feature table ss filename
                        this._proteinSetsProteinIDColumn = s;
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_PROTEIN_SET_NAME_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_PROTEIN_SET_NAME_ARG + "' argument. "
                            + "Please specify a protein set to analyze.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_PROTEIN_SET_NAME_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // add the feature to the list of those to analyze
                    if (!this._proteinSetsToAnalyze.Contains(s))
                    {
                        this._proteinSetsToAnalyze.Add(s);
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_ALL_PROTEIN_SETS_ARG))
                {
                    // set the flag to analyze all protein sets in the protein sets file.
                    this._analyzeAllProteinSets = true;
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_ANALYTES_FILE_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_ANALYTES_FILE_ARG + "' argument. "
                            + "Please specify a fully justfied filename for the analytes spreadsheet file.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_ANALYTES_FILE_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the attribute has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._analytesFilename))
                    {
                        throw new Exception("Use Error -- Multiple analytes filenames were specified.");
                    }
                    else
                    {
                        // set the protein set filename
                        this._analytesFilename = s;
                        if (!File.Exists(this._analytesFilename))
                        {
                            throw new Exception("Use Error -- The analytes file '" + this._analytesFilename + "' does not exist.");
                        }
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_ANALYTES_WS_NAME_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_ANALYTES_WS_NAME_ARG + "' argument. "
                            + "Please specify a worksheet name for the input analytes file.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_ANALYTES_WS_NAME_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the folder has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._analytesWorksheetName))
                    {
                        throw new Exception("Use Error -- Multiple analytes worksheet names were specified.");
                    }
                    else
                    {
                        // set the protein set worksheet name
                        this._analytesWorksheetName = s;
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_ANALYTES_FILENAME_COL_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_ANALYTES_FILENAME_COL_ARG + "' argument. "
                            + "Please specify a analytes filename column for the input analytes file.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_ANALYTES_FILENAME_COL_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the folder has already been read, then throw an exception, multiple specified.
                    if (!String.IsNullOrEmpty(this._analytesFilenameColumn))
                    {
                        throw new Exception("Use Error -- Multiple analytes filename columns were specified.");
                    }
                    else
                    {
                        // set the feature table ss filename
                        this._analytesFilenameColumn = s;
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_NUM_REALIZATIONS_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_NUM_REALIZATIONS_ARG + "' argument. "
                            + "Please specify a number of realizations to use in computing the null ditribution of the enhancement score.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_NUM_REALIZATIONS_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the folder has already been read, then throw an exception, multiple specified.
                    if (this._numRealizationsToComputeNullDist != -1)
                    {
                        throw new Exception("Use Error -- Multiple numbers of realizations to use are specified.");
                    }
                    else
                    {
                        // set the feature table ss filename
                        this._numRealizationsToComputeNullDist = Convert.ToInt32(s);
                        if (this._numRealizationsToComputeNullDist <= 0)
                        {
                            string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_NUM_REALIZATIONS_ARG + "' argument. "
                            + "Please specify a valid value greater than zero and try again.";
                            throw new Exception(errorMsg);
                        }
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_NUM_SPLITS_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_NUM_SPLITS_ARG + "' argument. "
                            + "Please specify a number of data splits to use in the PSEA analysis.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_NUM_SPLITS_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the num splits has already been read, then throw an exception, multiple specified.
                    if (this._numDataSplits != -1)
                    {
                        throw new Exception("Use Error -- Multiple numbers of data splits to use are specified.");
                    }
                    else
                    {
                        // set the feature table ss filename
                        this._numDataSplits = Convert.ToInt32(s);
                        if (this._numDataSplits <= 0)
                        {
                            string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_NUM_SPLITS_ARG + "' argument. "
                            + "Please specify a valid value greater than zero and try again.";
                            throw new Exception(errorMsg);
                        }
                    }
                    argIndex++;
                }
                else if (s.Equals(PSEAConstants.PSEA_MIN_GRP_SZ_ARG))
                {
                    argIndex++;
                    if (argIndex >= args.Length)
                    {
                        string errorMsg = "Use Error -- No value specified for the '" + PSEAConstants.PSEA_MIN_GRP_SZ_ARG + "' argument. "
                            + "Please specify a minimum group size to use in the PSEA analysis.";
                        throw new Exception(errorMsg);
                    }
                    s = args[argIndex].Trim();
                    if (IsLegalArg(s))
                    {
                        string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_MIN_GRP_SZ_ARG + "' argument. "
                            + "Please specify a valid value and try again.";
                        throw new Exception(errorMsg);
                    }
                    // if the num splits has already been read, then throw an exception, multiple specified.
                    if (this._minGroupSize != -1)
                    {
                        throw new Exception("Use Error -- Multiple minimum group sizes are specified.");
                    }
                    else
                    {
                        // set the feature table ss filename
                        this._minGroupSize = Convert.ToInt32(s);
                        if (this._minGroupSize <= 0)
                        {
                            string errorMsg = "Use Error -- An illegal value was specified for the '" + PSEAConstants.PSEA_MIN_GRP_SZ_ARG + "' argument. "
                            + "Please specify a valid value greater than zero and try again.";
                            throw new Exception(errorMsg);
                        }
                    }
                    argIndex++;
                }
                else //invalid argument
                {
                    throw new Exception("Use Error -- An invalid argument was specified '" + s + "'.");
                }
            }
        }

        /// <summary>
        /// This method verifies that the minimum required input data have been supplied.  An exception is thrown with an 
        /// appropriate error message if the required inputs have not been received.
        /// </summary>
        private void ValidateEntries()
        {
            #region Check for Required Arguments

            #region Input Feature Table
            if (String.IsNullOrEmpty(this._featureTableSpreadsheetFilename))
            {
                throw new Exception(
                    "Use Error -- Please specify an input feature table spreadsheet using the " + PSEAConstants.PSEA_FTR_TBL_SS_FILE_ARG + " argument.");
            }

            if (String.IsNullOrEmpty(this._featureTableWorksheetName))
            {
                throw new Exception(
                    "Use Error -- Please specify an input feature table worksheet name using the " + PSEAConstants.PSEA_FTR_TBL_WS_NAME_ARG + " argument.");
            }

            if (String.IsNullOrEmpty(this._featureTableFilenameColumn))
            {
                throw new Exception(
                    "Use Error -- Please specify an input feature table filename column name using the " + PSEAConstants.PSEA_FTR_TBL_FILENAME_COL_ARG + " argument.");
            }

            if (String.IsNullOrEmpty(this._featureTableGroupnameColumn))
            {
                throw new Exception(
                    "Use Error -- Please specify an input feature table groupname column name using the " + PSEAConstants.PSEA_FTR_TBL_GROUPNAME_COL_ARG + " argument.");
            }

            try
            {
                this._inputFeatureTableDataTable = OpenXmlExcelReader.GetDataTable(this._featureTableSpreadsheetFilename, this._featureTableWorksheetName);
            }
            catch (Exception ex)
            {
                throw new Exception("Error -- Could not open the specified input feature table file " + this._featureTableSpreadsheetFilename +
                    ". The error is " + ex.Message);
            }

            if (!this._inputFeatureTableDataTable.Columns.Contains(this._featureTableFilenameColumn))
            {
                throw new Exception(
                    "Use Error -- The specified input feature table spreadsheet/worksheet does not contain the filename column " +
                    this._featureTableFilenameColumn + ". Please check your input feature table spreadsheet.");
            }

            if (!this._inputFeatureTableDataTable.Columns.Contains(this._featureTableGroupnameColumn))
            {
                throw new Exception(
                    "Use Error -- The specified input feature table spreadsheet/worksheet does not contain the groupname column " +
                    this._featureTableGroupnameColumn + ". Please check your input feature table spreadsheet.");
            }
            #endregion

            #region Input Analytes Table
            if (String.IsNullOrEmpty(this._analytesFilename))
            {
                throw new Exception(
                    "Use Error -- Please specify an input analytes file using the " + PSEAConstants.PSEA_ANALYTES_FILE_ARG + " argument.");
            }
            else
            {
                if (this._analytesFilename.EndsWith(".xlsx"))
                {
                    if (String.IsNullOrEmpty(this._analytesWorksheetName))
                    {
                        throw new Exception(
                            "Use Error -- Please specify an input analytes worksheet name using the " + PSEAConstants.PSEA_ANALYTES_WS_NAME_ARG + " argument.");
                    }
                }
            }

            if (String.IsNullOrEmpty(this._analytesFilenameColumn))
            {
                throw new Exception(
                    "Use Error -- Please specify an input analytes filename column name using the " + PSEAConstants.PSEA_ANALYTES_FILENAME_COL_ARG + " argument.");
            }

            try
            {
                if (this._analytesFilename.ToLower().EndsWith(".xlsx"))
                {
                    this._analytesDataTable = OpenXmlExcelReader.GetDataTable(this._analytesFilename, this._analytesWorksheetName);
                }
                else if (this._analytesFilename.ToLower().EndsWith(".csv"))
                {
                    string[,] analytes2DArray = Local.Util.TwoDimensionalStringArrayUtils.ReadCVSFile(this._analytesFilename);
                    this._analytesDataTable = Local.Util.TwoDimensionalStringArrayUtils.Convert2DStringToDataTable(analytes2DArray);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error -- Could not open the specified analytes file " + this._analytesFilename +
                    ". The error is " + ex.Message);
            }

            if (!this._analytesDataTable.Columns.Contains(this._analytesFilenameColumn))
            {
                throw new Exception(
                    "Use Error -- The specified input analytes spreadsheet/worksheet does not contain the filename column " +
                    this._analytesFilenameColumn + ". Please check your input analytes spreadsheet.");
            }
            #endregion

            #region Input Protein Sets Table
            if (String.IsNullOrEmpty(this._proteinSetsSpreadsheetFilename))
            {
                throw new Exception(
                    "Use Error -- Please specify an input protein sets spreadsheet using the " +
                    PSEAConstants.PSEA_PROTEIN_SET_SS_FILE_ARG + " argument.");
            }

            if (String.IsNullOrEmpty(this._proteinSetsWorksheetName))
            {
                throw new Exception(
                    "Use Error -- Please specify an input protein sets worksheet name using the " +
                    PSEAConstants.PSEA_PROTEIN_SET_WS_NAME_ARG + " argument.");
            }

            if (String.IsNullOrEmpty(this._proteinSetsProteinIDColumn))
            {
                throw new Exception(
                    "Use Error -- Please specify an input protein sets protein ID column name using the " +
                    PSEAConstants.PSEA_PROTEIN_SET_PROTEIN_ID_COL_ARG + " argument.");
            }

            try
            {
                this._proteinSetsDataTable = OpenXmlExcelReader.GetDataTable(this._proteinSetsSpreadsheetFilename, this._proteinSetsWorksheetName);
            }
            catch (Exception ex)
            {
                throw new Exception("Error -- Could not open the specified protein sets file " + this._proteinSetsSpreadsheetFilename +
                    ". The error is " + ex.Message);
            }

            if (!this._proteinSetsDataTable.Columns.Contains(this._proteinSetsProteinIDColumn))
            {
                throw new Exception(
                    "Use Error -- The specified input protein sets spreadsheet/worksheet does not contain the protein ID column " +
                    this._proteinSetsProteinIDColumn + ". Please check your input protein sets spreadsheet.");
            }

            List<Tuple<string, string>> errorCells = new List<Tuple<string, string>>();
            foreach (DataRow dr in this._proteinSetsDataTable.Rows)
            {
                foreach (DataColumn dc in this._proteinSetsDataTable.Columns)
                {
                    if (!dc.ColumnName.Equals(this._proteinSetsProteinIDColumn))
                    {
                        string value = dr[dc].ToString().Trim().ToLower();
                        if (!(value.Equals(PSEAConstants.PROTEIN_SET_YES)) && !(value.Equals(PSEAConstants.PROTEIN_SET_NO)))
                        {
                            Tuple<string, string> errorCell = new Tuple<string, string>(dr[this._proteinSetsProteinIDColumn].ToString(), dc.ColumnName);
                            errorCells.Add(errorCell);
                        }
                    }
                }
            }

            if (errorCells.Count > 0)
            {
                string errorMsg = "Data Error -- The Protein Sets Table contains values that are not \"yes\" or \"no\" for the following row,column pairs:";
                foreach (Tuple<string, string> errorCell in errorCells)
                {
                    errorMsg += ("\n\t" + errorCell.Item1 + ", " + errorCell.Item2);
                }
                throw new Exception(errorMsg);
            }

            #endregion

            // Number of Realizations to compute Null Distribution of Enhancement Score
            if (this._numRealizationsToComputeNullDist <= 0)
            {
                throw new Exception(
                    "Use Error -- Please specify a number of realizations (greater than zero) to use to compute the null distribution of the enhancement score using the " +
                    PSEAConstants.PSEA_NUM_REALIZATIONS_ARG + " argument.");
            }

            // Output Directory
            if (String.IsNullOrEmpty(this._outputDir))
            {
                throw new Exception(
                    "Use Error -- Please specify an output directory for the processing results using the " +
                    PSEAConstants.PSEA_OUTPUT_FOLDER_ARG + " argument.");
            }

            #endregion

            #region Check the Data in the Required Arguments

            #region Check that the feature table filenames exist in the analytes file.
            List<string> theAnalytesFiles = DataTableUtils.GetDistinctValuesForColumn(this._analytesDataTable, this._analytesFilenameColumn);
            List<string> theFeatureTableFilenames = DataTableUtils.GetDistinctValuesForColumn(this._inputFeatureTableDataTable, this._featureTableFilenameColumn);
            List<string> missing = new List<string>();

            foreach (string s in theFeatureTableFilenames)
            {
                if (!(theAnalytesFiles.Contains(s)))
                {
                    missing.Add(s);
                }
            }

            if (missing.Count > 0)
            {
                string msg = "Data Mismatch Error -- The input feature table contains the following files that are not present in the analytes file: \n";
                foreach (string s in missing)
                {
                    msg += "\t" + s + "\n";
                }
                throw new Exception(msg);
            }
            #endregion

            #region Check that the protein ids in the protein sets are in the column headers of the analytes file.
            List<string> theProteins = DataTableUtils.GetDistinctValuesForColumn(this._proteinSetsDataTable, this._proteinSetsProteinIDColumn);
            List<string> missingProteins = new List<string>();

            foreach (string s in theProteins)
            {
                if (!(this._analytesDataTable.Columns.Contains(s)))
                {
                    missingProteins.Add(s);
                }
            }
            if (missingProteins.Count > 0)
            {
                string msg = "Data Mismatch Error -- The protein sets contain the following protein ids that are not in the analytes file:\n";
                foreach (string s in missingProteins)
                {
                    msg += "\t" + s + "\n";
                }
                throw new Exception(msg);
            }
            #endregion

            #endregion

            #region Check the Optional Arguments

            #region Number of Data Splits
            // Check to make sure the number of intput data rows is even.
            if (this._numDataSplits > 0)
            {
                if (this._inputFeatureTableDataTable.Rows.Count % 2 != 0)
                {
                    throw new Exception(
                        "Use Error -- When specifying the number of data set splits, the input table must have an even number of samples to do a 50/50 split.  There are " +
                        this._inputFeatureTableDataTable.Rows.Count.ToString() + " sample rows - " +
                        PSEAConstants.PSEA_NUM_SPLITS_ARG + " argument.");
                }
            }
            #endregion

            #region Continuous Features To Analyze
            if (!this._analyzeAllFeatures)
            {
                List<string> missingFeatures = new List<string>();
                foreach (string featureName in this._featuresToAnalyze)
                {
                    if (!this._inputFeatureTableDataTable.Columns.Contains(featureName))
                    {
                        missingFeatures.Add(featureName);
                    }
                }

                if (missingFeatures.Count > 0)
                {
                    string msg = "Data Error -- The following feature names specified on the command line do not exist in the input feature table:\n";
                    foreach (string s in missingFeatures)
                    {
                        msg += "\t" + s + "\n";
                    }
                    throw new Exception(msg);
                }
            }
            #endregion

            #region Phenotype Label To Analyze
            if (!String.IsNullOrEmpty(this._phenotypeLabelToAnalyze))
            {
                if (String.IsNullOrEmpty(this._positiveLabel))
                {
                    throw new Exception(
                        "Use Error -- Please specify a positive label for the phenotype label " + this._phenotypeLabelToAnalyze +
                        " using the argument " + PSEAConstants.PSEA_POS_LABEL_ARG + ".");
                }

                if (!this._inputFeatureTableDataTable.Columns.Contains(this._phenotypeLabelToAnalyze))
                {
                    throw new Exception(
                        "Data Error -- The phenotype label to analyze, " + this._phenotypeLabelToAnalyze + ", does not exist in the input feature table " +
                        this._featureTableSpreadsheetFilename + ". Please check your input feature table and command line arguments.");
                }

                List<string> distinctValues = DataTableUtils.GetDistinctValuesForColumn(this._inputFeatureTableDataTable, this._phenotypeLabelToAnalyze);

                if (distinctValues.Count > 2)
                {
                    throw new Exception(
                        "Data Error -- The phenotype label to analyze column in the input feature table must contain 2 distinct values.  There are " +
                        distinctValues.Count.ToString() + ". Please check your input feature table.");
                }
                else if (distinctValues.Count < 2)
                {
                    throw new Exception(
                        "Data Error -- The phenotype label to analyze column in the input feature table must contain 2 distinct values.  There are " +
                        distinctValues.Count.ToString() + ". Please check your input feature table.");
                }

                if (!distinctValues.Contains(this._positiveLabel))
                {
                    throw new Exception(
                        "Data Error -- The positive label, " + this._positiveLabel + " for analyzing the phenotype label, " +
                        this._phenotypeLabelToAnalyze + ", does not exist in the input feature table. Please check your input feature table.");
                }

                // If we're doing labels with data splits, check to ensure that the mingrpsz argument is valid (less than the smaller of the two group sizes)
                if (this._numDataSplits > 0)
                {
                    if (this._minGroupSize <= 0)
                    {
                        string errorMsg = "Use Error -- If data splits are used with analyzing labels, the -mingrpsz argument is required.  Please pass a value greater than zero for that argument.";
                        throw new Exception(errorMsg);
                    }

                    int numberOfPosLabelRows = 0;
                    int numberOfNegLabelRows = 0;
                    foreach (DataRow dr in this._inputFeatureTableDataTable.Rows)
                    {
                        string label = dr[this._phenotypeLabelToAnalyze].ToString();

                        if (label.Equals(this._positiveLabel))
                        {
                            numberOfPosLabelRows++;
                        }
                        else
                        {
                            numberOfNegLabelRows++;
                        }
                    }

                    if (this._minGroupSize > Math.Min(numberOfPosLabelRows, numberOfNegLabelRows))
                    {
                        throw new Exception(
                        "Use Error -- The specified minimum group size, " + this._minGroupSize.ToString() + ", for analyzing labels while using data splits, for analyzing the phenotype label, " +
                        this._phenotypeLabelToAnalyze + ", is too large. Please make sure the value is less than the size of the smaller group.");
                    }
                }
            }
            #endregion

            #region Protein Sets to Analyze
            if (!this._analyzeAllProteinSets)
            {
                List<string> missingProteinSets = new List<string>();
                foreach (string proteinSet in this._proteinSetsToAnalyze)
                {
                    if (proteinSet.Equals(this._proteinSetsProteinIDColumn))
                    {
                        throw new Exception("Argument Error -- The Protein ID column, " + this._proteinSetsProteinIDColumn + ", cannot be specified as a protein set to analyze.");
                    }

                    if (!this._proteinSetsDataTable.Columns.Contains(proteinSet))
                    {
                        missingProteinSets.Add(proteinSet);
                    }
                }

                if (missingProteinSets.Count > 0)
                {
                    string msg = "Data Error -- The following protein sets specified on the command line do not exist in the input protein sets file:\n";
                    foreach (string s in missingProteinSets)
                    {
                        msg += "\t" + s + "\n";
                    }
                    throw new Exception(msg);
                }
            }
            #endregion

            #endregion
        }

        /// <summary>
        /// This method determines if the argument used is legal.
        /// </summary>
        /// <param name="s">the argument used.</param>
        /// <returns>true if the arg is legal, false if the arg is not legal</returns>
        private static bool IsLegalArg(string s)
        {
            // returns true if the argument is contained within the list.
            return _legalArgs.Contains(s);
        }
    }
}
