﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: FeatureTable.cs
//
//      (C) Copyright 2018 Biodesix Inc.
//      All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Xml;
using System.IO;

namespace Local.Util
{
    /// <summary>
    /// A Feature Table is a data table with two mandatory columns - filename column and group column.
    /// The rest of the columns are features with feature values in the rows.  In order to be a valid 
    /// feature table, all rows must be populated with non-null, non-empty string, values for every column.
    /// A utility class FeatureTableUtils is provided as well to accomplish common operations using a
    /// Feature Table object.
    /// 
    /// Initializing the Feature Table:
    /// 
    /// There are two ways to initialize the data in this feature table.
    /// 1) Load from an xml file
    ///         If this method is used, the feature table is read entirely from xml.  The filename
    ///         column and group column are read from the xml, the data table name is read from the
    ///         xml file and if that attribute of the FeatureTable object is empty string, then the 
    ///         table name read from the xml is used.  So the default constructor may be used in this 
    ///         case as follows:
    ///             FeatureTable theFeatureTable = new FeatureTable();
    ///             theFeatureTable.LoadFromXml(rootNodeOfFeatureTableXmlFile);
    ///         If the user wishes to use a different table name they could do as follows:
    ///             FeatureTable theFeatureTable = new FeatureTable("TheTableName", "", "");
    ///             theFeatureTable.LoadFromXml(rootNodeOfFeatureTableXmlFile);
    /// 2) Load from a different provided DataTable (such as one obtained from an Excel Worksheet)
    ///         If this method is used, the feature table must be created given a filename column name and
    ///         group column name so that the provided DataTable can be checked.  If a non-empty table
    ///         name is given, then it will be used in the FeatureTable, if not then the provided
    ///         DataTable table name will be used.  Example to use table name from provided data table:
    ///             FeatureTable theFeatureTable = new FeatureTable("", "FilenameColumnName", "GroupColumnName");
    ///             theFeatureTable.SetDataFromTable(dataTableFromExcelWorksheet);
    ///         Example to use new table name provided in constructor:
    ///             FeatureTable theFeatureTable = new FeatureTable("NewTableName", "FilenameColumnName", "GroupColumnName");
    ///             theFeatureTable.SetDataFromTable(dataTableFromExcelWorksheet);
    ///         As stated above if either of the required columns are missing from the provided data
    ///         table, an exception is thrown.
    /// 
    /// </summary>
    public class FeatureTable : DataTable
    {
        /// <summary>
        /// Name of the column in the table that contains the filenames.
        /// </summary>
        protected string _filenameColumnName = "";

        /// <summary>
        /// Name of the column in the table that contains the groups
        /// </summary>
        protected string _groupColumnName = "";

        /// <summary>
        /// Public accessor for filename column name.
        /// </summary>
        public string FilenameColumnName
        {
            get { return this._filenameColumnName; }
        }

        /// <summary>
        /// Public accessor for group column name.
        /// </summary>
        public string GroupColumnName
        {
            get { return this._groupColumnName; }
        }

        /// <summary>
        /// Default constructor, for use in conjunction with loading from xml.
        /// </summary>
        /// <param name="filenameColumnName">the filename column</param>
        /// <param name="groupColumnName">the group label column</param>
        public FeatureTable()
        {
            this._filenameColumnName = "";
            this._groupColumnName = "";
            this.TableName = "";
        }
        
        /// <summary>
        /// Constructor - must provide feature table name (necessary for using WriteXml),
        /// filename and group column names.
        /// </summary>
        /// <param name="filenameColumnName">the filename column</param>
        /// <param name="groupColumnName">the group label column</param>
        public FeatureTable(string featureTableName, string filenameColumnName, string groupColumnName)
        {
            this._filenameColumnName = filenameColumnName;
            this._groupColumnName = groupColumnName;
            this.TableName = featureTableName;
        }

        /// <summary>
        /// Copy Contructor, deep copy, all data is copied.
        /// </summary>
        /// <param name="ftToCopy">Feature Table to make a copy of.</param>
        public FeatureTable(FeatureTable ftToCopy)
        {
            this.TableName = ftToCopy.TableName;
            this._filenameColumnName = ftToCopy.FilenameColumnName;
            this._groupColumnName = ftToCopy.GroupColumnName;
            this.SetDataFromTable(ftToCopy);
        }
        
        /// <summary>
        /// This method takes the data in the provided data table and copies it into this FeatureTable
        /// object.  If the provided data table does not contain the two required columns (filename column
        /// name, group column name) then an exception is thrown.
        /// </summary>
        /// <param name="dt">the data table to use in populating the feature table</param>
        public void SetDataFromTable(DataTable dt)
        {
            // Check to make sure filename and group columns exist in table, if so then copy table
            if ((dt.Columns.Contains(this._filenameColumnName)) && (dt.Columns.Contains(this._groupColumnName)))
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    this.Columns.Add(new DataColumn(dc.ColumnName));
                }
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow newdr = this.NewRow();
                    foreach (DataColumn dc in dt.Columns)
                    {
                        newdr[dc.ColumnName] = dr[dc];
                    }
                    this.Rows.Add(newdr);
                }

                // If we do not have a data table name yet, use the same as the provided table
                if (this.TableName == "")
                {
                    this.TableName = dt.TableName;
                }
            }
            else
            {
                throw new Exception("Feature Table -- SetDataFromTable Error -- Provided table does not contain the columns [" + this._filenameColumnName + ", " + this._groupColumnName + "].");
            }
        }

               
    }
}
