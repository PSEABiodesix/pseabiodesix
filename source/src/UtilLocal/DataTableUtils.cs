﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: DataTableUtils.cs
//
//      (C) Copyright 2018 Biodesix Inc.
//      All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data;

namespace Local.Util
{
    /// <summary>
    /// this class contains methods that can be used to process, and sort DataTables.
    /// </summary>
    public static class DataTableUtils
    {       

        /// <summary>
        /// this method returns the list of distinct values that are present in within the specified
        /// column of the specified data table
        /// </summary>
        /// <param name="theDataTable">the data table to process</param>
        /// <param name="columnName">the column name for which to get the distinct values</param>
        /// <returns>the list of distinct values within the specified column</returns>
        public static List<string> GetDistinctValuesForColumn(DataTable theDataTable, string columnName)
        {
            List<string> distinctValues = new List<string>();

            var result = (from row in theDataTable.AsEnumerable()
                          where !(String.IsNullOrEmpty(row.Field<string>(columnName)))
                          select (string)(row.Field<string>(columnName)));

            if (result != null)
            {
                List<string> theResults = new List<string>(result);
                if (theResults.Count == 0)
                {
                    throw new Exception("Error Reading the unique values. no unique values found.");
                }


                foreach (string s in theResults)
                {
                    string theVal = s;
                    if ((theVal != null) && (theVal.Length > 0))
                    {
                        if (!(distinctValues.Contains(theVal)))
                        {
                            distinctValues.Add(theVal);
                        }
                    }
                }
            }
            else
            {
                throw new Exception("select distinct values failed");
            }
            return distinctValues;
        }
 

    }
}
