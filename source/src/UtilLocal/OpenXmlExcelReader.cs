﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: OpenXmlExcelReader.cs
//
//      (C) Copyright 2018 Biodesix Inc.
//      All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;

namespace Local.Util
{
    /// <summary>
    /// this class implements the methods used to access information contained within Excel.
    /// the class can be used to read information from an MS Excel 2007 or higher document. This class uses
    /// the OpenXML interface to excel, so there is no limitations to using this other than the
    /// excel file must be 2007 or a more recent version of Excel. the spreadsheet file to be used must be .xlsx.
    /// Limitations: this class does not yet handle password protected spreadsheets.
    /// </summary>
    public class OpenXmlExcelReader
    {
        /// <summary>
        /// THis is the max length allowed for an excel filename, the application issues errors if the filename is
        /// longer than this, and the errors are not quite user friendly.
        /// </summary>
        public static int MAX_LENGTH_EXCEL_FULLY_JUSTIFIED_FILENAME = 218;
        /// <summary>
        /// This method should be used to check that the excel filename is under the legal length for the excel application.
        /// the excel app presently reports errors that are not as clear as they might be for this error.
        /// </summary>
        /// <param name="fullyJustifiedExcelFilename">the fully justified filename for the excel file</param>
        public static void CheckLengthOFExcelFilename(string fullyJustifiedExcelFilename)
        {
            if (fullyJustifiedExcelFilename.Length > MAX_LENGTH_EXCEL_FULLY_JUSTIFIED_FILENAME)
            {
                throw new Exception(" OpenXMLExcelReader -- illegal filename, the excel application allows filenames not in excess of " + MAX_LENGTH_EXCEL_FULLY_JUSTIFIED_FILENAME + " characters, your filename " + fullyJustifiedExcelFilename + " is " + fullyJustifiedExcelFilename.Length + " characters, please relocate the file to another location, or shorten the filename to be less than " + MAX_LENGTH_EXCEL_FULLY_JUSTIFIED_FILENAME + " characters.");
            }
        }
        /// <summary>
        /// THis is the max length allowed for an excel worksheet, the application issues errors if the worksheet is
        /// longer than this, and the errors are not quite user friendly.
        /// </summary>
        public static int MAX_LENGTH_EXCEL_WORKSHEET_NAME = 31;
        /// <summary>
        /// This method should be used to check that the excel worksheet is under the legal length for the excel application.
        /// the excel app presently reports errors that are not as clear as they might be for this error.
        /// </summary>
        /// <param name="worksheetName">the name of the worksheet</param>
        public static void CheckLengthOFExcelWorksheet(string worksheetName)
        {
            if (worksheetName.Length > MAX_LENGTH_EXCEL_WORKSHEET_NAME)
            {
                throw new Exception(" OpenXMLExcelReader -- illegal worksheet name, the excel application allows worksheet names not in excess of " + MAX_LENGTH_EXCEL_WORKSHEET_NAME + " characters, your worksheet " + worksheetName + " is " + worksheetName.Length + " characters, please use a shorter worksheet name that is less than " + MAX_LENGTH_EXCEL_WORKSHEET_NAME + " characters.");
            }
        }

        
        //}
        /// <summary>
        /// this method will read the specified worksheet from the specified fully justifed spreadsheet filename.
        /// the resulting data will be placed into a datatable with all of the cells being string. the resulting data table
        /// may then be queried using DataTable.Select(sqlWhereClause), or using linq. please see the code examples included.
        /// </summary>
        /// <example>
        /// to get specific rows, just use a SQL where clause
        /// <code>
        ///  DataTable theDT = Central.Util.OpenXmlExcelReader.GetDataTable(this.txtBxSelectedSpreadsheet.Text, this.cmboBxSelectedWorksheet.Text);
        ///
        ///        DataRow[] theRows = null;
        ///        
        ///            theRows = theDT.Select("((Convert(columnName,'System.Decimal') %LT% 20) AND (NOT(columnName == NULL)))");
        ///
        ///        DataSet ds = Central.Util.OpenXmlExcelReader.MakeDataSetFromResults(theDT, theRows);
        ///        
        ///        //if specific columns are needed use linq on ds.Tables[0] as in the next code example.
        ///
        /// </code>
        /// to get a specific column within the resulting datatable, use linq with a select.
        /// <code>
        /// DataTable theDT = Central.Util.OpenXmlExcelReader.GetDataTable(this.txtBxSelectedSpreadsheet.Text, this.cmboBxSelectedWorksheet.Text);
        ///       
        ///        var result = (from row in theDT.AsEnumerable()
        ///                      where !(String.IsNullOrEmpty(row.Field%LT%string%GT%("columnName")))
        ///                      select (string)(row.Field%LT%string%GT%("columnName")));
        ///
        ///        if (result != null)
        ///        {
        ///            List %LT%string%GT% theResults = new List%LT%string%GT%(result);
        ///            if (theResults.Count == 0)
        ///            {
        ///                throw new Exception("Error Reading the unique values. no unique values found.");
        ///            }
        ///
        ///            this.lstbxSelectedCriteria.Items.Clear();
        ///
        ///            foreach (string s in theResults)
        ///            {
        ///                string theVal = s;
        ///                if ((theVal != null) && (theVal.Length %GT% 0))
        ///                {
        ///                    if (!(lstbxSelectedCriteria.Items.Contains(theVal)))
        ///                    {
        ///                        lstbxSelectedCriteria.Items.Add(theVal);
        ///                    }
        ///                }
        ///            }
        ///            
        /// </code>
        /// 
        /// </example>
        /// <param name="fullyJustifiedSpreadsheetFilename">the fully justified filename of the spreadsheet to be used. must be 2007 xlsx or higher</param>
        /// <param name="worksheetName">the worksheet name to be read from the specified spreadsheet.</param>
        /// <returns>a datatable containing columns named according to the column headers in the WS, and populated with the ws data
        /// all cell values are text.</returns>
        public static DataTable GetDataTable(string fullyJustifiedSpreadsheetFilename, string worksheetName)
        {
            CheckLengthOFExcelFilename(fullyJustifiedSpreadsheetFilename);
            CheckLengthOFExcelWorksheet(worksheetName);

            SpreadsheetDocument theDoc = SpreadsheetDocument.Open(fullyJustifiedSpreadsheetFilename, false);
            try
            {
                IEnumerable<Sheet> sheets = theDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(c => string.Compare(c.Name, worksheetName) == 0);
                if ((sheets == null) || (sheets.Count() == 0))
                {
                    throw new Exception(" Error -- Worksheet named " + worksheetName + " Not found in spreadsheet " + fullyJustifiedSpreadsheetFilename);
                }
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)theDoc.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                DataTable retDT = new DataTable();

                if (rows.Count<Row>() == 0)
                {
                    throw new Exception("OpenXMLReader: Error -- " + worksheetName + " appears to be empty.");
                }
                int numColHeaders = rows.ElementAt(0).Descendants<Cell>().Count();
                List<string> theHeaderColumnNames = new List<string>();

                foreach (Cell cell in rows.ElementAt(0))
                {
                    retDT.Columns.Add(GetCellValue(theDoc, cell));
                    string excelColumnName = GetExcelColumnName(cell.CellReference.InnerText.ToString());
                    theHeaderColumnNames.Add(excelColumnName);
                }
                int cnt = 0;
                foreach (Row row in rows)
                {
                    if (cnt > 0) //skip the header row.
                    {
                        if (row.Descendants<Cell>() != null)
                        {
                            //get the non empty cells, if all empty this is ok.
                            IEnumerable<Cell> cellValues = (from cell in row.Descendants<Cell>()
                                                            where cell.CellValue != null
                                                            select cell);
                            //skip it if its all empty.
                            if (cellValues.Count<Cell>() > 0)
                            {
                                DataRow tempRow = retDT.NewRow();
                                //if a row has empty cells these will be left out of the collection.
                                //so we have to introspect the cell reference to get the proper column.
                                for (int i = 0; i < row.Descendants<Cell>().Count<Cell>(); i++)
                                {
                                    Cell c = row.Descendants<Cell>().ElementAt(i);
                                    string cellColumn = GetExcelColumnName(c.CellReference.InnerText.ToString());
                                    int ci = theHeaderColumnNames.IndexOf(cellColumn);
                                    if (ci >= 0 && ci < theHeaderColumnNames.Count)
                                    {
                                        tempRow[ci] = GetCellValue(theDoc, c);
                                    }
                                }
                                retDT.Rows.Add(tempRow);
                            }
                        }
                    }
                    cnt++;
                }
                return retDT;
            }
            finally
            {
                if (theDoc != null)
                {
                    theDoc.Close();
                }
            }
        }
        
        
        /// <summary>
        /// this method will return true if the specified worksheet is present within the specified spreadsheet
        /// it will return false if the specified worksheet is not present in the specified spreadsheet
        /// </summary>
        /// <param name="fullyJustifiedSpreadsheetFilename">the excel spreadsheet, must be excel 2007 or higher</param>
        /// <param name="worksheetName">the worksheet name</param>
        /// <returns>true if the worksheet is present in the spreadsheet, false if the worksheet is not present in the spreadsheet</returns>
        public static bool WorksheetExists(string fullyJustifiedSpreadsheetFilename, string worksheetName)
        {
            CheckLengthOFExcelFilename(fullyJustifiedSpreadsheetFilename);
            CheckLengthOFExcelWorksheet(worksheetName);

            SpreadsheetDocument theDoc = SpreadsheetDocument.Open(fullyJustifiedSpreadsheetFilename, false);
            try
            {
                IEnumerable<Sheet> sheets = theDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(c => string.Compare(c.Name, worksheetName) == 0);
                if ((sheets == null) || (sheets.Count() == 0))
                {
                    return false;

                }
                return true;
            }
            finally
            {
                if (theDoc != null)
                {
                    theDoc.Close();
                }
            }
        }

        
        
        /// <summary>
        /// this method will get the excel column name, for example A,B,C,D etc. for the specified excel cellname.
        /// </summary>
        /// <param name="cellName">the cell name</param>
        /// <returns>the MS excel column name of the cell</returns>
        private static string GetExcelColumnName(string cellName)
        {
            // Create a regular expression to match the column name portion of the cell name.
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("[A-Za-z]+");
            System.Text.RegularExpressions.Match match = regex.Match(cellName);

            return match.Value;
        }

        
        /// <summary>
        /// this is the method that will read the contents of a call and return the string value of the contents.
        /// </summary>
        /// <param name="document">the spreadsheet document that is being read.</param>
        /// <param name="cell">the cell within the spreadsheet document.</param>
        /// <returns></returns>
        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            List<CellFormats> cellFormats = new List<CellFormats>(document.WorkbookPart.WorkbookStylesPart.Stylesheet.Descendants<CellFormats>());
            List<CellFormat> useCellFormats = cellFormats[0].Descendants<CellFormat>().ToList();
            List<NumberingFormats> numberingFormats = new List<NumberingFormats>(document.WorkbookPart.WorkbookStylesPart.Stylesheet.Descendants<NumberingFormats>());
            // this code commented out because we will not use the numbering formats.
            // if we do decide to use these in the future, then understand sometimes
            //the numberingFormats is null, meaning there are none used.
            //List<NumberingFormat> useNumberingFormats = numberingFormats[0].Descendants<NumberingFormat>().ToList();
            /*
             * 
             *    numberingFormatID definitions. these numbering formats do not exist in the excel document, 
             *    and are standard for the excel application so we must just 'know' them!!!
             *    IF the numberingFormatID is outside of this range, then go and look in the numbering formats specified in the document.
             *    Find the one with the matching id, and use it.
             * 
             *   --------numberingFormatID----------Format------------------------------------
                            1                        0
                            2                        0.00
                            3                        #,##0
                            4                        #,##0.00
                            5                        $#,##0_);($#,##0)
                            6                        $#,##0_);[Red]($#,##0)
                            7                        $#,##0.00_);($#,##0.00)
                            8                        $#,##0.00_);[Red]($#,##0.00)
                            9                        0%
                            10                       0.00%
                            11                       0.00E+00
                            12                       # ?/?
                            13                       # ??/??
                            14                       m/d/yyyy
                            15                       d-mmm-yy
                            16                       d-mmm
                            17                       mmm-yy
                            18                       h:mm AM/PM
                            19                       h:mm:ss AM/PM
                            20                       h:mm
                            21                       h:mm:ss
                            22                       m/d/yyyy h:mm
                            37                       #,##0_);(#,##0)
                            38                       #,##0_);[Red](#,##0)
                            39                       #,##0.00_);(#,##0.00)
                            40                       #,##0.00_);[Red](#,##0.00)
                            45                       mm:ss
                            46                       [h]:mm:ss
                            47                       mm:ss.0
                            48                       ##0.0E+0
                            49                       @
             */

            //we will check for strongly formatted cells, but will just do a general conversion for these cells.....
            //we could write alot of code to ensure the data gets into the same format as the user sees within the 
            //excel document, but since we will ask within biodesix that all cells be formatted as text, this would be overkill.
            //we support strongly formatted cells for the cases where we receive SS that do not adhere to Biodesix internal
            //conventions, so we can still use these.

            //So excel does the following,
            //a cell can have a data type, IF a cell does NOT have a data type
            //specified then the cell formatting specifies what is in the cell...
            //
            //each cell format, refers to a numbering format, and numbering formats have a
            //set of "standard ids" that we should just "know" and are NOT defined
            //within the Excel document anywhere.
            //if the numbering format id is NOT in the set of standard ids then the numbering
            //format will be defined within the Excel document xml.
            //numbering formats may be introspected to gain knowledge of the Excel specific
            //regEx that is used for formatting the cell, this needs to be translated to a c#
            //regEx in order to maintain cell formats.

            //if the cell type is not set, then use the numbering formats.
            if (cell.DataType == null)
            {
                if (cell.StyleIndex == null)
                {
                    if (cell.CellValue == null)
                    {
                        throw new Exception("OpenXmlExcelReader -- Error: The cell at address "
                            + cell.CellReference.ToString()
                            + " seems to be corrupt! Please try copying all of the populated cells from the current worksheet"
                            + " to a new worksheet and try again. If the problem persists, contact a Biodesix software developer.");
                    }
                    return cell.CellValue.Text;
                }
                //get the cell format associated with this cell.
                CellFormat cf = useCellFormats[Convert.ToInt32(cell.StyleIndex.Value)];
                //do a primitive check for number, date, or just text, and output a generically formatted string.
                if (cf.NumberFormatId > 0 && cf.NumberFormatId <= 13)
                {// This is a number   
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        //double here to support all formats including scientific notation!
                        return Double.Parse(cell.CellValue.Text).ToString("##0.0##");
                    }
                }
                else if (cf.NumberFormatId == 14)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("M/d/yyyy");
                    }
                }
                else if (cf.NumberFormatId == 15)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("d-MMM-yy");
                    }
                }
                else if (cf.NumberFormatId == 16)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("d-MMM");
                    }
                }
                else if (cf.NumberFormatId == 17)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("MMM-yy");
                    }
                }
                else if (cf.NumberFormatId == 18)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("h:mm AM/PM");
                    }
                }
                else if (cf.NumberFormatId == 19)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("h:mm:ss AM/PM");
                    }
                }
                else if (cf.NumberFormatId == 20)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("h:mm");
                    }
                }
                else if (cf.NumberFormatId == 21)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("h:mm:ss");
                    }
                }
                else if (cf.NumberFormatId == 22)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("M/d/yyyy h:mm");
                    }
                }
                else if (cf.NumberFormatId > 163) //greater than 163 is a custom numbering format.
                {
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        string format = document.WorkbookPart.WorkbookStylesPart.Stylesheet.NumberingFormats.Elements<NumberingFormat>()
                                          .Where(i => i.NumberFormatId.ToString() == cf.NumberFormatId.ToString())
                                            .First().FormatCode;

                        // Determine if a custom number format was used for a date value. Custom formatting in Excel may take many forms
                        // such as '[$-409]ddmmmyyyy' which includes a locale ([$-409] for US dates) or 'yy MMM'. if it can be
                        // determined that the cell is some kind of custom date format, format the cell value as a standard Biodesix date.
                        if ((!string.IsNullOrEmpty(format)) && ((format.StartsWith("[$-")) || (format.ToLower().Contains("yy"))))
                        {
                            return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("M/d/yyyy");
                        }
                        else if (!String.IsNullOrEmpty(format))
                        {
                            // some other unknown format is used, use the format found from the cell 
                            double d = Convert.ToDouble(cell.CellValue.Text);
                            return d.ToString(format);
                        }
                        else
                        {
                            return cell.CellValue.Text;
                        }

                    }
                }
                else if (cell.CellValue == null)
                {//just text
                    return cell.InnerText.ToString();
                }
                else
                {
                    return cell.CellValue.Text;
                }
            }

            //if the data type is set, use this.
            switch (cell.DataType.Value)
            {
                case CellValues.SharedString:
                    {//if its a chared string, then get the value of the cell from the shared string table.
                        return stringTablePart.SharedStringTable.ChildElements[Convert.ToInt32(cell.CellValue.Text)].InnerText;
                    }
                case CellValues.Boolean:
                    {
                        //if its a boolean then convert it to the value 'true' or 'false'
                        if (cell.CellValue.Text == "1")
                        {
                            return "true";
                        }
                        else
                        {
                            return "false";
                        }
                    }
                case CellValues.Date:
                    { //if its a date then it is a number that specifies the offset from the start of time, convert this to a date
                        // and output the date string.
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString();
                    }
                case CellValues.Number:
                    {
                        //if its a number just use the cellvalue.
                        return cell.CellValue.Text;
                    }
                default:
                    {//if its none of these then just use the cellvalue if it is present.
                        if (cell.CellValue != null)
                        {
                            return cell.CellValue.Text;
                        }
                    }
                    // return an empty value.
                    return string.Empty;
            }
        }
                       
    }
}
