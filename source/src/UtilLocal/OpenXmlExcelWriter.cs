﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: OpenXmlExcelWriter.cs
//
//      (C) Copyright 2018 Biodesix Inc.
//      All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;

namespace Local.Util
{
    /// <summary>
    /// this class implements the methods used to write to Excel.
    /// the class can be used to write information to MS Excel. This class uses
    /// the OpenXML interface to excel, so there is no limitations to using this other than the
    /// excel file must be 2007 or a more recent version of Excel.
    /// the recommended use of this is to modify datatables within the code,then overwrite existing worksheets
    /// by first deleting them, then writing the datatable from application memory. Note, this approach will
    /// lose all formatting that might exist in the original worksheet, so users should be aware of this
    /// and not apply any additional formatting to the worksheets that are updated by the application.
    /// </summary>
    public class OpenXmlExcelWriter
    {
        ///<summary>
        /// object used to provide multi-threaded safety.
        /// </summary>
        private static object _syncRoot = new object();

        
        /// <summary>
        /// this method will write the specified data table to the specified worksheet in the specified spreadsheet.
        /// if the worksheet exists an Exception will be thrown, use OpenXMLExcelReader.WorksheetExists to check.
        /// </summary>
        /// <param name="inputDataTable">the data table to write</param>
        /// <param name="fullyJustifiedSpreadsheetFilename">the spreadsheet filename.</param>
        /// <param name="worksheetToCreate">the worksheet to create</param>
        public static void WriteDataTableToExcelWorksheet(DataTable inputDataTable,
            string fullyJustifiedSpreadsheetFilename,
            string worksheetToCreate)
        {

					OpenXmlExcelReader.CheckLengthOFExcelFilename(fullyJustifiedSpreadsheetFilename);
					OpenXmlExcelReader.CheckLengthOFExcelWorksheet(worksheetToCreate);

            if (File.Exists(fullyJustifiedSpreadsheetFilename))
            {
                //check if the worksheet exists. if it does throw an error.
                if (Local.Util.OpenXmlExcelReader.WorksheetExists(fullyJustifiedSpreadsheetFilename, worksheetToCreate))
                {
                    throw new Exception("The worksheet " + worksheetToCreate + " already exists in the file " + fullyJustifiedSpreadsheetFilename);
                }
            }

            // Create a spreadsheet document by supplying the filepath.
            // By default, AutoSave = true, Editable = true, and Type = xlsx.
            SpreadsheetDocument spreadsheetDocument = null;
            WorkbookPart workbookpart = null;
            try
            {
                if (!File.Exists(fullyJustifiedSpreadsheetFilename))
                {
                    string emptySS = System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "Empty.xlsx";
                    
                    lock (_syncRoot)
                    {
                        File.Copy(emptySS, fullyJustifiedSpreadsheetFilename);
                    }
                }
                spreadsheetDocument = SpreadsheetDocument.Open(fullyJustifiedSpreadsheetFilename, true);
                workbookpart = spreadsheetDocument.WorkbookPart;

                // Add a WorksheetPart to the WorkbookPart.
                WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                // Add Sheets to the Workbook.
                Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.Sheets;

                // Append a new worksheet and associate it with the workbook.
                Sheet sheet = new Sheet();

                uint theSheetId = 1;

                if (sheets.Elements<Sheet>().Count() > 0)
                {
                    theSheetId =
                        sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                }


                sheet.Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart);
                sheet.SheetId = theSheetId;
                sheet.Name = worksheetToCreate;

                sheets.Append(sheet);

                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

                //write the header row.
                Row r = new Row();
                List<string> theColumnHeaders = new List<string>();
                int dcCount = 1;
                foreach (DataColumn dc in inputDataTable.Columns)
                {
                    string valueT = dc.ColumnName;
                    //Create a new inline string cell.
                    Cell c = new Cell();
                    c.DataType = CellValues.InlineString;
                    //Add text to the text cell.
                    InlineString inlineString = new InlineString();
                    Text t = new Text();
                    t.Text = valueT;
                    inlineString.AppendChild(t);
                    c.AppendChild(inlineString);
                    r.Append(c);
                    theColumnHeaders.Add(GetExcelColumnName(dcCount));
                    c.CellReference = theColumnHeaders[dcCount - 1] + "1";
                    c.CellValue = new CellValue(valueT);
                    dcCount++;
                }
                sheetData.Append(r);
                int rCount = 2;
                foreach (DataRow dr in inputDataTable.Rows)
                {
                    r = new Row();
                    dcCount = 0;
                    foreach (DataColumn dc in inputDataTable.Columns)
                    {
                        string valueT = dr[dc.ColumnName].ToString();
                        //Create a new inline string cell.
                        Cell c = new Cell();
                        c.DataType = CellValues.InlineString;
                        //Add text to the text cell.
                        InlineString inlineString = new InlineString();
                        Text t = new Text();
                        t.Text = valueT;
                        inlineString.AppendChild(t);
                        c.AppendChild(inlineString);
                        c.CellReference = theColumnHeaders[dcCount]+rCount.ToString("#######");
                        c.CellValue = new CellValue(valueT);
                        r.Append(c);
                        dcCount++;
                    }
                    rCount++;
                    sheetData.Append(r);
                }

                workbookpart.Workbook.Save();
            }
            finally
            {
                if (spreadsheetDocument != null)
                {
                    // Close the document.
                    spreadsheetDocument.Close();
                }
            }
        }
        
        /// <summary>
        /// this method will generate the Excel centric column name for columns 1-N.
        /// </summary>
        /// <param name="columnNumber">column number 1 -N</param>
        /// <returns>the excel centric column name A,B...Z, AA,BB...ZZ etc</returns>
        private static string GetExcelColumnName(int columnNumber) 
        { 
            int dividend = columnNumber; 
            string columnName = String.Empty; 
            int modulo; 
            while (dividend > 0) 
            { 
                modulo = (dividend - 1) % 26; 
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }
            return columnName; 
        } 

    }
}
