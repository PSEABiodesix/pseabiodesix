﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: PearsonsRValue.cs
//
//      (C) Copyright 2018 Biodesix Inc.
//      All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;

namespace Local.Util
{
	/// <summary>
	/// content from onlinestatsbook.com David M Lane author.
	/// 
	/// There are several formulas that can be used to compute Pearson's correlation. 
	/// Some formulas make more conceptual sense whereas others are easier to actually compute. 
	/// We are going to begin with a formula that makes more conceptual sense.
	/// 
	///
	///We are going to compute the correlation between the variables X and Y shown in Table 1. 
	///We begin by computing the mean for X and subtracting this mean from all values of X. 
	///The new variable is called "x." 
	///The variable "y" is computed similarly. The variables x and y are said to be deviation scores because each score is a deviation from the mean. 
	///Notice that the means of x and y are both 0. Next we create a new column by multiplying x and y. 

	///Before proceeding with the calculations, 
	///let's consider why the sum of the xy column reveals the relationship between X and Y. 
	///If there were no relationship between X and Y, then positive values of x would be just as likely to be 
	///paired with negative values of y as with positive values. 
	///This would make negative values of xy as likely as positive values and the sum would be small. 
	///On the other hand, consider Table 1 in which high values of X are associated with 
	///high values of Y and low values of X are associated with low values of Y. 
	///You can see that positive values of x are associated with positive values of y 
	///and negative values of x are associated with negative values of y. 
	///In all cases, the product of x and y is positive, resulting in a high total for the xy column. 
	///Finally, if there were a negative relationship then positive values of x would be associated 
	///with negative values of y and negative values of x would be associated with positive values of y. 
	///This would lead to negative values for xy.
	///
	///Table 1. Calculation of r.
	///
	///          X  Y  x  y  xy x2 y2
	///
	///          1  4 -3 -5 15  9 25 
	///          3  6 -1 -3  3  1  9 
	///          5 10  1  1  1  1  1 
	///          5 12  1  3  3  1  9 
	///          6 13  2  4  8  4 16 
	///   Total 20 45  0  0 30 16 60 
	///    Mean  4  9  0  0  6     
	///
	///Pearson's r is designed so that the correlation between height and weight is the same whether 
	///height is measured in inches or in feet. To achieve this property, Pearson's correlation is 
	///computed by dividing the sum of the xy column (Σxy) by the square root of the product of the sum 
	///of the x2 column (Σx2) and the sum of the y2 column (Σy2). The resulting formula is:
	///
	///	R = SumOfAll(xy) / (SquareRoot(Square(SumOfAll(x)) / Square(SumOfAll(y))))
	///
	/// </summary>
	public static class PearsonsRValue
	{
		/// <summary>
		/// For the specified X and Y values, compute the pearsons R value
		/// </summary>
		/// <param name="xVals">the list of X values.</param>
		/// <param name="yVals">the list of Y values</param>
		/// <returns>the pearsons R value that results from the input X and Y values</returns>
		public static double ComputePearsonsR(List<double> xVals, List<double> yVals)
		{
			double pearsonsR = -1000.0;
			try
			{
				if (xVals.Count != yVals.Count)
				{
					throw new Exception(" Input Data Error, X values is not the same size as Y values");
				}
				//first we compute the mean for X and Y.
				double accum = 0.0;
				foreach(double d in xVals)
				{
					accum += d;
				}
				double meanX = accum / (Convert.ToDouble(xVals.Count));
				accum = 0.0;
				foreach (double d in yVals)
				{
					accum += d;
				}
				double meanY = accum / (Convert.ToDouble(yVals.Count));

				//next we compute the deviation scores.
				List<double> deviationScoresX = new List<double>();
				List<double> deviationScoresY = new List<double>();

				foreach (double d in xVals)
				{
					double deviationScoreX = meanX - d;
					deviationScoresX.Add(deviationScoreX);
				}

				foreach (double d in yVals)
				{
					double deviationScoreY = meanY - d;
					deviationScoresY.Add(deviationScoreY);
				}
                
				//compute all the sums as required.
				List<double> xy = new List<double>();
				List<double> x2 = new List<double>();
				List<double> y2 = new List<double>();

				double sumxy = 0.0;
				double sumy2 = 0.0;
				double sumx2 = 0.0;

				for (int i = 0; i < deviationScoresX.Count; i++)
				{
					double x = deviationScoresX[i];
					double y = deviationScoresY[i];
					double prod = x * y;
					double xSquared = x * x;
					double ySquared = y * y;

					xy.Add(prod);
					x2.Add(xSquared);
					y2.Add(ySquared);

					sumxy += prod;
					sumy2 += ySquared;
					sumx2 += xSquared;
				}
				double tsqrt = Math.Sqrt(sumx2 * sumy2);
				//final computation, this will return NaN if values are 0's
				pearsonsR = sumxy / tsqrt ;

				return pearsonsR;
			}
			catch (Exception ex)
			{
				throw new Exception("ComputePearsonsR, Error, an unexpected error occured computing pearsons R value, the message is " + ex.Message + " Please examine teh nextgen log for more details");
			}
		}
	}
}
