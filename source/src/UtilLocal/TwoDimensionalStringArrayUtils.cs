﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: TwoDimensionalStringArrayUtils.cs
//
//      (C) Copyright 2018 Biodesix Inc.
//      All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Local.Util
{
    /// <summary>
    /// this class contains utility methods relating to 2d arrays of strings.
    /// 
    /// There are a host of CSV file utilities in this class that should be moved to a common
    /// CSV utilty class at some point!
    /// </summary>
    public static class TwoDimensionalStringArrayUtils    {

        /// <summary>
        /// this method will convert the specified data table into a 2d array of strings.
        /// the 0th row contains the column headers.
        /// </summary>
        /// <param name="theData">the data table</param>
        /// <returns>a 2d array of strings, the oth row of the array contains the column headers</returns>
        public static string[,] ConvertDataTableTo2DString(DataTable theData)
        {
            string[,] thestr = new string[theData.Rows.Count + 1, theData.Columns.Count];
            int row = 0;
            int column = 0;
            foreach (DataColumn dc in theData.Columns)
            {
                thestr[row, column] = dc.ColumnName;
                column++;
            }
            row++;
            foreach (DataRow dr in theData.Rows)
            {
                column = 0;
                foreach (DataColumn dc in theData.Columns)
                {
                    thestr[row, column] = dr[dc.ColumnName].ToString();
                    column++;
                }
                row++;
            }
            return thestr;
        }

        /// <summary>
        /// this method will convert a 3d array of strings to a datatable,
        /// the oth row is used as the column headers
        /// </summary>
        /// <param name="theData">the 2d array of strings</param>
        /// <returns>a datatable populated from the 2d array of strings, the 0th row of string data is used as the column headers</returns>
        public static DataTable Convert2DStringToDataTable(string[,] theData)
        {
            DataTable theTable = new DataTable();
            List<string> colNames = new List<string>();
            int row = 0;
            for (int col = 0; col < theData.GetLength(1); col++)
            {
                theTable.Columns.Add(theData[row, col], typeof(string));
                colNames.Add(theData[row, col]);
            }
            row++;
            for (int r = row; r < theData.GetLength(0); r++)
            {
                DataRow newRow = theTable.NewRow();

                for (int col = 0; col < theData.GetLength(1); col++)
                {
                    newRow[colNames[col]] = theData[r, col];
                }
                theTable.Rows.Add(newRow);
            }
            return theTable;
        }

        /// <summary>
        /// This method will populate a comma seperated file from the specified 2d array of strings.
        /// </summary>
        /// <param name="data">The 2d array of strings to write to the .csv file.</param>
        /// <param name="filename">The fully justified filename of the file to create and store.</param>
        public static void WriteCSVFile(string[,] data, string filename)
        {
            // This code was changed from doing string concatenations to using string.Join
            // It is much faster, add a line at a time to the StringBuilder object.
            // Then dump the StringBuilder.ToString() to a file.
            StringBuilder sb = new StringBuilder();

            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }

            for (int i = 0; i < data.GetLength(0); i++)
            {

                string[] theLineData = new string[data.GetLength(1)];
                for (int l = 0; l < data.GetLength(1); l++)
                {
                    theLineData[l] = data[i, l];
                }
                string theLine = string.Join(",", theLineData);
                sb.AppendLine(theLine);
            }
            File.WriteAllText(filename, sb.ToString());

            
        }


        /// <summary>
        /// This method will populate a comma seperated file from the specified 2d array of strings.
        /// </summary>
        /// <param name="data">The 2d array of strings to write to the .csv file.</param>
        /// <param name="filename">The fully justified filename of the file to create and store.</param>
        public static void WriteCSVFile(double[,] data, string filename)
        {
            // This code was changed from doing string concatenations to using string.Join
            // It is much faster, add a line at a time to the StringBuilder object.
            // Then dump the StringBuilder.ToString() to a file.
            StringBuilder sb = new StringBuilder();

            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }

            for (int i = 0; i < data.GetLength(0); i++)
            {

                string[] theLineData = new string[data.GetLength(1)];
                for (int l = 0; l < data.GetLength(1); l++)
                {
                    theLineData[l] = data[i, l].ToString();
                }
                string theLine = string.Join(",", theLineData);
                sb.AppendLine(theLine);
            }
            File.WriteAllText(filename, sb.ToString());
            
        }

        /// <summary>
        /// This method will read a comma separated file into a 2d array of strings.
        /// </summary>
        /// <param name="filename">The comma separated file, fully justified path.</param>
        /// <returns>The 2d array of strings populated from the comma separated file.</returns>
        public static string[,] ReadCVSFile(string filename)
        {
            List<List<string>> theData = new List<List<string>>();
            StreamReader sr = null;



            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }
            try
            {
                FileStream fs = new FileStream(filename, FileMode.Open);
                sr = new StreamReader(fs);
                string line = null;
                int numberColumns = -1;

                while ((line = sr.ReadLine()) != null)
                {

                    string[] items = line.Split(',');
                    if (numberColumns == -1)
                    {
                        numberColumns = items.Count();
                    }
                    if (items.Count() != numberColumns)
                    {
                        throw new Exception("the data file " + filename + " contains a variable number of columns within its data, this cannot be read");
                    }

                    List<string> theValues = new List<string>();
                    for (int l = 0; l < items.Count(); l++)
                    {
                        theValues.Add(items[l]);
                    }
                    theData.Add(theValues);
                }
            }
            finally
            {
                // Closing the StreamReader also closes the FileStream.
                if (sr != null)
                {
                    sr.Close();
                }
            }
            int rows = theData.Count;
            int columns = theData[0].Count;
            string[,] theRet = new string[rows, columns];

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < columns; c++)
                {
                    theRet[r, c] = theData[r][c];
                }
            }

            return theRet;
        }
        

    }
}
