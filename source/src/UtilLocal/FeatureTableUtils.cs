﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: FeatureTableUtils.cs
//
//      (C) Copyright 2018 Biodesix Inc.
//      All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using Local.Util;
using System.Data;

namespace Local.Util
{
    /// <summary>
    /// This class contains a set of static utility methods that operate on FeatureTable objects.
    /// </summary>
    public static class FeatureTableUtils
    {
                
        /// <summary>
        /// This method returns a list of column names for the columns that contain complete numeric 
        /// features.  This means that every row of the feature table contains a non-null numeric value.
        /// </summary>
        /// <example>
        /// ClassifySpectraInputValidator uses this method in validating input data
        /// <code>
        /// // Check every input test feature table - test feature tables must contain all of the 
        /// // numeric columns that the classifier feature table contains.
        /// foreach (DataItem testFeatureTableDI in testFeatureTableItems)
        /// {
        ///     FeatureTable testFeatureTable = (FeatureTable)Cache.Cache.GetInstance().Operations.GetData(testFeatureTableDI.CacheID);
        ///     List{string} testFeatureTableNumericColumns = FeatureTableUtils.GetCompleteNumericFeatures(testFeatureTable);
        ///
        ///     foreach (string numericCol in classifierFeatureTableNumericColumns)
        ///     {
        ///         if (!testFeatureTableNumericColumns.Contains(numericCol))
        ///         {
        ///             throw new Exception("Test Feature Table - " + testFeatureTable.TableName + " - is missing column - " + numericCol + " for analysis.  Please create a feature table whose feature columns match the training set feature table.");
        ///         }
        ///     }
        /// }
        /// </code>
        /// </example>
        /// <param name="ft">The feature table</param>
        /// <returns>List of column names for which every row contains a non-null numeric value</returns>
        public static List<string> GetCompleteNumericFeatures(FeatureTable ft)
        {
            // Create list to return 
            List<string> completeNumericColumns = new List<string>();

            // Check every column
            foreach (DataColumn dc in ft.Columns)
            {
							//never use the filename or groupname columns as feature data!!!...even if they have numbers in them.
							if (dc.ColumnName.Equals(ft.FilenameColumnName) || dc.ColumnName.Equals(ft.GroupColumnName))
							{
								continue;
							}
                // set complete numeric column to true, until proven otherwise
                bool isCompleteNumericColumn = true;

                // check every row for a numeric value
                foreach (DataRow dr in ft.Rows)
                {
                    double testDouble = 0.0;
                   if (!(Double.TryParse(dr[dc].ToString(),out testDouble)))
                   {
                        // value is either null or non-numeric
                        isCompleteNumericColumn = false;
                        // no need to continue looking through rows
                        break;
                   }
                }

                // if complete and numeric, add column name to the return list
                if (isCompleteNumericColumn)
                {
                    completeNumericColumns.Add(dc.ColumnName);
                }
            }
            return completeNumericColumns;
        }

		               

        /// <summary>
        /// Given a feature table and a filename, return the data row from the feature
        /// table that corresponds to the filename.
        /// </summary>
        /// <param name="theFT">The feature table from which to get the DataRow.</param>
        /// <param name="filename">The filename that corresponds to the desired DataRow.</param>
        /// <returns>The DataRow corresponding to the given filename</returns>
        public static DataRow GetDataRowForFilename(FeatureTable theFT, string filename)
        {
            DataRow retval = null;

            foreach (DataRow dr in theFT.Rows)
            {
                if (dr[theFT.FilenameColumnName].ToString().Equals(filename))
                {
                    retval = dr;
                    break;
                }
            }

            return retval;
        }
    }
}
